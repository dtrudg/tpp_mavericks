;
; TPP installer script component:
; List of files from Proteowizard to include in TPP.
;

!macro proteowizardFiles addOrDelete	; "addOrDelete" is a parameter for what action to take

SetOverwrite on

!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\msconvert.exe" ""

; ABI
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.Data.AnalystDataProvider.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.Data.CommonInterfaces.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.Data.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.Data.WiffReader.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.InternalRawXYProcessing.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.ProjectUtilities.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.StructuredStorage.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Clearcore2.Utility.dll" ""

; Agilent
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\agtsampleinforw.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BaseCommon.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BaseDataAccess.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BaseError.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BaseTof.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\MassSpecDataReader.dll" ""

; Bruker
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BDal.CXt.Lc.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BDal.CXt.Lc.Factory.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BDal.CXt.Lc.Interfaces.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BDal.CXt.Lc.UntU2.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BDal.BCO.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\BDal.BCO.interfaces.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\boost_date_time-vc90-mt-1_37-BDAL_20091123.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\boost_regex-vc90-mt-1_37-BDAL_20091123.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\boost_thread-vc90-mt-1_37-BDAL_20091123.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\mkl_sequential.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\msvcr71.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\CompassXtractMS.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\HSReadWrite.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Interop.EDAL.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Interop.HSREADWRITELib.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Interop.EDAL.SxS.manifest" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\Interop.HSREADWRITELib.SxS.manifest" ""

; Thermo
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\fileio.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\fregistry.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\MSFileReader.XRawfile2.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\MSFileReader.XRawfile2.SxS.manifest" ""

; Waters
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pwiz\MassLynxRaw.dll" ""

!macroend
