;
; TPP installer script component:
; a list of TPP install files, broken out from rest of script to 
; distinguish content changes from installer logic changes
;
; Copyright (c) 2006, 2007, 2008 Insilicos, LLC
;
; This library is free software; you can redistribute it and/or
; modify it under the terms of the GNU Lesser General Public
; License as published by the Free Software Foundation; either
; version 2.1 of the License, or (at your option) any later version.
; 
; This library is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
; Lesser General Public License for more details.
; 
; You should have received a copy of the GNU Lesser General Public
; License along with this library; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
; $Author: bpratt $
;
; $DateTime: 2006/08/08 16:44:37 $
;
;
; NOTES:
;
; TODO:
;
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


!macro handleFiles addOrDelete ; "addOrDelete" is a parameter that defines the action

SetOverwrite on

; qualscore
; NOTE! qualscore must be checked out at top-level dir
; NOTE: qualscore is copied and renamed to the build dir in the qualscore MSVC project
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "qualscore.jar" ""

; PepC statistical peptide counting
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "Pepc.jar" ""
!insertmacro ${addOrDelete} "${PRODUCT_BUILD_DIR}\PepcView" "*" "PepcView"	

!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "*.xsl" ""
!insertmacro ${addOrDelete} "..\schema" "*.xsd" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "run_in.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "tpp_hostname.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ASAPRatioPeptideParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ASAPRatioProteinRatioParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ASAPRatioPvalueParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "BatchCoverage.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "crypt.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "CompactParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "DatabaseParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "decoyFASTA.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "EnzymeDigestionParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "InteractParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "LibraPeptideParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "LibraProteinRatioParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "Mascot2XML.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "PeptideProphetParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "InterProphetParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ProteinProphet.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "PTMProphetParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "RefreshParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "runperl.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "Sequest2XML.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "spectrast.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "Sqt2XML.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "Tandem2XML.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "xinteract.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "XPressPeptideParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "XPressProteinRatioParser.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "compareProts.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "compareProts3.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "TPPVersionInfo.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "run_marimba.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "filterMRM.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "get_prots.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "renamedat.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "min_sort.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "StatUtilities.pm" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "calctppstat.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "exporTPP.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pep_dbcount.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "digestdb.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "subsetdb.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "add_mz.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "calculate_pi.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "translateDNA2AA-FASTA.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "iconv.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "iconv.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "libeay32.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "libexslt.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "libxml2.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "libxmlsec-mscrypto.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "libxmlsec-openssl.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "libxmlsec.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "libxslt.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "minigzip.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "openssl.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "ssleay32.dll" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "xmlcatalog.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "xmllint.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "xmlsec.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "xsldbg.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "xsltproc.exe" ""
!insertmacro ${addOrDelete} "..\extern\xsltproc" "zlib1.dll" ""
!insertmacro ${addOrDelete} "..\extern\bsdtar-1.2.38-bin\bin" "archive1.dll" ""
!insertmacro ${addOrDelete} "..\extern\bsdtar-1.2.38-bin\bin" "bsdtar.exe" ""
!insertmacro ${addOrDelete} "..\extern\bsdtar-1.2.38-bin\bin" "bzip2.dll" ""
!insertmacro ${addOrDelete} "..\extern\bsdtar-1.2.38-bin\bin" "zlib1.dll" ""
!insertmacro ${addOrDelete} "..\extern\gnuplot\bin" "pgnuplot.exe" ""
!insertmacro ${addOrDelete} "..\extern\gnuplot\bin" "wgnuplot.exe" ""
!insertmacro ${addOrDelete} "..\extern\gnuplot\bin" "wgnuplot.hlp" ""
!insertmacro ${addOrDelete} "..\extern\gnuplot\bin" "*.dll" ""
!insertmacro ${addOrDelete} "..\extern\gnuplot\bin\etc\fonts" "fonts.conf" "etc\fonts"
!insertmacro ${addOrDelete} "..\extern\gdwin32" "bgd.dll" ""
!insertmacro ${addOrDelete} "..\extern\libw32c\bin" "bzip2.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "setWin32Permissions.exe" ""
!insertmacro ${addOrDelete} "." "license.txt" ""
; !insertmacro ${addOrDelete} "." "releasenotes.txt" ""
!insertmacro ${addOrDelete} ".." "README.txt" ""
!insertmacro ${addOrDelete} "..\HELP_DIR" "*.png" ""
!insertmacro ${addOrDelete} "..\HELP_DIR" "*.jpg" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "AnalysisSummaryParser.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ASAPCGIDisplay.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ASAPRatioPeptideCGIDisplayParser.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "comet-fastadb.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "cometresult.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "disCidData_xml.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "dta-xml.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "getCidData_xml.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ModelParser.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "iModelParser.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "Pep3D_xml.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "PepXMLViewer.cgi" ""
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\css" "PepXMLViewer.css" "..\wwwroot\ISB\html\css"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\js" "addEvent.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\js" "main.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\js" "selectbox.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\js" "sweetTitles.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\js" "toggleDiv.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\js" "validations.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html\js" "PepXMLViewerUtils.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html" "PepXMLViewer.html" "..\wwwroot\ISB\html"
!insertmacro ${addOrDelete} "..\src\Visualization\PepXMLViewer\html" "PepXMLViewer_noFile.html" "..\wwwroot\ISB\html"
!insertmacro ${addOrDelete} "..\HTML" "post_to_spectrast.html" "..\wwwroot\ISB\html"
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "plot-msms.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "plot-msms-js.cgi" ""
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\css" "lorikeet.css" "..\wwwroot\ISB\html\css"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "aminoacid.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "excanvas.min.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "ion.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "jquery.flot.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "jquery.flot.selection.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "jquery.min.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "jquery-ui.min.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "peptide.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\js" "specview.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\images" "header_lorikeet.png" "..\wwwroot\ISB\html\images\lorikeet"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\images" "header_scan.png" "..\wwwroot\ISB\html\images\lorikeet"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\images" "header_text.png" "..\wwwroot\ISB\html\images\lorikeet"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\images" "lorikeet_text_small.png" "..\wwwroot\ISB\html\images\lorikeet"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\images" "slider_handle.png" "..\wwwroot\ISB\html\images\lorikeet"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\images" "zoom_in.png" "..\wwwroot\ISB\html\images\lorikeet"
!insertmacro ${addOrDelete} "..\src\Visualization\Comet\plot-msms\images" "zoom_out.png" "..\wwwroot\ISB\html\images\lorikeet"
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "plotspectrast.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "plotspectrast.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "Lib2HTML.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "runperl.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "sequest-tgz-out.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "xpress-prophet-update.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "XPressCGIProteinDisplayParser.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "XPressPeptideUpdateParser.cgi" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "tpplib_perl.pm" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "check_env.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "fileDownloader.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "findsubsets.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "mascotout.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "more_anal.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "more_pepanal.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "peptidexml_html2.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pepxml2html.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "protxml2html.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "prot_wt_xml.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ProphetModels.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "decoyFastaGenerator.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "ProtProphModels.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "tpp_models.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_dataset_derivation.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_tmp_pngfile.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_help.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_nspbin.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_pepdataset_derivation.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_pipeline_help.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_search_params.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "show_sens_err.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "replaceall.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "updateAllPaths.pl" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "TPPVersionInfo.pl" ""
!insertmacro ${addOrDelete} "..\perl" "SSRCalc3.par" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "SSRCalc3.pl" ""
!insertmacro ${addOrDelete} "..\src\Visualization\Comet" "cometlinks.def" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "out2summary.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "out2XML.exe" ""
!insertmacro ${addOrDelete} "..\images" "pa_tiny.png" "..\wwwroot\ISB\html\images"
!insertmacro ${addOrDelete} "..\images" "spectrast_tiny.png" "..\wwwroot\ISB\html\images"
;!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "msxml2other.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "MzXML2Search.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "dta2mzxml.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "readmzXML.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "indexmzXML.exe" ""
!insertmacro ${addOrDelete} "." "tpp_installtest.bat" ""
SetOverwrite off
!insertmacro ${addOrDelete} "." "residue_modifications.cfg" ""
SetOverwrite on

!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "RTCalc.exe" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "RTCalc.coeff" ""

; GUI - note that perlpaths.mak manupulates tpp_gui.pl
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "tpp_gui.css" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui\js" "filterTable.js" "..\wwwroot\ISB\html\js"
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "ASAPRatio.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "libra_info.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "pepXML.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "mascothowto.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "pepprophet.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "protprophet.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "xpress.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "RunMascotinfo.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "RunSearchinfo.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "RunTandeminfo.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "mzXMLinfo.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "pepXMLinfo.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "pepXMLcheckinfo.html" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "pepxmlcheck.pl" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "peptideinfo.html" ""
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "tandemhowto.html" ""
!insertmacro ${addOrDelete} "..\extern\d3" "d3.js" "..\wwwroot\ISB\html\js"

;!insertmacro ${addOrDelete} "." "TPP_FAQ.html" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "tpp_gui.pl" ""
SetOverwrite off
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "tpp_gui_config.pl" ""
SetOverwrite on

!insertmacro ${addOrDelete} "..\images" "qMark.png" "images"
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "nhlbi.gif" "images"
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "nigms.jpg" "images"
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "isb_logo_tiny.jpg" "images"
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "isb_logo.gif" "images"
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "logo_anim.gif" "images"
!insertmacro ${addOrDelete} "..\CGI\tpp_gui" "spc_logo.png" "images"
!insertmacro ${addOrDelete} "." "TPP.ico" "images"
!insertmacro ${addOrDelete} "." "petunia.ico" "images"
SetOverwrite off
!insertmacro ${addOrDelete} "..\CGI\tpp_gui\users\guest" ".password" "users\guest"
SetOverwrite on
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "runsearch.exe" ""
!insertmacro ${addOrDelete} "..\extern\setacl-cmdline" "SetACL.exe" ""

; pluggable x!tandem
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "tandem.exe" ""
!insertmacro ${addOrDelete} "\MinGW\bin" "pthreadGC2.dll" ""
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "isb_default_input_kscore.xml" "..\wwwroot\ISB\data\parameters"
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "isb_default_input_native.xml" "..\wwwroot\ISB\data\parameters"
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "tandem_params.xml" "..\wwwroot\ISB\data\parameters"
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "taxonomy.xml" "..\wwwroot\ISB\data\parameters"
!insertmacro ${addOrDelete} ${PRODUCT_BUILD_DIR} "tandem-style.xsl" "..\wwwroot\ISB\data\parameters"

; MaRiMba Tutorial Files
!insertmacro ${addOrDelete} "..\HELP_DIR\MaRiMba_Tutorial" "README.txt" "..\wwwroot\ISB\data\MaRiMba"

; win32 versions of unix utilities from http://unxutils.sourceforge.net/ 
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "cp.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "rm.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "cat.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "cut.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "sed.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "sort.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "find.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "grep.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "less.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "ls.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "mv.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "chmod.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "mkdir.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "md5sum.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "echo.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "head.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "tail.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "touch.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "tar.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "unzip.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "wc.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "wget.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "which.exe" ""
!insertmacro ${addOrDelete} "..\extern\UnxUtils\usr\local\wbin" "xargs.exe" ""

!macroend
