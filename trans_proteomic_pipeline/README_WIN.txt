Windows Native Build (MinGW and VC8) Instructions
-------------------------------------------------

Updated November 16, 2009
Natalie Tasman
Updated October 14, 2010
Brian Pratt


==IMPORTANT NOTE==

 These instructions are only for developers who wish to modify the TPP
in any way.  For users who just wish to use the TPP, you *do not* have
to build it yourself: you should use the pre-built windows native
installer, which will install the TPP and other required software and
configure everything automatically.



==Introduction== 

The TPP is currently maintained for two native Windows build
platforms: MinGW and Microsoft Visual Studio C++.  MinGW is actually
required even if you want to use MSVC, as there are one or two libraries
that need the *nix style "make configure" step for installation. Visual
Studio 2008 Professional (VC9) is the officially supported MSVC version,
Visual Studio 2005 Professional (VC8) gets occasional updates, support
for VC7.1 and VC6 has been discontinued.  We hope to migrate to Visual
Studio 2009 Express in the future, but it may not currently work.
MinGW was chosen because it provides a completely free unix-like build
enviroment.  (Historically, we used to use the Cygwin system for the
same purpose, but it requires a much more heavyweight installation.)
VC9 is still required for building the raw-to-mzXML converters (which
rely on DLL/COM interfacing), but you're better off just getting those
from the ProteoWizard project's official build set.



==Prerequisites==

You'll need MinGW, even if you're an MSVC user, for reasons mentioned above.  
See below for detailed MinGW install notes.


===Perl===

* must be installed in C:/Perl

* ActiveState 5.8 is recommended; tested with 5.8.9.826 at time of writing

* http://www.activestate.com/activeperl/downloads/


===MinGW===

* note: installer leaves lots of temp files; I suggest running it in
  its own directory rather than from the desktop.

* should be installed with "g++ compiler" and "MinGW Make".

* from https://sourceforge.net/projects/mingw/files/, find the "Automated MinGW Installer".

* tested with version 5.1.4 at time of writing.


===The MSYS shell for MinGW===

* install MinGW first!

* follow default install instructions and make sure it's linked with
MinGW (you should be able to type "which make" and have success.)

* from https://sourceforge.net/projects/mingw/files/, find the "MSYS
  Base System", and then "Current Release".  Tested with msys-1.0.10
  at time of writing.


===NSIS installer===

* Follow standard install options.

* http://nsis.sourceforge.net/Download

* testing with 2.45 at time of writing


===SVN===

* TortoiseSVN client recommended: http://tortoisesvn.net/downloads,
  tested with 1.6.3.16613 at time of writing.


===Sashimi SVN sourcecode=== 

Choose a local directory to work in and a repository branch (probably
the current stable release branch, or the unstable trunk) and check
out the source.

* qualscore (do not rename the local directory)
  svn co https://sashimi.svn.sourceforge.net/svnroot/sashimi/trunk/qualscore

* trans_proteomic_pipeline (can be renamed; for example "tpp-trunk",
  "4-2", etc)
  svn co https://sashimi.svn.sourceforge.net/svnroot/sashimi/trunk/trans_proteomic_pipeline  

I usually create a local directory structure like

c:\sashimi
c:\sashimi\trunk
c:\sashimi\trunk\qualscore
c:\sashimi\trunk\trans_proteomic_pipeline


==Building==

For MinGW, open an MSYS window, cd to the trans_proteomic_pipeline/src
directory then type "make".

MSVC: Ideally, you should be able to open the main TPP.sln file and
"build solution" once you have done an initial MinGW build to configure
certain libraries (GSL in particular, there may be others too).

Note that both VC8 and MinGW builds produce an installer using
NSIS. The installer will install (if not already installed) and
configure the Apache websever.


