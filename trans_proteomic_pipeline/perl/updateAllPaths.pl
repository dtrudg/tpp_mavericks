#!/usr/bin/perl -w
#
###############################################################################
# Program     : updateAllPaths.pl
# Author      : Abhishek Pratap
# 2008/09
#
#
# Copyright (C) 2008 Abhishek Pratap
#                                                                           
# This library is free software; you can redistribute it and/or             
# modify it under the terms of the GNU Lesser General Public                
# License as published by the Free Software Foundation; either              
# version 2.1 of the License, or (at your option) any later version.        
#                                                                           
# This library is distributed in the hope that it will be useful,           
# but WITHOUT ANY WARRANTY; without even the implied warranty of            
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         
# General Public License for more details.                                  
#                                                                           
# You should have received a copy of the GNU Lesser General Public          
# License along with this library; if not, write to the Free Software       
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
#                                                                           
# Insitute for Systems Biology                                              
# 1441 North 34th St.                                                       
# Seattle, WA  98103  USA                                                   
#
#
#
#
# Description : This program updates the paths in all the *.xml's *.shtml's *.xsl's 
#               to the current directory from which this script is run
#               
#
#  % cd search_dir
#  % updateLocation.pl
#
###############################################################################

use strict;
use Getopt::Long;
use FindBin;
use File::Spec;
use Cwd;

use vars qw ($PROG_NAME $USAGE %OPTIONS $QUIET $VERBOSE $DEBUG $TESTONLY $DATABASE $NEWPATH);

#### Set program name and usage banner
my $PROG_NAME = $FindBin::Script;
my $USAGE = <<EOU;
Usage: $PROG_NAME [OPTIONS] <HTML FILES>
Options:
  --verbose n         Set verbosity level.  default is 0
  --quiet             Set flag to print nothing at all except errors
  --debug n           Set debug flag
  --testonly          If set, do not actually change any files
  --database          The path for database to be fixed in the .pep.xml
 

 e.g.:  $PROG_NAME --verbose 1 *.xml, *.shtml , *.xsl  
        $PROG_NAME --verbose 1 --database=/path/of/database/to/fix *.xml, *.shtml , *.xsl 

EOU


#### Process options
unless (GetOptions(\%OPTIONS,"verbose:s","quiet","debug:s","database:s",
  "testonly","qtof",
  )) {
  print "$USAGE";
  exit;
}


$VERBOSE = $OPTIONS{"verbose"} || 0;
$QUIET = $OPTIONS{"quiet"} || 0;
$DEBUG = $OPTIONS{"debug"} || 0;
$TESTONLY = $OPTIONS{"testonly"} || 0;
$DATABASE = $OPTIONS{database};
$NEWPATH = $OPTIONS{newpath};


if ($DEBUG) {
  print "Options settings:\n";
  print "  VERBOSE = $VERBOSE\n";
  print "    QUIET = $QUIET\n";
  print "    DEBUG = $DEBUG\n";
  print " TESTONLY = $TESTONLY\n";
}


main();
exit(0);


###############################################################################
sub main {

  #### If no parameters are given, print usage information
  unless ($ARGV[0]){
    print "$USAGE";
    exit;
  }



  #### Define the used variables
  my $srcstr;
  my $dststr;
  my $filename;
  my $line;
  my $origline;
  my $changed;
  my $filename_root;
  my $dbase_name;
  my $dbase_flag;


  #### Loop over each supplied filename
  while ($filename = shift(@ARGV)) {

    print "Processing $filename\n" if ($VERBOSE);

    #### Open the file
    open (INFILE,$filename)
      || die "Unable to open $filename for read";

    #### Read through the header
    while ($line = <INFILE>) {
	#print "$line" ;
 
	## Setting default dbase_flag=0
	$dbase_flag=0;

     #### Will only read the file till the search summary....
      last if ($line =~ /<\/search_summary>/);
      
my ($current_path_pep_xml,$base_name, $oldroot);

    if ($line =~ m#summary_xml="(.+?)"#) {
       
        $current_path_pep_xml = $1;
	$oldroot=$current_path_pep_xml;
	$oldroot=~ s/$filename//;
	#print "OldRoot : $oldroot\n";


   }


##Matching this in the *.xsl files
#<input type="hidden" name="xmlfile" value="/regis/sbeams4/nobackup/pig/orig/CMC/iTRAQ/CMC022_XL1/XTK_PigMerge20080430_noref/interact-prob.prot.xml"/>

elsif ($line =~ m#name="xmlfile"\s+?value="(.+?)"#) {
 $oldroot = $1;
      #print "\n\n Old Root : $oldroot";

   }



## Processing files with this line Pep.shtml's     
##<!--#include virtual="/tpp-bin/PepXMLViewer.cgi?xmlFileName=/regis/sbeams4/nobackup/pig/orig/CMC/iTRAQ/CMC042_XL1/XTK_PigMerge20080430_noref/interact-prob.pep.xml" -->



 elsif ( $line =~ m~xml[fF]{1}ile[Nname]{4}=(.+?)"~) {
	 $oldroot = $1;
	   #print "\n\n Old Root : $oldroot\n ";
	   
       }

## Processing files with this line Prot.shtml's   
#<!--#include virtual="/tpp-bin/protxml2html.pl?xmlfile=/regis/sbeams4/nobackup/pig/orig/CMC/iTRAQ/CMC022_XL1/XTK_PigMerge20080430_noref/interact-prob.prot.xml&restore_view=yes" -->



 elsif ( $line =~ m~xml[fF]{1}ile=(.+?)("|&)~) {

	    $oldroot = $1;
	   #print "\n\n Old Root : $oldroot";

	   }

## Processing files *.pep.xml's to fix the database path if user enters the Database path

elsif ( $DATABASE && ($line =~ m#<search_database.+?="(.+?)".+?#  || $line =~ m~<.+?ProteinDatabase.+?="(.+?)".+?~ )) {
         
       $dbase_name=$1;
	$oldroot=$1;
      #print "\n\n Dbase name  Found is $dbase_name \n\n ";
	
      $dbase_flag=1;

   }

	   
else {

	next;
     }
     
    
    

	my $newroot = getcwd; #`pwd`; #$ENV{PWD}
	my $dbroot = "";
	
	if ($NEWPATH) {
		$newroot = $NEWPATH;
	}
	chomp $newroot;
	
	$newroot .= "/";
    
	unless ($newroot) {
      print "  ERROR: Failed to get PWD\n";
    return;
    }

### Altering $newroot to $DBASE only when $dbase_flag=1
	if($dbase_flag== '1' && $DATABASE ) { 
	    
	    $dbroot=$DATABASE; 
		$newroot = $dbroot;
	    print "In the Database Change Mode : DATABASE_VALUE=$dbroot \n ";
	}

   
    # $oldroot should be an absolute path to a file
    #my @dirs = File::Spec->splitdir( $oldroot ); 
    #pop @dirs;                                    # get rid of file name
    #$oldroot = File::Spec->catdir( @dirs ) . "/";
	$oldroot =~ s/\\/\//g;
	$newroot =~ s/\\/\//g;
		
	my @oldpath = split /\//, $oldroot;
	my @newpath = split /\//, $newroot;
	$oldroot = "";
	for (my $i=0; $i < $#oldpath; $i++) {
		$oldroot .= $oldpath[$i]."/"
	}
	$newroot = "";
	for (my $i=0; $i <= $#newpath; $i++) {
		if ($i < $#newpath || !$dbase_flag) {
			$newroot .= $newpath[$i]."/"
		}
	}		
	
		
	$oldroot=~s/[\s\n\t\r]//;
    $newroot=~s/[\s\n\t\r]//;
	$dbroot=~s/[\s\n\t\r]//;	

    print " Old root: $oldroot\n" if ($VERBOSE);
    print " New root: $newroot\n" if ($VERBOSE);

    close(INFILE);
    if ($oldroot eq $newroot) {
     print "  $filename is already up to date\n";
    } else {
	next if($TESTONLY);
      print "  Updating '$oldroot' ==> '$newroot' and ";
     system("replaceall.pl  \"$oldroot\" \"$newroot\"  \"$filename\"");
    }

	$dbase_flag=0;

  } ### End While loop for reading each file

   

} ### End File loop for reading all the input command line values

}  ### End main sub routine

