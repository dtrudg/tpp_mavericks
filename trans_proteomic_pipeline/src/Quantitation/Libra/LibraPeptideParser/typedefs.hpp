#ifndef TYPEDEFS_HPP
#define TYPEDEFS_HPP

#include "common/sysdepend.h"
#include <vector>

typedef std::vector<double> vf;
typedef std::vector<vf> vvf;
typedef std::pair<double,double> ff;
typedef std::vector< ff > vff;

#define FALSE 0
#define TRUE 1
#define dbug FALSE

#endif
