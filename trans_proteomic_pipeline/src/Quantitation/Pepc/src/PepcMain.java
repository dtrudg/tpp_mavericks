// package pepc;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;

//
// Copyright (c) 2007,2008,2009 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Nate Heinecke
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA




public class PepcMain
{
    private static final String default_gstatistic_range = "0,.25,.5,.75,1,1.25,1.5,1.75,2,2.25,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10";
    private static final String default_ttest_range = "0.001,0.0025,0.005,0.01,0.02,0.03,0.04,0.05,0.075,0.1,0.25,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1";
    private static final int default_maxPermutations=1000000; // randomly select if needed so as to not exceed this
    public static void main(String[] args) throws IOException
    {
        if(args.length==0)
        {
            System.out.println("\nPepC 1.0\n");
            System.out.println("a program for RELATIVE PROTEIN QUANTITATION using Peptide Counting");
            System.out.println("by Nate Heinecke, Insilicos LLC");
            System.out.println("Seattle, WA\n");
            System.out.println("Based on the innovative technique developed by LEV BECKER, University of Washington\n");
            System.out.println("Please see Pepc-notes.txt for full instructions");
            System.out.println("usage: pepc [-n <jobname(default=basename from csv or file-list file, or 'pepc-results')>] [-p <max_permutations>] [-c <csv_file>] | [<file-list file>] | [-A<protxml file set A #1> [-A...]] [-B<protxml file set B #1> [-B...]]] [-G<gstatistic_range>] [-T<ttest_range>]");
            System.out.println("  max_permutations default is "+default_maxPermutations); // randomly select if needed so as to not exceed this

            System.out.println("  gstatistic_range default is ");
            System.out.println(    default_gstatistic_range);           //-C -A -B options are only meant for TPP integration
            System.out.println("  ttest_range default is ");       //they really suck to enter by hand
            System.out.println(    default_ttest_range);
            System.exit(0);
        }
        boolean doTest=false;
        boolean doJSON=false;   //do we need JSON instead of regular output???
        boolean newTest=false;
        boolean csv=false;      //are we using a CSV file? if not than we are using protxmls
        boolean ctrlfile=true; //are we using a control file (a list of protxml files)? if not than
                                //we get the list straight from the command line as -Aprot1.xml -Bprot2.xml
        String tname = "";
        String filename = null;
        String filenamesA = null;
        String filenamesB = null;
        String gstatistic_range = null;
        String ttest_range = null;
        String outdir = null;
        int group1size = 0;     //if using the -C option, this is the number of files heading into group 1.
                                //All the group 1 files should be listed first.
        int maxPermutations = default_maxPermutations; // randomly select if needed so as to not exceed this

  

        for(int i=0;i<args.length;i++)
        {
            if(args[i].equals("-t"))
            {
                doTest=true;
            }
            else if(args[i].equals("-t!"))
            {
                newTest=true;
            }
            else if(args[i].equals("-n"))
            {
                tname = args[i+1];
                i++;
            }
            else if(args[i].equals("-j"))
            {
                doJSON=true;
            }
            else if(args[i].equals("-p"))
            {
                maxPermutations = Integer.parseInt(args[i+1]);
                i++;
            }
            else if(args[i].equals("-o"))
            {
                outdir = args[i+1];
                i++;
            }
            else if(args[i].equals("-c"))
            {
                csv=true;
                ctrlfile=false;
            }
            else if(args[i].substring(0,2).equals("-A"))     // protXML file set A
                {
                ctrlfile=false;
                if(args[i].length()>2)
                    {
                    if(filenamesA==null)
                        {
                        filenamesA="";
                        }
                    String str=args[i].substring(2);
                    filenamesA+=" "+str;
					group1size += 1;
                    }
                }
            else if(args[i].substring(0,2).equals("-B"))     // protXML file set B
                {
                ctrlfile=false;
                if(args[i].length()>2)
                    {
                    if(filenamesB==null)
                        {
                        filenamesB="";
                        }
                    String str=args[i].substring(2);
                    filenamesB+=" "+str;
                    }
                }				
            else if(args[i].substring(0,2).equals("-G")) // range specification
            {
                gstatistic_range = args[i].substring(2);
            }
            else if(args[i].substring(0,2).equals("-T")) // range specification
            {
                ttest_range = args[i].substring(2);;
            }
            else if(filename==null)
            {   // not a switch, must be the -c arg or the file-list file
            if(ctrlfile || csv)
                filename=args[i];
            }

        }
		if((filename==null) && (filenamesA != null))
		{
			filename = filenamesA+" "+filenamesB;
		}
        if(filename==null)
        {
            System.out.println("Error: no files specified");
            System.exit(1);
        }
        if(outdir != null)
            {

            }
        // what gstatistic and ttest space are we exploring?
        if (gstatistic_range==null) {
            gstatistic_range = default_gstatistic_range;
        }
        if (ttest_range==null) {
            ttest_range = default_ttest_range;
        }
        SimpleProteinPopulata simpleLoader = new SimpleProteinPopulata();
        if(csv)
            simpleLoader.parseCSV(filename);        //working from one csv file
        else if(ctrlfile)
            simpleLoader.parseProtxml(filename,true,0);    //working from multiple protxmls + a ctrl file
        else
            simpleLoader.parseProtxml(filename,false,group1size);     //multiple protxml, no ctrl. Need group size

        if (tname.length()==0)
        {   // smarter default name, based on input CSV or file-list file
            if (csv || ctrlfile)
            {
                tname = filename.substring(0,filename.lastIndexOf("."));
            }
            if (tname.length()==0)
            {
                tname = "pepc-results";
            }
        }

        StringTokenizer gstatistic_tokens = new StringTokenizer(gstatistic_range,",");
        double gstatistics[] = new double[gstatistic_tokens.countTokens()];
        for(int i=0; i<gstatistics.length;i++) {
            gstatistics[i] = Double.parseDouble(gstatistic_tokens.nextToken());
        }                                          
        StringTokenizer ttest_tokens = new StringTokenizer(ttest_range,",");
        double ttests[] = new double[ttest_tokens.countTokens()];
        for(int i=0; i<ttests.length;i++) {
            ttests[i] = Double.parseDouble(ttest_tokens.nextToken());
        }
        if(newTest)
        {
            System.out.println("Creating regression info ... \n");
            new Experiment(tname,simpleLoader,gstatistics,ttests,maxPermutations,outdir,2);
            System.out.println("Done! Now use -t to run a regression test");
            return;
        }
        else if(doTest)
        {
            System.out.println("Running regression test... \n");
            new Experiment(tname,simpleLoader,gstatistics,ttests,maxPermutations,outdir,1);

            RegressionTest.runTest(tname);
            System.out.println("Done!");
            return;
        }


        //START THE ANALYSIS
        Experiment experiment;
        if(doJSON)//test type = 3 for JSON output
            experiment = new Experiment(tname,simpleLoader,gstatistics,ttests,maxPermutations,outdir,3);
        else
            experiment = new Experiment(tname,simpleLoader,gstatistics,ttests,maxPermutations,outdir,0);

        //SHOW THE RESULTS INTERACTIVELY
        if(!doJSON)
            new PepcViewer(experiment);

        
        System.out.println("Done!");
    }
}
