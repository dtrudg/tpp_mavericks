// package pepc;
                                      
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;

//
// Copyright (c) 2007 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Nate Heinecke
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA

class RegressionTest
	{
	RegressionTest()
		{

		}
	static void runTest(String sumName)
		{
		System.out.println("Comparing summaries...");
		SummaryCompara sc = new SummaryCompara();
		sc.compare(sumName+"\\regTest.txt",sumName+"\\pepcSummary.txt");
		}
	public static class SummaryCompara
	{
        class SummaryLine {
            String text;
            int linenum;
            SummaryLine(String t, int l) {
                text = t;
                linenum = l;
            }
            SummaryLine() {
            }
        }
        class SummaryInfo

			{
			double numAnalyzed=0;
			double numPerms=0;
			double summaryVersion=0;
            Vector<SummaryLine> results;
            }
		void compare(String fileOne, String fileTwo)
			{
			SummaryInfo sumiOne = new SummaryInfo();
			SummaryInfo sumiTwo = new SummaryInfo();
			System.out.println("Loading summary files for comparison ("+fileOne+" & "+fileTwo+")\n");
			try	{
				fillInfo(fileOne,sumiOne);
				fillInfo(fileTwo,sumiTwo);

				if(hasEmptyFields(sumiOne))
					{
					System.out.println("Error parsing "+fileOne);
					return;
					}
				if(hasEmptyFields(sumiTwo))
					{
					System.out.println("Error parsing "+fileTwo);
					return;
					}
				double d;
				int errcount=0;
                double maxJobPctDiff = 0;
                double maxJobDiff = 0;
                SummaryLine maxJobDiffLineOne=new SummaryLine();
                SummaryLine maxJobDiffLineTwo=new SummaryLine();
                SummaryLine maxJobPctDiffLineOne=new SummaryLine();
                SummaryLine maxJobPctDiffLineTwo=new SummaryLine();
                if((d=sumiOne.summaryVersion - sumiTwo.summaryVersion) != 0)
					{
					System.out.println("Summary versions differ by "+d);
					errcount++;
					}
				if((d=sumiOne.numAnalyzed - sumiTwo.numAnalyzed) != 0)
					{
					System.out.println("Number of proteins analyzed differs by "+d);
					errcount++;
					}
				if((d=sumiOne.numPerms - sumiTwo.numPerms) != 0)
					{
					System.out.println("Permutations per protein differs by "+d);
					errcount++;
					}
                if (sumiOne.results.size() != sumiTwo.results.size()) {
                    System.out.println("different results count");
					errcount++;
                } else {
                    for (int i=0;i<sumiOne.results.size();i++) {
                        String line1 = sumiOne.results.get(i).text;
                        String line2 = sumiTwo.results.get(i).text;
                        if (!line1.equals(line2)) {                           
                            System.out.print("different result at line "+ i);
                            StringTokenizer st1 = new StringTokenizer(line1,"\t");
                            StringTokenizer st2 = new StringTokenizer(line2,"\t");
                            double maxLinePctDiff = 0;
                            double maxLineDiff = 0;
                            while (st1.hasMoreTokens() && st2.hasMoreTokens()) {
                                String s1=st1.nextToken();
                                String s2=st2.nextToken();
                                double d1 = Double.parseDouble(s1);
                                double d2 = Double.parseDouble(s2);
                                double diff = Math.abs(d1-d2);
                                if (diff>0) {
                                    double pctdiff = diff/Math.max(d1,d2);
                                    maxLinePctDiff = Math.max(maxLinePctDiff,pctdiff);
                                    maxLineDiff = Math.max(diff,maxLineDiff);
                                }
                             }
                            System.out.print(", max abs change is "+ maxLineDiff);
                            System.out.println(", max % change is "+ 100.0*maxLinePctDiff);
                            System.out.println(line1);
                            System.out.println(line2);
                            if (maxJobDiff<maxLineDiff) {
                                maxJobDiff = maxLineDiff;
                                maxJobDiffLineOne = sumiOne.results.get(i);
                                maxJobDiffLineTwo = sumiTwo.results.get(i);
                            }
                            if (maxJobPctDiff<maxLinePctDiff) {
                                maxJobPctDiff = maxLinePctDiff;
                                maxJobPctDiffLineOne = sumiOne.results.get(i);
                                maxJobPctDiffLineTwo = sumiTwo.results.get(i);
                            }
                            errcount++;
                        }
                    }
                }
                System.out.println("");
				if(errcount==0) {
					System.out.println("Found no differences between summaries");
                } else {
					System.out.println("Found "+errcount+" differences between summaries");
                    System.out.println("max abs change is "+ maxJobDiff + " at line "+maxJobDiffLineOne.linenum);
                    System.out.println(maxJobDiffLineOne.text);
                    System.out.println(maxJobDiffLineTwo.text);
                    System.out.println("max % change is "+ 100.0*maxJobPctDiff+ " at line "+maxJobPctDiffLineOne.linenum);
                    System.out.println(maxJobPctDiffLineOne.text);
                    System.out.println(maxJobPctDiffLineTwo.text);
                }
                }
			catch (IOException e)
				{
				System.out.println(e);
				System.out.println("Error parsing files");
				}
			}
		 boolean hasEmptyFields(SummaryInfo sumi)
			 {
			 return (sumi.numAnalyzed ==0 ||
					 sumi.numPerms == 0 ||
					 sumi.summaryVersion == 0 ||
					 sumi.results.size() == 0
					 );
			 }
		 void fillInfo(String filename,SummaryInfo sumi) throws IOException
			{
			if(sumi==null)
				return;
			BufferedReader inputStream = new BufferedReader( new FileReader(filename));
                inputStream.readLine();	//throw away first 2 lines
				inputStream.readLine();
				String line = inputStream.readLine();
                int linenum = 3;
                //while((line = inputStream.readLine()) != null)
				//	{ }
					//SummaryInfo sumi = new SummaryInfo();
					StringTokenizer tokens = new StringTokenizer(line," ");
					String str = removeText(tokens,"Summary Version: ");
					sumi.summaryVersion = Double.parseDouble(str);

					while((line=inputStream.readLine()) != null ) {
                        linenum++;
                        if  ((str=removeText((new StringTokenizer(line," ")),"Number of proteins analyzed: ")) != null)
                        {
                            break;
                        }
                    }
					if(line==null)
						return;
					sumi.numAnalyzed = Double.parseDouble(str);
					while((line=inputStream.readLine()) != null ) {
                        linenum++;
                        if  ((str=removeText((new StringTokenizer(line," ")),"Number of permutations per protein: ")) != null)
                        {
                            break;
                        }
                    }
					if(line==null)
						return;
					sumi.numPerms = Double.parseDouble(str);
                sumi.results = new Vector<SummaryLine>();
                while((line=inputStream.readLine()) != null ) {
                  linenum++;
                  sumi.results.add(new SummaryLine(line,linenum));
                }

            }
		String removeText(StringTokenizer tok,String text)
			{
			StringTokenizer tt = new StringTokenizer(text," ");
			int count = tt.countTokens();
			for(int i=0;i<count;i++)
				{
				if(!tok.hasMoreTokens() || !tt.hasMoreTokens())
					return null;
				if(!tok.nextToken().equals(tt.nextToken()))
					{
					return null;
					}
				}
			return tok.nextToken("\n");
			}
		}
	}

