// package pepc;

import org.json.JSONStringer;
import org.json.JSONException;
import java.io.*;
import java.util.*;
import java.text.*;
import java.awt.geom.Rectangle2D;

//
// Copyright (c) 2007,2008 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Nate Heinecke
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA




class Experiment
{
    private int nPermutationsTried=0;  // in the bioreps=4 case, this ends up at 35
    private int nMaxPermutations; // randomly select if needed so as to not exceed this 
    private double[][] overallFilterPassCount; // indexed by GStatistic cutoff and PVALUE cutoff (see GStatisticCutoffs and PValueCutoffs for these)
    private double[][] nullhypothesisFilterPassCount; // indexed by GStatistic cutoff and PVALUE cutoff
    private  Vector<Vector<Vector<Protein>>> nullhypothesisFilterPassProteins; // proteins passing GStatistic Pvalue cutoffs
    private final DoubleList GStatisticCutoffs;   // list of GStatistic cutoffs
    private final DoubleList PValueCutoffs;  // list of PValue cutoffs
    private double FDRcutoff; // FDR above which we're not interested
    private final String expName;
    private final String inputFile;     // CSV input file

    public static double MAX_REASONABLE_FDR = .20;
    public static double FDR_SLIDER_PRECISION = .01; // slider accurate 

    private final Vector<Protein> proteinList;
    private final Vector<String> hdrNames; // names of header columns in input file
    private final Vector<Class> hdrClasses; // display as string or double?
    private final Vector<String> groupNames; // ie the "A" and "B" in samples A1,A2,A3,A4,B1,B2,B3,B4
    private String project_path;    //path to the current "project," where files will be created
    private final static String jsonCallback="here";

    //testType:
    //		0:		no test, normal run
    //      1:		do regression test    	( -t )
    //		2: 		set up regression test	( -t! )
    //		3:		no test, normal run with JSON
     
    Experiment(String name,SimpleProteinPopulata simpleLoader,double[] gstatistic_range, double[] ttest_range,int maxPerms, String outdir,int testType)
    {
        this.expName = name;
        this.nMaxPermutations=maxPerms; // randomly select if needed so as to not exceed this
        //PARSE INPUT FILES
        System.out.print("Parsing Proteins...");

        this.inputFile = simpleLoader.getFilename();
        GStatisticCutoffs = new DoubleList(gstatistic_range);
        PValueCutoffs = new DoubleList(ttest_range);
        proteinList = simpleLoader.getProteinList();
        hdrNames = simpleLoader.getHdrNames();
        groupNames = simpleLoader.getGroupNames();
        // examine dynamic columns, treat as numbers if they are that
        hdrClasses = new Vector<Class>();
        for (int c = 0;c<getHdrNames().size();c++) {
            Boolean allDouble = true; // until we see one that isn't
            for (Protein p:getProteins()) {
                try {
                    Double.parseDouble(p.getProteinInfo().get(c));
                } catch(NumberFormatException nfe) {
                    allDouble = false;
                    break;
                }
            }
            if (allDouble) {  // set the data type for proper sorting
                hdrClasses.add(Double.class); // show as double
            } else {
                hdrClasses.add(String.class); // show as string double
            }
        }
        System.out.println(" OK");

        int nBioReps = simpleLoader.getReplicateCount();

        // construct the null hypothesis, all A vs all B
        Combination leftside_nullhyp=new Combination(nBioReps);
        for (int i = nBioReps; i--> 0;) {
            leftside_nullhyp.setAt(i,i);
        }
        Combination rightside_nullhyp=leftside_nullhyp.inverse();
        CombinationPair nullhyp = new CombinationPair(leftside_nullhyp, rightside_nullhyp);

        System.out.print("Calculating false discovery rates...");
        //CALCULATE TRUE DISCOVERY RATE / FALSE DISCOVERY RATE
        calculateDiscoveryRates(nBioReps,nullhyp);
        // set FDR threshold for display purposes - midway of reasonable range
        setFDRCutoff(MAX_REASONABLE_FDR/2);

        System.out.println(" OK");

        //MAKE DIRECTORY AND OUTPUT FILES
        System.out.print("Outputting Files... ");
		
		if ((outdir != null) && !(new File("filename")).exists())
			(new File(outdir)).mkdirs();

        if(outdir == null)
            project_path=expName;
        else if(testType ==3)        //JSON output only creates one file, no folders
            project_path=outdir;
        else
            project_path=outdir+"/"+expName;


        makeOutputFiles(testType);
        System.out.println("OK");
    }

    public String getName() {
        return this.expName;
    }

    public String getInputFileName() {
        return this.inputFile;
    }


    // return FDR above which we're not interested
    public double getFDRCutoff() {
        return FDRcutoff;
    }

    public String getFDRCutoffString() {
        return new String (String.format("%.2f",getFDRCutoff()*100));
    }

    public void setFDRCutoff(double FDR) {
        FDRcutoff = FDR;
    }

    // return protein info column names from input file
    public Vector<String> getHdrNames() {
        return hdrNames;
    }

    public Vector<Class> getHdrClasses() {
        return hdrClasses;
    }

    public DoubleList getGStatisticCutoffs() {
        return GStatisticCutoffs;
    }

    public DoubleList getPValueCutoffs() {
        return PValueCutoffs;
    }

    public double getNullhypothesisFilterPassCount(int G,int P) {
        return nullhypothesisFilterPassCount[G][P];
    }
    
    //write the file used for GNUplots
    void writeStuff() throws IOException
    {
        File file = new File(project_path+"/pepcResults.txt");
        FileWriter fwriter = new FileWriter(file);
        WriteCutoffs(fwriter);
        fwriter.close();
    }

    void WriteCutoffs(FileWriter fwriter) throws IOException {
        for(int gi=0;gi< GStatisticCutoffs.size();gi++)
        {
            for(int pi=0;pi< PValueCutoffs.size();pi++)
            {
                String str = GStatisticCutoffs.get(gi) + "\t" + PValueCutoffs.get(pi) + "\t" +this.getTPbyIndex(gi,pi)+ "\t" +this.getFDRbyIndex(gi,pi) +"\n";
                fwriter.write(str);
            }
            fwriter.write("\n"); // blank line to seperate "scans" for pm3d surface plots
        }
    }

    int getGStatisticCutoffsCount(Rectangle2D subset) {
        if (subset!=null) {
           int low = (int)Math.max(0,subset.getMinX());
           int hi = (int)Math.min(GStatisticCutoffs.size()-1,subset.getMaxX());
           if (low <= hi) {
               return 1+(hi-low);
           }
        }
        return GStatisticCutoffs.size();
    }
    double getGStatisticCutoffByIndex(int index) {
        return GStatisticCutoffs.get(index);
    }
    int getPValueCutoffsCount(Rectangle2D subset) {
        if (subset!=null) {
           int low = (int)Math.max(0,subset.getMinY());
           int hi = (int)Math.min(PValueCutoffs.size()-1,subset.getMaxY());
           if (low <= hi) {
               return 1+(hi-low);
           }
        }
        return PValueCutoffs.size();
    }
    double getPValueCutoffByIndex(int index) {
        return PValueCutoffs.get(index);
    }

    //fill in a table with the FDRs and TDRs of a bunch of different cutoff values likely to be interesting
    void calculateDiscoveryRates(int nBioReps,CombinationPair nullhyp)
    {
        if(proteinList == null)
            return;

        this.overallFilterPassCount = new double[this.GStatisticCutoffs.size()][this.PValueCutoffs.size()];
        this.nullhypothesisFilterPassCount = new double[this.GStatisticCutoffs.size()][this.PValueCutoffs.size()];
        this.nullhypothesisFilterPassProteins = new Vector<Vector<Vector<Protein>>>();
        for (int gi = 0;gi<this.GStatisticCutoffs.size();gi++) {
            this.nullhypothesisFilterPassProteins.add(new Vector<Vector<Protein>>());
            for (int pi=0;pi<this.PValueCutoffs.size();pi++) {
                this.nullhypothesisFilterPassProteins.elementAt(gi).add(new Vector<Protein>());
                this.overallFilterPassCount[gi][pi] = 0;
                this.nullhypothesisFilterPassCount[gi][pi] =0;
            }
        }

        // get the false discovery rate
        // build the list of permutations
        // don't do the mirror cases (1235:4678 is same as 4678:1235)
        CombinationGenerator combos = new CombinationGenerator(nBioReps);

        int [] groupsCount = new int[nBioReps]; // for tracking how many of each kind of permutation we've done
        BitSet[] selections = new BitSet[nBioReps]; // for randomly selection perms

        double selectionRate = (double)this.nMaxPermutations/combos.getTotal().doubleValue();
        if (selectionRate < 1.0) {
            System.out.println("Randomly sampling "+
                    Math.round(combos.getTotal().doubleValue()*selectionRate) +
                    " ("+(int)Math.ceil(selectionRate*100.0)+"%) of "+combos.getTotal().intValue()+
                    " data permutations...");
        } else {
            System.out.println("Processing "+combos.getTotal().intValue()+" data permutations...");
        }
        for (int order=0;order<nBioReps;order++) {
            int orderSize = combos.getOrderSize(order);
            selections[order] = new BitSet(orderSize);
            if (selectionRate < 1.0) {
                // make a random selection
                Random r = new Random(orderSize);
                int nSelect = 1 + (int)((double)orderSize * selectionRate);
                nSelect = Math.min(nSelect,orderSize); // watch for rounding
                for (int i=nSelect;i>0;) {
                    int slot = r.nextInt(orderSize);  // return range 0...(orderSize-1)
                    if (!selections[order].get(slot)) { // random may repeat
                        selections[order].set(slot);
                        i--;
                    }
                }
            } else { // select all
                selections[order].set(0,orderSize); // set 0-(orderSize-1) true
            }
        }

        double progress=0;
        int last_pct = 0;
        while (combos.hasMore()) {
            CombinationPair pair = combos.getNext();
            int order = pair.getOrder();
            int pctdone = (int)(100.0*++progress/combos.getTotal().doubleValue());
            if (pctdone != last_pct) {
                last_pct = pctdone;
                System.out.print(((last_pct%5)==0)?(last_pct+"%"):".");
            }
            // was this permutation randomly selected to represent its order type?
            // note, null hypothesis is always selected since it's the only one of its order
            if (selections[order].get(groupsCount[order]++)) {
                this.nPermutationsTried++;
                boolean bNullHyp = pair.equals(nullhyp);
                for(Protein prot: this.proteinList)
                {  // for all proteins
                    ProteinPermutation perm = prot.doPermutation(pair);
                    if (bNullHyp) {
                        prot.nullHypothesis = perm;
                    }
                    for(int gi=0;gi<GStatisticCutoffs.size();gi++)
                    {
                        if (perm.getGStatistic() >=  GStatisticCutoffs.get(gi))
                        {
                            for(int pi=PValueCutoffs.size();pi-->0;)
                            {
                                if (perm.getPValue() <= PValueCutoffs.get(pi))
                                {
                                    this.overallFilterPassCount[gi][pi]++;
                                    if (bNullHyp)
                                    {
                                        this.nullhypothesisFilterPassCount[gi][pi]++;
                                        this.nullhypothesisFilterPassProteins.elementAt(gi).elementAt(pi).add(prot); // note for later reporting
                                    }
                                } else { // everything past here is even bigger
                                    break;
                                }
                            } // end for each PValue
                        } else { // everything past here is even smaller
                           break;
                        } // end if gstatistic passes cutoff
                    } // end for each gstatistic
                } // end for each protein
            } // end if we haven't already done a bunch of this order
        } // end for each permutation
    } // end calculateDiscoveryRates

    public int getGStatisticIndex(double GStatistic) {
        return GStatisticCutoffs.getIndex(GStatistic);
    }

    public int getPValueIndex(double PValue) {
        return PValueCutoffs.getIndex(PValue);
    }

    double getFPbyIndex(int GStatisticIndex,int PTestIndex) {
        return this.overallFilterPassCount[GStatisticIndex][PTestIndex] / (double) this.nPermutationsTried;
    }

    public double getTPbyIndex(int GStatisticIndex,int PTestIndex) {
        double FP = getFPbyIndex(GStatisticIndex, PTestIndex);
        double nullHypPassCount = this.nullhypothesisFilterPassCount[GStatisticIndex][PTestIndex];
        double TP = (nullHypPassCount -  FP);
        return Math.max(TP,0.0);
    }

    public double getFDRbyIndex(int GStatisticIndex,int PTestIndex) {
        double FP = getFPbyIndex(GStatisticIndex, PTestIndex);
        double TP = getTPbyIndex(GStatisticIndex, PTestIndex);
        double FDR = ((FP + TP) != 0)? (FP / (FP + TP)) : 1.0;
        return (FDR);
    }

    Vector<Protein> getProteins() {
        return this.proteinList;
    }

    // get a vector of suitable-for-display strings describing proteins passing this cutoff
    Vector<Protein> getProteinsByCutoffIndices(int GstatisticIndex,int pvalueIndex) {
        Vector<Protein> result = new Vector<Protein>();
        for (Protein p:this.nullhypothesisFilterPassProteins.get(GstatisticIndex).get(pvalueIndex)) {
           result.add(p);
        }
        return result;
    }

    //Writes the list of proteins used by PepcResults
    void writeProteins(FileWriter fwriter) throws IOException
    {
        for(Protein p:this.proteinList)
        {
            String str="";
            for (String s:p.getProteinInfo()) {
                str+=s + "\t";
            }
            str+=p.getGStatistic()+"\t"+p.getPValue()+"\n";
            fwriter.write(str);
        }
    }

    //  Writes a summary output file.
    //  These are also used for regression testing
    void writeSummary(String sname) throws IOException
    {
        File file = new File(project_path+"/"+sname);
        FileWriter fwriter = new FileWriter(file);
        fwriter.write("Pepc Summary File:\n\n");
        fwriter.write("Summary Version: 1.0\n");
        fwriter.write("Analysis name: "+this.expName+"\n");

        Date now = new Date();
        DateFormat df =  DateFormat.getDateInstance();
        fwriter.write("Summary file created on "+df.format(now)+"\n");
        fwriter.write("Input File: \n\n");
        fwriter.write(this.inputFile+"\n");
        fwriter.write("\n");
        fwriter.write("Number of proteins analyzed: " + this.proteinList.size() + "\n");
        fwriter.write("Number of permutations per protein: " + this.nPermutationsTried + "\n");
        fwriter.write("\n");
        fwriter.write("Proteins\n");
        writeProteins(fwriter);
        fwriter.write("\n");
        fwriter.write("Cutoffs\n");
        WriteCutoffs(fwriter);
        fwriter.close();
    }

    void makePlots() throws IOException
    {
        Process proc = Runtime.getRuntime().exec("GNUplot");
        if(proc==null)
        {
            System.out.println("Error opening GNUplot- it may not be installed or else path variable is not set");
            System.out.println("Cannot create sexy graphs");
            return;
        }
        OutputStream os = proc.getOutputStream();
        PrintStream ps = new PrintStream(os);

        makeCutoffPlotFile(ps);

        ps.close();
    }
    void makeCutoffPlotFile(PrintStream ps)
    {
        ps.println("reset");
        ps.println("clear");
        ps.println("cd \""+project_path+"\"");
        ps.println("set term png large size 960,720");
        ps.println("set output \"CutoffsSurfPlot.png\"");
        ps.println("set border");
        ps.println("set grid");
        ps.println("set log yzcb");
        ps.println("set pm3d at s");
        ps.println("set title \"TP and FDR by G-statistic & T-Test p-value Cutoffs\"");
        ps.println("set xlabel \"G-statistic\"");
        ps.println("set ylabel \"p-value\"");
        ps.println("set label 1 \"TP\" center rotate by 90 at graph 0, graph 0, graph 0.5 offset -5");
        ps.println("set key off");
        ps.println("splot \"pepcResults.txt\"");
        ps.println("reset");
        ps.println("set term png large size 960,720");
        ps.println("set output \"Cutoffs.png\"");
        ps.println("set border");
        ps.println("set log ycb");
        ps.println("set title \"FDR by G-statistict & T-Test p-value Cutoffs\"");
        ps.println("set xlabel \"G-statistic\"");
        ps.println("set ylabel \"p-value\"");
        ps.println("splot \"pepcResults.txt\" with view");
        ps.flush();
    }

    void createOwnDirectory()
    {
        new File(project_path).mkdirs();
    }
    void makeOutputFiles(int testType)
    {
        try {
           
            if(testType==2)
                {
                createOwnDirectory();
                writeStuff();
                writeSummary("regTest.txt");
                writeSummary("pepcSummary.txt");
                File file = new File(project_path+"/pepcResults3.txt");
                FileWriter fwriter = new FileWriter(file);
                writeProteins(fwriter);
                fwriter.close();
                }
            else if(testType==3)    //JSON Output
                {
                writeStuff();
                File file = new File(project_path+"/"+expName+".js");
                FileWriter fwriter = new FileWriter(file);
                String jstr=toJSON();
                if(jstr != null)
                    {
                    //this must match the callback function specified in the GWT receiving code
                    fwriter.write(jsonCallback+'(');
                    fwriter.write(jstr);
                    fwriter.write(");");
                    }
                fwriter.close();
                }
            else
                {
                createOwnDirectory();
                writeStuff();
                writeSummary("pepcSummary.txt");
                File file = new File(project_path+"/pepcResults3.txt");
                FileWriter fwriter = new FileWriter(file);
                writeProteins(fwriter);
                fwriter.close();
                }

            // makePlots();       done with gnuplot now
        }
        catch(Exception e)
        {
            System.out.println(e);
            System.out.println("Error writing output files");
        }
    }
    //Serialize into json for output to GWT interface
    String toJSON()
        {
        JSONStringer ret = new JSONStringer();
        try {
            ret.object();
                ret.key("Array_by_GStatistic");
                ret.array();//--***&&&
                for(int i=0;i<GStatisticCutoffs.size();i++)
                    {
                    ret.object();   //---***
                    ret.key("GStatistic");
                    ret.value(GStatisticCutoffs.get(i));
                    ret.key("Array_by_TTest");
                    ret.array();//---
                        for(int ii=0;ii<PValueCutoffs.size();ii++)
                            {
                            ret.object();
                                ret.key("TTest");   //TTest is the same as PValues 
                                double tt = PValueCutoffs.get(ii);
                                //if(Double.isNaN(tt))
                                //    tt=9999;
                                ret.value(tt);
                                ret.key("Pos");
                                ret.value(getNullhypothesisFilterPassCount(i,ii));
                                ret.key("FPos");  //False Discovered
                                ret.value(getFPbyIndex(i,ii));
                                //ret.key("Extra_Info");      //optional column info
                            ret.endObject();
                            }
                    ret.endArray();//---
                    ret.endObject();//---***
                    }
                ret.endArray();//--***&&&

                ret.key("Headers");
                ret.array();
                ret.value("IPI");
                ret.value("SC Avg (A)");
                ret.value("SC Avg (B)");
                ret.value("G-Statistic");
                ret.value("T-Test");
                ret.endArray();

                ret.key("ProteinList");
                ret.array();//---
                    for (Protein p:proteinList)
                        {
                        if(p!=null)
                            {
                            ret.object();
                                ret.key("PValue");
                                double pv = p.getPValue();
                                //if(Double.isNaN(pv))
                                //    pv=9999;    //apparently this is necessary because of divide by zero errors so...
                                ret.value(pv);
                                double gt = p.getGStatistic();
                                //if(Double.isNaN(gt))
                                //    gt=9999;     //pretend you didn't see this...
                                ret.key("GStatistic");
                                ret.value(gt);
                                ret.key("Protein_Info");
                                ret.array();
                                    if((p.getProteinInfo() != null) && p.getProteinInfo().size() > 0)
                                        for (String str:p.getProteinInfo())
                                            {
                                            ret.value(str);
                                            }
                                    else
                                        {
                                        ret.value("Unknown_Protein");
                                        }
                                ret.endArray();

                            ret.endObject();
                            }
                        }
                ret.endArray();//---

            ret.endObject();
            }
        catch(JSONException e)
            {
            System.out.println("Error :"+e.toString());
            return null;
            }
        return ret.toString();
        }
}
