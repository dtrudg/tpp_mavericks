
// package pepc;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

//
// Copyright (c) 2008 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Brian Pratt
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA

//
// a sorted list of nonnegative double primitives
//
public class DoubleList {
   private final double[] contents;

   DoubleList(double[] vals) {
       contents = new double[vals.length];
       for (int i=vals.length;i-->0;) {
           contents[i] = Math.abs(vals[i]);
       }
       Arrays.sort(contents);
   }

   double get(int i) {
       return contents[i];
   }
   String [] toStringArray() {
       String[] ret = new String[this.size()];
       for (int i=0;i<this.size();i++) {
           ret[i] = (new Double(get(i))).toString();
       }
       return ret;
   }
   int size() {
       return contents.length;
   }
   int getIndex(double d) {
       int index= Arrays.binarySearch(contents,d);

        if (index < 0) { // no exact match, returns -(insertionpoint+1)
            index = -(index+1);
        }
        if (index >= size()) {
            index = size()-1;
        }

        return index;
   }

}
