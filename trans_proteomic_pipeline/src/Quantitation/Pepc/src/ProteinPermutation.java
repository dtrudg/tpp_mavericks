//
// Copyright (c) 2007,2008 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Nate Heinecke
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA

// package pepc;


class ProteinPermutation
	{

    private final double pValue;
    private final double GStatistic;

	ProteinPermutation(double gt,double pv)
    {
        this.GStatistic=gt;
        this.pValue=pv;
    }

    double getGStatistic()
		{
		return GStatistic;
		}

	public double getPValue()
		{
		return pValue;
		}

	}

