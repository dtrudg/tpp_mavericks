/* -------------------
 *   PepcViewer.java
 *
 *  jfreechart based clickable SpectralCount results viewer
 *
 *
 */

//
// Copyright (c) 2008 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Brian Pratt
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA
// package pepc;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Vector;
import java.util.Hashtable;
import java.text.DecimalFormat;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import org.jfree.chart.*;
import org.jfree.chart.title.PaintScaleLegend;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.axis.*;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.*;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.renderer.xy.XYBlockRenderer;
import org.jfree.chart.renderer.PaintScale;
import org.jfree.chart.renderer.GrayPaintScale;
import org.jfree.data.Range;
import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYZDataset;
import org.jfree.ui.*;


/**
 * A clickable Pepc results viewer.
 */
class  PepcViewer extends ApplicationFrame {

    private static class PepcAxis extends SymbolAxis {
        PepcAxis(String name,String []valstrs) {
            super(name,valstrs);
        }
        public void setRange(Range range) {
            // insist on integer values, then add the half block edge overlap
            double l = Math.floor(range.getLowerBound()+.5);
            l = Math.max(l,0);
            double u = Math.floor(range.getUpperBound()+.5);
            u = Math.min(u,this.getSymbols().length-1);
            Range intRange = new Range(l-.5,u+.5);
            super.setRange(intRange);
        }
    }

    private static class PepcPaintScaleLegend extends PaintScaleLegend {
        public final Experiment experiment;
        PepcPaintScaleLegend(PepcPaintScale scale, NumberAxis axis, Experiment e) {
            super(scale,axis);
            experiment = e;
        }
        void updateRange(Rectangle2D range) {   // rectange is bounds of gstatistic,pvalue space
            PepcPaintScaleLegend noob = createHeatMapLegend(experiment,range);
            this.setScale(noob.getScale());
            this.setAxis(noob.getAxis());
        }
    }


    private static class PepcPaintScale extends GrayPaintScale {
        final Experiment experiment;
        static public final double EXCESS_FDR_MAGIC = -99999.8877;
        PepcPaintScale(Range range, Experiment e) {
            super(range.getLowerBound(),range.getUpperBound());
            experiment = e;
        }
        public Paint getPaint(double value) {
            if (value == EXCESS_FDR_MAGIC) {
                return Color.white;
            }
            double v = value;
            double L = this.getLowerBound();
            double U = this.getUpperBound();
            double range = U-L;
            v = Math.max(v, L);
            v = Math.min(v, U);
            double offset = v-L;
            double gray = (offset / range);
            // this craziness is the gnuplot default gray->rgb conversion
            int r = (int)(255.0 * Math.sqrt(gray));
            int g = (int)(255.0 * (gray*gray*gray));
            int b = (int)(255.0 * Math.sin(2*gray*Math.PI));
            return new Color(r,g,(b<0)?0:b);
        }
    }


    private static class PepcXYBlockRenderer extends XYBlockRenderer {
        public final Experiment experiment;
        public PepcXYBlockRenderer(Experiment e) {
            super();
            experiment = e;
        }
    }

    private static class PepcChartDataset implements XYZDataset  {
        final Experiment experiment;
        final Rectangle2D GStatisticPValueRange;
        PepcChartDataset(Experiment e,Rectangle2D GPRange) {
            experiment = e;
            if (GPRange!=null) {
                GStatisticPValueRange = GPRange.createIntersection(new Rectangle2D.Double(0,0,
                        experiment.getGStatisticCutoffsCount(null)-1,experiment.getPValueCutoffsCount(null)-1));
            } else {
                GStatisticPValueRange = null;
            }
        }
        public Experiment getExperiment() {
            return experiment;
        }
        public int getSeriesCount() {
            return experiment.getGStatisticCutoffsCount(GStatisticPValueRange);
        }
        public int getItemCount(int series) {
            return experiment.getPValueCutoffsCount(GStatisticPValueRange);
        }
        public Number getX(int series, int item) {
            return getXValue(series, item);
        }
        public double getXValue(int series, int item) {
            return series; // we present a simple integer space to jfreechart
        }
        public Number getY(int series, int item) {
            return getYValue(series, item);
        }
        public double getYValue(int series, int item) {
            return item; // we present a simple integer space to jfreechart
        }
        public Number getZ(int series, int item) {
            return getZValue(series, item);
        }
        // return Z value subject to FDR cutoff
        public double getZValue(int series, int item) {
            double FDR=experiment.getFDRbyIndex(series,item);
            if (FDR>experiment.getFDRCutoff()) {
                return PepcPaintScale.EXCESS_FDR_MAGIC;
            }
            return getTrueZValue(series,item);

        }
        // return actual Z value
        public double getTrueZValue(int series, int item) {
            return experiment.getTPbyIndex(series,item);
        }
        public Range getZRange() {
            int seriesHi=getSeriesCount();
            int seriesLo = 0;
            int itemHi=getItemCount(0);
            int itemLo = 0;
            if (this.GStatisticPValueRange!=null) {   // for zoom purposes
                seriesHi = (int)GStatisticPValueRange.getMaxX()+1;
                seriesLo = (int)GStatisticPValueRange.getMinX();
                itemHi = (int)GStatisticPValueRange.getMaxY()+1;
                itemLo = (int)GStatisticPValueRange.getMinY();
            }
            double lower = PepcPaintScale.EXCESS_FDR_MAGIC;
            double upper = lower;
            for (int i=seriesHi;i-->seriesLo;) {
                for (int j=itemHi;j-->itemLo;) {
                    double z = getZValue(i,j);
                    if (z != PepcPaintScale.EXCESS_FDR_MAGIC) {
                        if (z<=0.0) { // values <=0 don't display well in log axis!
                            z = LogarithmicAxis.SMALL_LOG_VALUE; // just an epsilon greater than 0.0
                        }
                        if (lower == PepcPaintScale.EXCESS_FDR_MAGIC) {
                            lower = z;
                        } else {
                            lower = Math.min(lower,z);
                        }
                        if (upper == PepcPaintScale.EXCESS_FDR_MAGIC) {
                            upper = z;
                        } else {
                            upper = Math.max(upper,z);
                        }
                    }
                }
            }
            if (lower == PepcPaintScale.EXCESS_FDR_MAGIC) {
                return new Range(0,1); // nothing cleared FDR threshold
            } else {
                return new Range(lower,upper);
            }
        }
        public void addChangeListener(DatasetChangeListener listener) {
            // ignore - this dataset never changes
        }
        public void removeChangeListener(DatasetChangeListener listener) {
            // ignore
        }
        public DatasetGroup getGroup() {
            return null;
        }
        public void setGroup(DatasetGroup group) {
            // ignore
        }
        public String getKey() {
            return "TP";
        }
        public Comparable getSeriesKey(int series) {
            return getKey();
        }
        public int indexOf(Comparable seriesKey) {
            return 0;
        }
        public DomainOrder getDomainOrder() {
            return DomainOrder.ASCENDING;
        }

    }
    
        /**
         * Creates the legend for the plot for FDR or TP.
         *
         * @return The legend.
         */
        private static PepcPaintScaleLegend createHeatMapLegend(Experiment experiment,Rectangle2D gStatisticPvalueRange) {
            PepcChartDataset dataset = new PepcChartDataset(experiment,gStatisticPvalueRange);
            Range range = dataset.getZRange();
            NumberAxis scaleAxis = new NumberAxis(dataset.getKey());
            scaleAxis.setRange(range);
            scaleAxis.setAxisLinePaint(Color.white);
            scaleAxis.setTickMarkPaint(Color.white);
            scaleAxis.setTickLabelFont(new Font("Dialog", Font.PLAIN, 7));
           PepcPaintScaleLegend legend = new PepcPaintScaleLegend(new PepcPaintScale(range,experiment),
                    scaleAxis, experiment);
            legend.setAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
            legend.setAxisOffset(5.0);
            legend.setMargin(new RectangleInsets(5, 5, 5, 5));
            legend.setFrame(new BlockBorder(Color.red));
            legend.setPadding(new RectangleInsets(10, 10, 10, 10));
            legend.setStripWidth(10);
            legend.setPosition(RectangleEdge.LEFT);
            legend.setBackgroundPaint(new Color(120, 120, 180));
            return legend;
        }

    private static class PepcChartPanel extends ChartPanel {
        final PepcPanel panel;

        public PepcChartPanel(JFreeChart c,PepcPanel p) {
            super(c, true, true, true, false, true);
            panel = p;
        }
        public String getToolTipText(MouseEvent e) {
            PepcCutoffInfo bi = panel.chartMouseEventBlockInfo(e.getX(),e.getY());
            if (bi!=null) {
                return bi.smalltext;
            } else {
                return null;
            }
        }


        private void zoomHeatmapRange() {  // adjust heatmap to use only currently displayed range

            ChartRenderingInfo chartinfo = this.getChartRenderingInfo();
            PlotRenderingInfo info = chartinfo.getPlotInfo();
            Rectangle2D dataArea = info.getDataArea();
            XYPlot plot = (XYPlot)this.getChart().getPlot();
            int GTIndexA = (int)Math.floor(plot.getDomainAxis().java2DToValue(dataArea.getMinX(), dataArea,
                    plot.getDomainAxisEdge()));
            int GTIndexB = (int)Math.ceil(plot.getDomainAxis().java2DToValue(dataArea.getMaxX(), dataArea,
                    plot.getDomainAxisEdge()));
            int PVIndexA = (int)Math.ceil(plot.getRangeAxis().java2DToValue(dataArea.getMinY(), dataArea,
                    plot.getRangeAxisEdge()));
            int PVIndexB = (int)Math.floor(plot.getRangeAxis().java2DToValue(dataArea.getMaxY(), dataArea,
                    plot.getRangeAxisEdge()));
            Rectangle2D zoomGStatisticPValue = new Rectangle2D.Double(Math.min(GTIndexA,GTIndexB),Math.min(PVIndexA,PVIndexB),
                    Math.abs(GTIndexB-GTIndexA),Math.abs(PVIndexB-PVIndexA));
            // update the legends...
            //PepcPaintScaleLegend ps = (PepcPaintScaleLegend)(getChart().getSubtitle(0));
            //ps.updateRange(zoomGStatisticPValue);
            // and update the plot
            PepcXYBlockRenderer renderer = (PepcXYBlockRenderer)plot.getRenderer();
            PepcChartDataset dataset = new PepcChartDataset(panel.getExperiment(),zoomGStatisticPValue);
            PepcPaintScale scale = new PepcPaintScale(dataset.getZRange(),renderer.experiment);
            renderer.setPaintScale(scale);
        }


        public void restoreAutoBounds() {  // unzoom - but don't rename it!  inheritance, you know
            super.restoreAutoBounds();
            XYPlot p = (XYPlot)this.getChart().getPlot();
            ChartRenderingInfo chartinfo = this.getChartRenderingInfo();
            PlotRenderingInfo info = chartinfo.getPlotInfo();
            if (p != null) {
                p.zoomDomainAxes(0.0, info, new Point());
                p.zoomRangeAxes(0.0, info, new Point());
            }
            zoomHeatmapRange(); // redo the heatmap colors
        }
        public void zoom(Rectangle2D selection) {
            PlotRenderingInfo plotInfo = this.getChartRenderingInfo().getPlotInfo();
            Rectangle2D scaledDataArea = getScreenDataArea(
                    (int) selection.getCenterX(), (int) selection.getCenterY());
            if ((selection.getHeight() > 0) && (selection.getWidth() > 0)) {

                double hLower = (selection.getMinX() - scaledDataArea.getMinX())
                    / scaledDataArea.getWidth();
                double hUpper = (selection.getMaxX() - scaledDataArea.getMinX())
                    / scaledDataArea.getWidth();
                double vLower = (scaledDataArea.getMaxY() - selection.getMaxY())
                    / scaledDataArea.getHeight();
                double vUpper = (scaledDataArea.getMaxY() - selection.getMinY())
                    / scaledDataArea.getHeight();

                Plot p = this.getChart().getPlot();
                if (p instanceof Zoomable) {
                    Zoomable z = (Zoomable) p;
                    Point2D source = new Point(-10000,-10000); // not on any plot, so it zooms them all
                    if (z.getOrientation() == PlotOrientation.HORIZONTAL) {
                        z.zoomDomainAxes(vLower, vUpper, plotInfo, source);
                        z.zoomRangeAxes(hLower, hUpper, plotInfo, source);
                    }
                    else {
                        z.zoomDomainAxes(hLower, hUpper, plotInfo, source);
                        z.zoomRangeAxes(vLower, vUpper, plotInfo, source);
                    }
                }
            }
            zoomHeatmapRange(); // adjust heatmaps to use only selected range
        }

    }
        private static class PepcPanel extends JPanel
                implements ChangeListener,
                ChartProgressListener,
                ChartMouseListener {
            private final Experiment experiment;
            private final PepcChartPanel chartPanel;
            private final TextTitle cutoffsReport;
            private final JSlider FDRcutoffSlider;
            private final Label FDRlabel;
            private final JFormattedTextField FDRedit;
            private final PepcResultsProteinReportTableModel proteinsReportTableModel;

            /**
             * Creates a new Pepc results viewer panel.
             */
            public PepcPanel(Experiment e) {
                super(new BorderLayout());
                experiment = e;
                XYPlot plot = createHeatMap(experiment);
                int preferred_xsize = 800;
                int preferred_ysize = 550;

                // return a new chart containing the overlaid plot...
                JFreeChart chart = new JFreeChart(
                        experiment.getName(), JFreeChart.DEFAULT_TITLE_FONT, plot, true  );
                chart.removeLegend();
                chart.setBackgroundPaint(this.getBackground());               
                //chart.addSubtitle(createHeatMapLegend(experiment,null)); // heat map legend for TP/FDR
                createHeatMapLegend(experiment,null);
                //chart.fireChartChanged();
                this.chartPanel = new PepcChartPanel(chart,this);
                this.chartPanel.setPreferredSize(new java.awt.Dimension(preferred_xsize, preferred_ysize));
                this.chartPanel.setDomainZoomable(true);
                this.chartPanel.setRangeZoomable(true);
                this.chartPanel.addChartMouseListener(this);
/*
                Border border = BorderFactory.createCompoundBorder(
                        BorderFactory.createEmptyBorder(4, 4, 4, 4),
                        BorderFactory.createEtchedBorder());
                this.chartPanel.setBorder(border);
*/                
                this.add(this.chartPanel, BorderLayout.WEST);

                // set up slider for FDR adjustment
                FDRcutoffSlider = new JSlider(JSlider.VERTICAL,0,
                        (int)(100.0*e.MAX_REASONABLE_FDR/e.FDR_SLIDER_PRECISION),
                        (int)(100.0*e.getFDRCutoff()/e.FDR_SLIDER_PRECISION));
                FDRcutoffSlider.setPaintTicks(true);
                FDRcutoffSlider.setMajorTickSpacing((int)((100.0*e.MAX_REASONABLE_FDR)/(10*experiment.FDR_SLIDER_PRECISION)));
                FDRcutoffSlider.setPaintLabels( true );

                //Create the label table
                Hashtable labelTable = new Hashtable();
                labelTable.put( new Integer( 0 ), new JLabel("0%") );
                Integer midpct = new Integer((int)(50.0*e.MAX_REASONABLE_FDR));
                labelTable.put( new Integer( (int)(50.0*e.MAX_REASONABLE_FDR/e.FDR_SLIDER_PRECISION) ), new JLabel(midpct.toString() + "%") );
                Integer maxpct = new Integer((int)(100.0*e.MAX_REASONABLE_FDR));
                labelTable.put( new Integer( (int)(100.0*e.MAX_REASONABLE_FDR/e.FDR_SLIDER_PRECISION) ), new JLabel(maxpct.toString() + "%") );
                FDRcutoffSlider.setLabelTable( labelTable );
                FDRcutoffSlider.addChangeListener(this);
                this.add(this.FDRcutoffSlider, BorderLayout.EAST);
                this.FDRlabel = new Label("Max FDR % =",Label.RIGHT);

                // set up an edit box for FDR value
                FDRedit = new JFormattedTextField(new DecimalFormat());
                PropertyChangeListener propertyChangeListener = new PropertyChangeListener() {
                  public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    String property = propertyChangeEvent.getPropertyName();
                    if ("value".equals(property)) {
                        double val = new Double(FDRedit.getValue().toString()).doubleValue()/100.0;
                        if ((val>=0.0) && (val <=1.0))
                        {
                            experiment.setFDRCutoff(val);
                            val = Math.min(val,experiment.MAX_REASONABLE_FDR);
                            FDRcutoffSlider.setValue((int)(100.0*val/experiment.FDR_SLIDER_PRECISION));
                            FDRcutoffSlider.repaint();
                            chartPanel.zoomHeatmapRange();
                            repaint(); // update
                        }
                    }
                  }
                };
                FDRedit.setValue(new Double(e.getFDRCutoffString()));
                FDRedit.setColumns(10);
                FDRedit.addPropertyChangeListener("value", propertyChangeListener);
                
	            JPanel panel = new JPanel();
                panel.add(FDRlabel);
                panel.add(FDRedit);
                panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
	            add(BorderLayout.NORTH, panel); // make sure you add the panel!
                JPanel proteinsReport = new JPanel(new BorderLayout());
                proteinsReport.setPreferredSize(new Dimension(preferred_xsize-100,
                        preferred_ysize/4));
                proteinsReport.setBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4));

                this.cutoffsReport = new TextTitle("...");
                this.cutoffsReport.setPosition(RectangleEdge.BOTTOM);

                this.proteinsReportTableModel = new PepcResultsProteinReportTableModel(experiment);
                JTable proteinsTable = new JTable(this.proteinsReportTableModel);
                proteinsTable.setAutoCreateRowSorter(true);  // sort on col header clicks

                TableCellRenderer renderer = new NumberCellRenderer();
                DoubleComparator dcomp = new DoubleComparator();
                // examine dynamic columns, treat as numbers if they are that
                for (int c = 0;c<experiment.getHdrNames().size();c++) {
                    if (experiment.getHdrClasses().get(c).equals(Double.class)) {  // set the data type for proper sorting
                        // show as double
                        proteinsTable.getColumnModel().getColumn(c).setCellRenderer(renderer); 
                        ((TableRowSorter)proteinsTable.getRowSorter()).setComparator(c,dcomp);
                    }
                }
                proteinsTable.getColumnModel().getColumn(getGStatisticColumnNum()).setCellRenderer(renderer);  // GStatistic
                ((TableRowSorter)proteinsTable.getRowSorter()).setComparator(getGStatisticColumnNum(),dcomp);
                proteinsTable.getColumnModel().getColumn(getPValueColumnNum()).setCellRenderer(renderer);  // PValue
                ((TableRowSorter)proteinsTable.getRowSorter()).setComparator(getPValueColumnNum(), dcomp);


                proteinsReport.add(new JScrollPane(proteinsTable));

                chart.addSubtitle(cutoffsReport);
                this.add(proteinsReport, BorderLayout.SOUTH);
                // now set the initial crosshair position
                PepcCutoffInfo bi = new PepcCutoffInfo(experiment,experiment.getGStatisticCutoffsCount(null)/2,experiment.getPValueCutoffsCount(null)/2);
                PepcUpdateCrossHairs(bi);
            }

            public Experiment getExperiment() {
                return experiment;
            }

            public int getGStatisticColumnNum() {
                return experiment.getHdrNames().size();
            }

            public int getPValueColumnNum() {
                return experiment.getHdrNames().size()+1;
            }



            /**
             * Creates the plot for TP/FDR.
             *
             * @return The plot.
             */
            private XYPlot createHeatMap(Experiment experiment) {

                PepcChartDataset dataset = new PepcChartDataset(experiment,null);
                Range range = dataset.getZRange();
                PepcAxis xAxis = new PepcAxis("G-statistic",experiment.getGStatisticCutoffs().toStringArray());
                //xAxis.setStandardTickUnits(PepcAxis.createLogTickUnits());
                xAxis.setLowerMargin(0.0);
                xAxis.setUpperMargin(0.0);
                xAxis.setAxisLinePaint(Color.white);
                xAxis.setTickMarkPaint(Color.white);
                xAxis.setGridBandsVisible(true);

                PepcXYBlockRenderer renderer = new PepcXYBlockRenderer(experiment);
                PaintScale scale = new PepcPaintScale(range,experiment);
                renderer.setPaintScale(scale);

                PepcAxis yAxis = new PepcAxis("p-value",experiment.getPValueCutoffs().toStringArray());
                yAxis.setGridBandsVisible(true);
                yAxis.setLowerMargin(0.0);
                yAxis.setUpperMargin(0.0);
                yAxis.setAxisLinePaint(Color.white);
                yAxis.setTickMarkPaint(Color.white);

                //renderer.setBlockAnchor(RectangleAnchor.BOTTOM_LEFT);
                XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
                plot.setBackgroundPaint(Color.lightGray);
                plot.setDomainGridlinesVisible(false);
                plot.setDomainGridlinePaint(Color.lightGray);
                plot.setRangeGridlinesVisible(false);
                plot.setRangeGridlinePaint(Color.lightGray);
                plot.setAxisOffset(new RectangleInsets(5, 5, 5, 5));
                plot.setOutlinePaint(Color.blue);
                plot.setDomainCrosshairPaint(Color.lightGray);
                plot.setDomainCrosshairVisible(true);
                plot.setDomainCrosshairLockedOnData(true);
                plot.setRangeCrosshairVisible(true);
                plot.setRangeCrosshairLockedOnData(true);
                plot.setRangeCrosshairPaint(Color.lightGray);

                plot.setOrientation(PlotOrientation.VERTICAL);

                return plot;
            }

            /**
             * Callback method for receiving notification of a mouse click on a
             * chart.
             *
             */
            public PepcCutoffInfo chartMouseEventBlockInfo(int x, int y) {

                // the following translation takes account of the fact that the
                // chart image may have been scaled up or down to fit the panel...
                Point2D p = this.chartPanel.translateScreenToJava2D(new Point(x, y));

                // now convert the Java2D coordinate to axis coordinates...
                XYPlot plot = (XYPlot)this.chartPanel.getChart().getPlot();
                ChartRenderingInfo chartinfo = this.chartPanel.getChartRenderingInfo();
                PlotRenderingInfo info = chartinfo.getPlotInfo();
                Rectangle2D dataArea = info.getDataArea();
                if (dataArea.contains(p)) {
                    int GTi = (int)(plot.getDomainAxis().java2DToValue(p.getX(), dataArea,
                            plot.getDomainAxisEdge())+0.5);

                    int PVi = (int)(plot.getRangeAxis().java2DToValue(p.getY(), dataArea,
                            plot.getRangeAxisEdge())+0.5);

                    JFreeChart ch = this.chartPanel.getChart();
                    if (ch != null) {
                        PepcChartDataset dataset = (PepcChartDataset)plot.getDataset();
                        return new PepcCutoffInfo(dataset.experiment,GTi,PVi);
                    }
                }
                return null;
            }

            public void chartMouseClicked(ChartMouseEvent event) {
                int x = event.getTrigger().getX();
                int y = event.getTrigger().getY();
                PepcCutoffInfo bi = chartMouseEventBlockInfo(x,y);
                if (bi != null) {
                    PepcUpdateCrossHairs(bi);
                }
            }
            public void PepcUpdateCrossHairs(PepcCutoffInfo bi) {
                if (bi != null) {
                    // update the table...
                    this.cutoffsReport.setText(bi.text);
                    this.proteinsReportTableModel.updateTable(bi);

                    // and update crosshair location

                    XYPlot plot = (XYPlot)this.chartPanel.getChart().getPlot();
                    plot.setDomainCrosshairValue(bi.GstatisticCutoffIndex);
                    plot.setRangeCrosshairValue(bi.PvalueCutoffIndex);
                }
            }

            /**
             * Callback method for receiving notification of a mouse movement on a
             * chart.
             *
             * @param event  information about the event.
             */
            public void chartMouseMoved(ChartMouseEvent event) {
                // ignore
            }

            /**
             * Handles a state change event.
             *
             * @param event  the event.
             */
            public void stateChanged(ChangeEvent event) {
                if (event.getSource() == this.FDRcutoffSlider) {
                    double FDRcutoff = this.experiment.FDR_SLIDER_PRECISION*0.01*(double)this.FDRcutoffSlider.getValue();
                    this.experiment.setFDRCutoff(FDRcutoff);
                    this.FDRedit.setText(this.experiment.getFDRCutoffString());
                    this.FDRedit.repaint();
                    this.chartPanel.zoomHeatmapRange();
                    this.repaint(); // update
                }
            }

            /**
             * Handles a chart progress event.
             *
             * @param event  the event.
             */
            public void chartProgress(ChartProgressEvent event) {
                // ignore
            }
        }
    
        /**
         * A clickable viewer for Spectral count results.
         *
         * @param experiment  contains the data and the frame title.
         */
        public  PepcViewer(Experiment experiment) {
            super(experiment.getName());
            setContentPane(new PepcPanel(experiment));
            this.pack();       /** Causes this Window to be sized to fit the preferred size
         * and layouts of its subcomponents.  If the window and/or its owner
         * are not yet displayable, both are made displayable before
         * calculating the preferred size.  The Window will be validated
         * after the preferredSize is calculated.
         * @see Component#isDisplayable
         */
            RefineryUtilities.centerFrameOnScreen(this);
            this.setVisible(true);
        }

        /**
         * A Pepc results table model.  IPIname, GStatistic, PValue, TP, FDR
         */
        static class PepcResultsProteinReportTableModel extends AbstractTableModel
                implements TableModel {

            private Object[][] data;
            final Experiment experiment;
            int rowcount; // current number of proteins clearing gstatistic,pvalue cuttofs (if none, this is 1 and we show "none")

            /**
             * Creates a new proteins report table model.
             *
             */
            public PepcResultsProteinReportTableModel(Experiment e) {
                experiment = e;
                rowcount = 1;
                this.data = new Object[getRowCount()][getColumnCount()];
                this.data[0][0] = "";
            }

            /**
             * Returns the number of columns.
             *
             * @return column count.
             */
            public int getColumnCount() {
                return experiment.getHdrNames().size()+2;
            }

            /**
             * Returns the row count.
             *
             * @return rowcount (at least one, plus any proteins clearing the current gstatistic,pvalue).
             */
            public int getRowCount() {
                return rowcount;
            }

            /**
             * Returns the value at the specified cell in the table.
             *
             * @param row  the row index.
             * @param column  the column index.
             *
             * @return The value.
             */
            public Object getValueAt(int row, int column) {
                return this.data[row][column];
            }

            /**
             * Sets the value at the specified cell.
             *
             * @param value  the value.
             * @param row  the row index.
             * @param column  the column index.
             */
            public void setValueAt(Object value, int row, int column) {
                this.data[row][column] = value;
                fireTableDataChanged();
            }

            public void updateTable(PepcCutoffInfo info) {
                Vector<Protein> proteins = experiment.getProteinsByCutoffIndices(info.GstatisticCutoffIndex,info.PvalueCutoffIndex);
                rowcount = Math.max(1,proteins.size());
                this.data = new Object[getRowCount()][getColumnCount()];
                if (0==proteins.size()) {
                    this.data[0][0] = "(none)";
                }
                else {
                    for (int s=0;s<proteins.size();s++) {
                        Protein p = proteins.get(s);
                        int i;
                        for (i=0;i<p.getProteinInfo().size();i++) {
                            if (getColumnClass(i).equals(String.class)) {
                                this.data[s][i] = p.getProteinInfo().get(i);
                            } else {
                                this.data[s][i] = Double.parseDouble(p.getProteinInfo().get(i));
                            }
                        }
                        this.data[s][i++] = p.getGStatistic();
                        this.data[s][i] = p.getPValue();
                    }
                }
                fireTableDataChanged();  // table has changed
            }

            /**
             * Returns the column name.
             *
             * @param column  the column index.
             *
             * @return The column name.
             */
            public String getColumnName(int column) {
                if (column < 0) {
                    return null;
                } else  if (column < experiment.getHdrNames().size()) {
                    return experiment.getHdrNames().get(column);
                } else  if (column == experiment.getHdrNames().size()) {
                    return "G-statistic";
                } else  if (column == experiment.getHdrNames().size()+1) {
                    return "p-value";
                }
                return null;
            }
        

            /**
             * Returns the column class for sorting purposes.
             *
             * @param column  the column index.
             *
             * @return The column class.
             */
            public Class getColumnClass(int column) {
                if (column < 0) {
                    return null;
                } else  if (column < experiment.getHdrNames().size()) {
                    return experiment.getHdrClasses().get(column);
                } else  if (column == experiment.getHdrNames().size()) {
                    return Double.class;
                } else  if (column == experiment.getHdrNames().size()+1) {
                    return Double.class;
                }
                return null;
            }
        }


        /**
         * A Pepc results table model.  Cutoff info.
         */
        static class PepcResultsCutoffReportTableModel extends AbstractTableModel
                implements TableModel {

            private final Object[][] data;
            final Experiment experiment;

            /**
             * Creates a cutoff report table model.
             *
             */
            public PepcResultsCutoffReportTableModel(Experiment e) {
                experiment = e;
                this.data = new Object[getRowCount()][getColumnCount()];
                this.data[0][0] = "";
            }

            /**
             * Returns the number of columns.
             *
             * @return 1.
             */
            public int getColumnCount() {
                return 1;
            }

            /**
             * Returns the row count.
             *
             * @return rowcount (at least one, plus any proteins clearing the current gstatistic,pvalue).
             */
            public int getRowCount() {
                return 1;
            }

            /**
             * Returns the value at the specified cell in the table.
             *
             * @param row  the row index.
             * @param column  the column index.
             *
             * @return The value.
             */
            public Object getValueAt(int row, int column) {
                return this.data[row][column];
            }

            /**
             * Sets the value at the specified cell.
             *
             * @param value  the value.
             * @param row  the row index.
             * @param column  the column index.
             */
            public void setValueAt(Object value, int row, int column) {
                this.data[row][column] = value;
                fireTableDataChanged();
            }

            public void updateTable(PepcCutoffInfo info) {
                this.data[0][0] = info.text;
                fireTableChanged(null);  // everything in table has changed
            }

            /**
             * Returns the column name.
             *
             * @param column  the column index.
             *
             * @return null. - no column headers
             */
            public String getColumnName(int column) {
                return null;
            }

        } //end class PepcResultsCutoffReportTableModel



    }

