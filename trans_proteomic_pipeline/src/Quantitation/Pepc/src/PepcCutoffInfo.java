//
// Copyright (c) 2008 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Brian Pratt
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA

// package pepc;


// things you can know about a Gstatistic,Pvalue cutoff in an experiment
public class PepcCutoffInfo {
    final int GstatisticCutoffIndex;
    final int PvalueCutoffIndex;
    public final String text;
    public final String smalltext;
    PepcCutoffInfo(Experiment e, int G, int P) {
        GstatisticCutoffIndex = G;
        PvalueCutoffIndex = P;
        Double GStatistic=e.getGStatisticCutoffByIndex(GstatisticCutoffIndex);
        Double PValue=e.getPValueCutoffByIndex(PvalueCutoffIndex);
        double TP=e.getTPbyIndex(GstatisticCutoffIndex, PvalueCutoffIndex);
        double FDR=e.getFDRbyIndex(GstatisticCutoffIndex, PvalueCutoffIndex);
        Double count=e.getNullhypothesisFilterPassCount(G,P);
        /*text = "cutoff GStatistic>=";+GStatistic.toString()+" PValue<="+PValue.toString()
                    +", TP="+String.format("%.3f",TP)+" FDR="+String.format("%.3f",FDR)+" Count="+count.toString();
                         */
        double FDRCutoff = 100*e.getFDRCutoff();
        
        text = "Found "+count.toString()+" proteins, with "+String.format("%.3f",FDR*count.doubleValue())
                +" expected false positives ( "+String.format("%.3f",100*FDR)+"% FDR ), at G-statistic >="+GStatistic.toString()+" and p-value<="+PValue.toString();
        smalltext = count.toString()+" proteins, "+String.format("%.2f",FDR*count.doubleValue())+ " expected false("+String.format("%.3f",100*FDR)+"% FDR). \n[ Brighter boxes = more true positives, white boxes = FDR > "+String.format("%.2f",FDRCutoff)+"%]";
    }
}
