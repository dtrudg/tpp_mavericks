// CombinationPair
// holds a left and right set of combinations (should not overlap)
//

//
// Copyright (c) 2008 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Brian Pratt
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA
// package pepc;

import java.util.*;


class CombinationPair {
    private final int[] left;
    private final int[] right;
    private int order; // 0-based count of items "native" to left side, {0123}:{4567 has order 3, {0124}:{3567} has order 2
    
    public CombinationPair(Combination l,Combination r)
    {
        if (l.getFirst() < r.getFirst())
        {
            left = l.getValues();
            right = r.getValues();
        } else {
            left = r.getValues();
            right = l.getValues();
        }
        // get 0-based count of items "native" to left side, 
        // {0123}:{4567 has order 3, {0124}:{3567} has order 2
        order = -1;
        for (int n=left.length;n-->0;) {
            if (left[n]<left.length) {
                order++;
            }
        }
    }

    public int getOrder() {
        return order;
    }

    public int[] getLeft() {
        return left;
    }
    public int getLeft(int n) {
        return left[n];
    }
    public int[] getRight() {
        return right;
    }
    public int getRight(int n) {
        return right[n];
    }
    public String toString()
    {
        return left.toString()+":"+right.toString()+" (order="+getOrder()+")";
    }
    public int hashCode()
    {
        return toString().hashCode();
    }
    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if((obj == null) || (obj.getClass() != this.getClass()))
            return false;
        CombinationPair p = (CombinationPair)obj;
        for (int n=left.length;n-->0;) {
            if ((left[n] != p.left[n]) || (right[n]!=p.right[n])) {
                return false;
            }
        }
        return true;
    }
}
