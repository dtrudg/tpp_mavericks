//--------------------------------------
// Systematically generate combination pairs.
// Adapted from http://www.merriampark.com/comb.htm,
// which states "The source code is free for you to use in whatever way you wish."
// written by Michael Gilleland
//
//
// [these are the original notes, I rejiggered getNext to return a set of Integers - bpratt]
//
/*The class is very easy to use. Suppose that you wish to generate all possible
  three-letter combinations of the letters "a", "b", "c", "d", "e", "f", "g".
  Put the letters into an array. Keep calling the combination generator's getNext ()
  method until there are no more combinations left. The getNext () method returns an
  array of integers, which tell you the order in which to arrange your original array
  of letters. Here is a snippet of code which illustrates how to use the
  CombinationGenerator class.
String[] elements = {"a", "b", "c", "d", "e", "f", "g"};
int[] indices;
CombinationGenerator x = new CombinationGenerator (elements.length, 3);
StringBuffer combination;
while (x.hasMore ()) {
  combination = new StringBuffer ();
  indices = x.getNext ();
  for (int i = 0; i < indices.length; i++) {
    combination.append (elements[indices[i]]);
  }
  System.out.println (combination.toString ());
}*/

//--------------------------------------
// package pepc;

import java.math.BigInteger;

class CombinationGenerator {

  private int[] a;
  private int[] orderSizes; // how many each of nA vs mB
  private int nBioRepsTimesTwo;
  private int nBioReps;
  private BigInteger numLeft;
  private BigInteger total;
  private BigInteger halfway; // we only care about half the space, since other half just mirros it

  //------------
  // Constructor
  //------------

  public CombinationGenerator (int nBioReps) {
    nBioRepsTimesTwo = nBioReps*2;
    this.nBioReps = nBioReps;
    if (this.nBioReps > nBioRepsTimesTwo) {
      throw new IllegalArgumentException ();
    }
    if (nBioRepsTimesTwo < 1) {
      throw new IllegalArgumentException ();
    }
    a = new int[this.nBioReps];
    orderSizes = new int[this.nBioReps];
    BigInteger nFact = getFactorial (this.nBioRepsTimesTwo);
    BigInteger rFact = getFactorial (this.nBioReps);
    total = nFact.divide (rFact.multiply (rFact)); // how many are possible, including mirrors?
    halfway = total.divide(new BigInteger("2")); // this is what we report to the world as "total"
    reset();  // prepare to iterate
    // find out how many of each kind of permutation we'll have
    while (hasMore()) {
        CombinationPair p=getNext();
        orderSizes[p.getOrder()]++;       
    }
    reset(); // prepare to iterate
  }

  private void reset() {

    // begin with the null hypothesis - all A vs all B
    for (int i = 0; i < this.nBioReps; i++) {
      a[i] = i;
    }
    // how many left to iterate over?  why, all of them of course
    numLeft = new BigInteger (total.toString ());  
  }

  //------
  //  how many combos are possible?
  // note that we only care about the first half,
  // since the other half just mirrors that   
  //------
  public BigInteger getTotal() {
      return halfway;  // outside world doesn't need to know about mirror cases
  }

  // how many of this kind of permutation are there?
  public int getOrderSize(int o) {
      return orderSizes[o];
  }

  //-----------------------------
  // Are there more combinations?
  // note that we only care about the first half,
  // since the other half just mirrors that
  //-----------------------------

  public boolean hasMore () {
    return numLeft.compareTo (halfway) == 1;
  }

  //------------------
  // Compute factorial
  //------------------

  private static BigInteger getFactorial (int n) {
    BigInteger fact = BigInteger.ONE;
    for (int i = n; i > 1; i--) {
      fact = fact.multiply (new BigInteger (Integer.toString (i)));
    }
    return fact;
  }


  private CombinationPair makeCombinationPair() {
     Combination leftside = new Combination(a);
     Combination rightside = leftside.inverse(); // set of ints not on left
     return new CombinationPair(leftside,rightside);
  }

 //--------------------------------------------------------
 // Generate next combination (algorithm from Rosen p. 286)
 //--------------------------------------------------------
  public CombinationPair getNext () {
     advance();
     return makeCombinationPair();
 }

  private void advance() {
    if (numLeft.equals (total)) {
      numLeft = numLeft.subtract (BigInteger.ONE);
      return;
    }

    int i = nBioReps - 1;
    while (a[i] == nBioReps + i) {
      i--;
    }
    a[i] = a[i] + 1;
    for (int j = i + 1; j < nBioReps; j++) {
      a[j] = a[i] + j - i;
    }

    numLeft = numLeft.subtract (BigInteger.ONE);
    return;

  }
}

