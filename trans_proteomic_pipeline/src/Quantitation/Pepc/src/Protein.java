//
// Copyright (c) 2007,2008 Insilicos LLC. All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Nate Heinecke
// Insilicos LLC
// www.insilicos.com
//
// Seattle, WA

// package pepc;

import java.util.*;


class Protein
{
    private final Vector<String> proteinInfo;        // as read from file
    private double[] spectralCounts;  // as read from file, length is 2*nBioReps
    ProteinPermutation nullHypothesis; //  all A vs all B case


    Protein(Vector<String> proteinInfo, double[] spectralCounts)
    {
        this.proteinInfo = proteinInfo;
        this.spectralCounts = spectralCounts;
    }

    //Initialize without knowing all of the spectral counts
    //but still need to know how many runs there will be
    Protein(Vector<String> proteinInfo, int nCounts)
    {
        this.proteinInfo=proteinInfo;
        this.spectralCounts=new double[nCounts];
    }
    Vector<String> getProteinInfo() {
        return proteinInfo;
    }

    //get the GStatistic for an all-nothing comparison (ie all control vs all foam)
    double getGStatistic()
    {
        return this.getNullHypothesis().getGStatistic();
    }

    //set one of the spectral counts
    public void insertCount(double value, int slot)
        {
        if(slot < spectralCounts.length)
            {
            spectralCounts[slot]=value;
            }
        }
    double[] getSpectralCounts()
        {
        return this.spectralCounts;
        }
    //here we are passing a permutation pattern, such as 5634-1278, and adding a ProteinPermutation to the list
    ProteinPermutation doPermutation(CombinationPair combo)
    {
        int[] leftIndices = combo.getLeft();
        int[] rightIndices = combo.getRight();
        double[] left = new double[leftIndices.length];
        double[] right = new double[rightIndices.length];
        double lmean = 0;
        double rmean = 0;
        for (int i=leftIndices.length;i-->0;) {
            left[i] = this.spectralCounts[leftIndices[i]];
            lmean += left[i];
            right[i] = this.spectralCounts[rightIndices[i]];
            rmean += right[i];
        }
        lmean /= leftIndices.length;
        rmean /= leftIndices.length;
        double t=this.calculatePValue(left,right,lmean,rmean);
        double g=this.calculateGStatistic(lmean,rmean);
        if(Double.isNaN(t))
            {
            t=1;
            }
        return new ProteinPermutation(g,t);
    }

    ProteinPermutation getNullHypothesis()
    {
        return nullHypothesis; //  all A vs all B case
    }

    String ar2str(int[] left, int[] right)     //make a text representation of a permutation pattern
    {
        String ret="";
        for (int i:left)
        {
            ret += i;
        }
        ret+=" - ";
        for (int i:right)
        {
            ret += i;
        }
        return ret;
    }

    double calculatePValue(double[] left, double[] right, double lmean, double rmean)	//do an unpaired 2-tailed t-test and return a p-value from the t-value
    {
        double sdleft=0;
        double sdright=0;
        double len = left.length; // note: assuming left and right same length
        double sqrtlen = Math.sqrt(len);
        for(double d:left)
        {
            d-=lmean;
            sdleft+= d*d;
        }
        sdleft=(Math.sqrt(sdleft/(len-1)));
        sdleft/=sqrtlen;
        sdleft=sdleft*sdleft;
        for(double d:right)
        {
            d-=rmean;
            sdright+=d*d;
        }
        sdright=(Math.sqrt(sdright/(len-1)));
        sdright/=sqrtlen;
        sdright=sdright*sdright;
        double tvalue = ((rmean-lmean) / Math.sqrt(sdright+sdleft));
        return studT(tvalue,((len*2)-2));
    }
    double calculateGStatistic( double lmean, double rmean)	//do a g-test
    {
        double rlmean = (lmean+rmean)/2;
        double lentropy;
        double rentropy;
        if (lmean > 0.1)        
            lentropy = lmean*Math.log(lmean/rlmean);
        else
            lentropy = 0;
        if (rmean > 0.1)        
            rentropy = rmean*Math.log(rmean/rlmean);
        else
            rentropy = 0;
        return 2*(lentropy+rentropy);
    }
    double studT(double t,double n)		//convert a t-value to a p-value (n=degrees of freedom)
    {
        double w=Math.abs(t)/Math.sqrt(n);
        double th=Math.atan(w);
        if(n==1)
        {
            return 1-th/(Math.PI/2);
        }
        double sth=Math.sin(th);
        double cth=Math.cos(th);
        if((n%2)==1)
        {
            return 1-(th+sth*cth*statCom(cth*cth,2,n-3,-1))/(Math.PI/2);
        }
        else
        {
            return 1-sth*statCom(cth*cth,1,n-3,-1); }
    }
    double statCom(double q,double i,double j,double b)
    {
        double zz=1;
        double z=1;
        double k=i;
        while(k<=j)
        {
            zz=zz*q*k/(k-b);
            z=z+zz;
            k=k+2;
        }
        return z;
    }

    //get the proteins P-value from its all-to-nothing comparison
     double getPValue()
     {
         return this.getNullHypothesis().getPValue();
     }

}

