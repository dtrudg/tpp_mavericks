package com.insilicos.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

//import com.google.gwt.user.client.HTTPRequest;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gchart.client.GChart;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.*;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Comparator;

public class PepcView implements EntryPoint {
    //protected String jsontext = "{\"Array_indexed_by_cutoff\":[{\"TTest\":0.05,\"GStatisticArray\":[{\"GStatistic\":2,\"Total_found\":16,\"Null_found\":14,\"Proteins\":[\"IPI0002\",\"IPI0005\",\"IPI0004\"]},{\"GStatistic\":3,\"Total_found\":19,\"Null_found\":11,\"Proteins\":[\"IPI0005\",\"IPI0005\",\"IPI00012\"]},{\"GStatistic\":4,\"Total_found\":17,\"Null_found\":19,\"Proteins\":[\"IPI0007\",\"IPI0008\",\"IPI0004\"]}]},{\"TTest\":0.04,\"GStatisticArray\":[{\"GStatistic\":2,\"Total_found\":21,\"Null_found\":14,\"Proteins\":[\"IPI0002\",\"IPI0005\",\"IPI00011\"]},{\"GStatistic\":3,\"Total_found\":25,\"Null_found\":15,\"Proteins\":[\"IPI0007\",\"IPI0005\",\"IPI0004\"]},{\"GStatistic\":4,\"Total_found\":23,\"Null_found\":8,\"Proteins\":[\"IPI0003\",\"IPI0005\",\"IPI0004\"]}]}],\"MasterProteinList\":[{\"Name\":\"IPI0001\",\"Pvalue\":0.03,\"GStatistic\":3.6},{\"Name\":\"IPI0002\",\"Pvalue\":0.02,\"GStatistic\":3},{\"Name\":\"IPI0003\",\"Pvalue\":0.01,\"GStatistic\":2},{\"Name\":\"IPI0004\",\"Pvalue\":0.04,\"GStatistic\":2}]}";
    //protected String jsontext2="{\"Array_by_GStatistic\":[{\"GStatistic\":2.5},{\"GStatistic\":2.5}]}";

    Frotein[] FroList;      //list of proteins
    double upperBound;      //range of TP, for choosing colors
    double lowerBound;
    double[] Gcutoffs;      //Our index of G-test cutoffs
    double[] Tcutoffs;      //Our index of T-test cutoffs
    //In general we access things by G-test, then T-test
    //i.e. the x axis is G-test indexed, the y axis is T-test indexed.
    //
    double[][] nPositives;          //Number of proteins that pass a given G/T filter
    double[][] nFalsePositives;     //predicted false positives
    FlexTable proteinTable;         //the table that lists our approved proteins
    PepcViewGChart chart;          //a sexy chart
    String[] tableHeaders;          //headers
    int lastColumnClicked=346;            //for inverse table sorting
    boolean flipped=false;
    double gCutoff;
    double tCutoff;                 //current g and t cutoffs, stored for list-reordering purposes
    double defaultFDRcutoff = .20;  // iniital FDR cutoff

    JSONValue jsonTree;
    int nProteins;
    int listindex;
    Frotein[] monFroList;
    int monFroListLength;
  public void onModuleLoad()
        {
        final Button button = new Button("Set max FDR");
        final Button ebutton = new Button("Export Table to CSV file");
        final Label label = new Label();
        String FDRCutoffExplain = "use this to adjust the maximum False Discovery Rate past which chart cells will be displayed as blank";
         //Gcutoffs=new double[] {0.5,1,1.5,2,2.5,3,3.5,4,5,6};
        //Tcutoffs=new double[] {0.001,0.002,0.003,0.004,0.004,0.005,0.006,0.01,0.015,0.020};
        //---------------
        // We want the user to be able to re-color the graph eliminating certain "bad" squares
        // To this end we present a False Discovery Rate Cutoff. Squares (cutoff pairs) whose results lie above
        // this cutoff will not be displayed, although you can still click them.
        final TextBox tb = new TextBox();
        tb.setText(Double.toString(defaultFDRcutoff));
        tb.setTitle(FDRCutoffExplain);

        button.setTitle(FDRCutoffExplain);
        button.addClickHandler(new ClickHandler() {
          public void onClick(ClickEvent theClick) {
            {
            double newc = Double.parseDouble(tb.getText());
            if(newc > 0)
                {
                chart.setFDRcutoff(newc);
                chart.redrawChart();
                }
            }
          }
        });

        ebutton.addClickHandler(new ClickHandler() {
          public void onClick(ClickEvent theClick) {
            {
            double newc = Double.parseDouble(tb.getText());
            if(newc > 0)
                {
                doCSV();
                }
            }
          }
        });               

        //hijacked from the GWT javadoc
        // Let's disallow non-numeric entry in the normal text box.
        tb.addKeyPressHandler(new KeyPressHandler() {
          public void onKeyPress(KeyPressEvent theKeyPressEvent) {
            char keyCode = theKeyPressEvent.getCharCode();
            if ((!Character.isDigit(keyCode)) && (keyCode != (char) KeyCodes.KEY_TAB)
                && (keyCode != (char) KeyCodes.KEY_BACKSPACE)
                && (keyCode != (char) KeyCodes.KEY_DELETE) && (keyCode != (char) KeyCodes.KEY_ENTER)
                && (keyCode != (char) KeyCodes.KEY_HOME) && (keyCode != (char) KeyCodes.KEY_END)
                && (keyCode != (char) KeyCodes.KEY_LEFT) && (keyCode != (char) KeyCodes.KEY_UP)
                && (keyCode != (char) KeyCodes.KEY_RIGHT) && (keyCode != (char) KeyCodes.KEY_DOWN)) {
              // TextBox.cancelKey() suppresses the current keyboard event.
              ((TextBox)theKeyPressEvent.getSource()).cancelKey();
            }
          }
        });
        //---------------   end textbox + button

        final FlexTable table = new FlexTable();
	table.setStyleName("table");
	table.getRowFormatter().setStyleName(0,"header");

        RootPanel.get("text").add(label);
        RootPanel.get("lane2").add(table);     
        RootPanel.get("fdrButton").add(tb);
        RootPanel.get("fdrButton").add(button);
        RootPanel.get("fdrButton").add(ebutton);

        //All the real stuff starts after we get our ResponseText from the server.
        doFetchURL();
        }

    private void doFetchURL()
        {
        QueryParameters parms = new QueryParameters();
        String filepath = parms.getValue("path");
        String filename = parms.getValue("file");
        setLabel("Retrieving "+filepath+filename+"...");
                   /*
        if (!HTTPRequest.asyncGet("json-Results.js", new JSONResponseTextHandler()))
            {
            setLabel("Error during GET");
            }         */
        JSONRequest jr = new JSONRequest();
        JSONRequestHandler jrh = new myHandler();
        //"here" is defined in the Pepc (pepc) code, they must match.
		try {
        jr.get(filepath+filename,"here",jrh);
        }
	    catch (Exception e) {
            setLabel("Error!"+e.toString());
            }
        }
    protected class myHandler implements JSONRequestHandler
        {
        public void onRequestComplete(JavaScriptObject json)
            {
            try {
                setLabel("Got it .... parsing (A)");
                
                jsonTree = new JSONObject(json);
                setLabel("Got it! .... parsing (B)");
                
                doFirstParsing(jsonTree);   //this is our routine
                setLabel("Got it! .... good !");
                
                chart = new PepcViewGChart(15,15);
                RootPanel.get("chart").add(chart);
                chart.redrawChart();
                setLabel("Ready! Click on the graph to get a list of proteins that satisfy the indicated G-statistic and T-Test p-value thresholds.  Brighter boxes indicate higher True Positive (TP) counts, white boxes indicate maximum FDR exceeded.");
                }
            catch (Exception e)
                {
                setLabel("Error!"+e.toString());
                }
            }
        }
    void clearTable(FlexTable table)
              {
              while(table.getRowCount() > 0)
                table.removeRow(0);
              }

    private void doSecondParsing(JSONValue mainjv)
        {
        JSONObject jo;
        if((jo=mainjv.isObject())!=null)
            {
            JSONArray ja;
            JSONObject joo;
            JSONArray jaa;
            JSONValue jv;
            JSONValue jvv;
            Frotein fro;

            if((jv=jo.get("Headers")) != null)
                {
                ja=jv.isArray();
                if(ja != null)
                    {
                    tableHeaders = new String[ja.size()];
                    for(int i=0;i<ja.size();i++)
                        {
                        tableHeaders[i]=ja.get(i).toString().replaceAll("\""," ").trim();
                        }
                    }
                }
            else
                {
                tableHeaders = new String[] {"IPI", "GStatistic", "TTest"} ;
                }
            ja=(jo.get("ProteinList")).isArray();
            nProteins = ja.size();
            //First get the master list of proteins
            for(int i=0;i<nProteins;i++)
                {
                joo=(ja.get(i)).isObject();
                if(joo != null)
                    {
                    //fro = new Frotein();
                    //fro.setTtest(Double.parseDouble(joo.get("PValue").toString()));
                    //fro.setGstatistic(Double.parseDouble(joo.get("GStatistic").toString()));
                   
                    //jaa = joo.get("Protein_Info").isArray();
                    jvv = joo.get("Protein_Info");
                    //fro = new Frotein(jaa.get(0).toString().replaceAll("\""," ").trim(),Double.parseDouble(joo.get("GStatistic").toString()),Double.parseDouble(joo.get("PValue").toString()));
                    //fro = new Frotein(jaa.get(0).toString(),Double.parseDouble(joo.get("GStatistic").toString()),Double.parseDouble(joo.get("PValue").toString()));
                    fro = new Frotein(jvv,Double.parseDouble(joo.get("GStatistic").toString()),Double.parseDouble(joo.get("PValue").toString()));
                    /*
                    if(jaa.size() > 1)
                        {
                        String[] infoi = new String[jaa.size()-1];
                        for (int p=0;p<jaa.size()-1;p++)
                            {
                            //infoi[p]=jaa.get(p+1).toString().replaceAll("\""," ").trim();
                            infoi[p]=jaa.get(p+1).toString();
                            }
                        fro.setProtInfo(infoi);
                        }*/
                    //fro.setName(jaa.get(0).toString().replaceAll("\""," ").trim());

                    addProteinToList(fro);

                    }
                }
            }
        }
    // Parse the tree of JSON objects gathered from the JSON text
    // This is where we actually build our list of proteins and get our information about cutoffs, etc.
    //
    private void doFirstParsing(JSONValue mainjv)
        {
        JSONObject jo;
        if((jo=mainjv.isObject())!=null)
            {
            JSONArray ja;
            JSONObject joo;
            JSONArray jaa;
            JSONValue jv;
            double gt;
            double tt;
            int pf;
            double nf;          

            ja=(jo.get("Array_by_GStatistic")).isArray();
            Gcutoffs = new double[ja.size()];
            jaa =ja.get(0).isObject().get("Array_by_TTest").isArray();
            Tcutoffs = new double[jaa.size()];
            nPositives = new double[Gcutoffs.length][Tcutoffs.length];
            nFalsePositives = new double[Gcutoffs.length][Tcutoffs.length];

            if(ja !=null)
                {

                for(int i=0;i<ja.size();i++)
                    {
                    joo=(ja.get(i)).isObject();
                    if(joo != null)
                        {
                        tt=0;
                        if((jv = joo.get("GStatistic")) != null)
                            {
                            tt=Double.parseDouble(jv.toString());
                            Gcutoffs[i]=tt;
                            }
                        if((jaa = joo.get("Array_by_TTest").isArray()) != null )
                            {

                            for(int ii=0;ii<jaa.size();ii++)
                                {
                                gt=0;
                                pf=0;
                                nf=0;
                                if((jv = jaa.get(ii).isObject().get("TTest")) != null)
                                    {
                                    gt=Double.parseDouble(jv.toString());
                                    }
                                if((jv = jaa.get(ii).isObject().get("Pos")) != null)
                                    {
                                    pf=Integer.parseInt(jv.toString());
                                    }
                                if((jv = jaa.get(ii).isObject().get("FPos")) != null)
                                    {
                                    nf=Double.parseDouble(jv.toString());
                                    }

                                nPositives[i][ii]=pf;
                                nFalsePositives[i][ii]=nf;
                                Tcutoffs[ii]=gt;
                                }
                            }// end ttest loop
                        }
                    }// end gstatistic loop
                }
            }
        }// end doParsing()
    String trimDouble(String number, int maxDecimals)
        {
        //Trim a number string to a certain decimal place, for display only
        int dec=number.indexOf('.');
        if(dec != -1)
            {
            String[] strs = number.split("[.]");
            return strs[0]+"."+strs[1].substring(0,maxDecimals);
            }
        else return number;
        }

    //add a protein to the display
    private void addProteinRow(FlexTable table,Frotein Fro)
         {

         String[] infoColumns = Fro.getProtInfo();
         int nColumns;
         int row = table.getRowCount();
         table.setText(row,0,Fro.name);
         if(infoColumns != null)
            {
            nColumns = infoColumns.length;
            for(int i=0;i<nColumns;i++)
                {
                table.setText(row,1+i,infoColumns[i]);
                }
            }
         else
             nColumns=0;
         table.setText(row,nColumns+1,trimDouble(Double.toString(Fro.getGstatistic()),5));
         table.setText(row,nColumns+2,trimDouble(Double.toString(Fro.getTtest()),5));

         //note: These calls don't do what they are supposed to
         (table.getCellFormatter()).setWidth(row,0,"6cm");
         (table.getCellFormatter()).setWidth(row,1,"6cm");
         (table.getCellFormatter()).setWidth(row,2,"6cm");

         }

    //add a protein to the internal list
    private void addProteinToList (Frotein fro)
        {

        if(FroList==null)               //create new list
            {
            FroList=new Frotein[nProteins];
            FroList[0]=fro;
            listindex=1;
            return;
            }
        FroList[listindex]=fro;
        listindex++;
        return;
        /*
        for(int i=0;i<FroList.length;i++)   //add protein to existing list
            {
            if(FroList[i]==null)
                {
                FroList[i]=fro;
                return;
                }
            }
	
        int len=FroList.length;             //list full- resize and add protein
        Frotein[] newList=new Frotein[len*2];
        for(int i=0;i<len;i++)
            {
            newList[i]=FroList[i];
            }
        FroList=newList;
        FroList[len]=fro;*/
        }

    //What is a frotein? It's like a protein
    private class Frotein
        {
        private String name;
        private double Gstatistic;
        private double Ttest;
        private JSONValue jove;
        private String[] protInfo;
        Frotein()
            {
            }
        Frotein(String nname, double gt, double tt)
            {
            name=nname;
            Gstatistic=gt;
            Ttest=tt;
            }
        Frotein(JSONValue jv, double gt, double tt)
            {
            jove=jv;
            Gstatistic=gt;
            Ttest=tt;
            }
        private void getReady()
            {
            if(jove == null)
                return;
            JSONArray jaa = jove.isArray();
            if(jaa.size() > 1)
                        {
                        protInfo = new String[jaa.size()-1];
                        for (int p=0;p<jaa.size()-1;p++)
                            {
                            protInfo[p]=jaa.get(p+1).toString().replaceAll("\""," ").trim();
                            }
                        }
            name=jaa.get(0).toString().replaceAll("\""," ").trim();
            }
        public String[] getProtInfo()
            {
            if(protInfo==null)
                getReady();
            return protInfo;
            }
        public void setProtInfo(String[] newinf)
            {
            protInfo = newinf;
            }
        public String getName()
            {
            if(name==null)
                getReady();
            return name;
            }
        public void setName(String name)
            {
            this.name = name;
            }

        public double getGstatistic()
            {
            return Gstatistic;
            }

        public void setGstatistic(double gstatistic)
            {
            Gstatistic = gstatistic;
            }

        public double getTtest()
            {
            return Ttest;
            }

        public void setTtest(double ttest)
            {
            Ttest = ttest;
            }
        }

    //Type out a new list of proteins
    void setCutoffs(double gcutoff, double tcutoff)
	{
	gCutoff=gcutoff;
	tCutoff=tcutoff;
	}

    class ColumnButton extends Button
	{
	private int column;
	ColumnButton(String str, int i)
		{
		this.setText(str);
		column=i;
		}
	public int getColumn()
		{
		return column;
		}
	}

    //A custom protein comparator, since we want to sort on different values depending
    //on which column gets clicked
    //
    class froComp implements Comparator
	{
	private int column=0;
	private boolean invert=false;
	froComp(int col,boolean flip)
		{
		setColumn(col);
		invert = flip;
		}
	public void setColumn(int col)
		{
		column=col;
		}
	public int compare(Object obj1, Object obj2)
		{
		if(invert)
			return -1*doCompare(obj1,obj2);
		else
			return doCompare(obj1,obj2);
		}
	public int doCompare(Object obj1, Object obj2)
		{
		//GWT doesn't support generics atm...
		//
		Frotein fro1=(Frotein)obj1;
		Frotein fro2=(Frotein)obj2;

		//there's a good chances some nulls are floating in our oversized list...
		//
		if(obj1 == null && obj2 != null)
			return -1;
		else if(obj1 != null && obj2 == null)
			return 1;
		else if(obj1 == null && obj2 == null)
			return 0;
		else if(column==0)
			return fro1.getName().compareTo(fro2.getName());
		else if(column==-2)
			return Double.compare(fro1.getGstatistic(),fro2.getGstatistic());
		else if(column==-1)
			return Double.compare(fro1.getTtest(),fro2.getTtest());
		else
			{
			try		//try and parse numerically
				{
				return Double.compare(
						Double.parseDouble((String)fro1.getProtInfo()[column-1]),
						Double.parseDouble((String)fro2.getProtInfo()[column-1])
							);
				}
			catch(NumberFormatException e)	//ok parse alphabetically
				{
				return ((String)fro1.getProtInfo()[column-1]).compareTo(fro2.getProtInfo()[column-1]);
				}
			}
		}
	}
    void remakeTable(boolean freshList)
        {
        FlexTable table = (FlexTable) RootPanel.get("lane2").getWidget(0);
	
        if(table != null)
            {
            clearTable(table);
            for(int i=0;i<tableHeaders.length;i++)
                {
                table.setWidget(0,i,new ColumnButton(tableHeaders[i],i));
		//
		//Clickable column headers that sort... Sexy.
		//
		((Button)table.getWidget(0,i)).addClickHandler(new ClickHandler() {
          		public void onClick(ClickEvent theClick) {
            			{
				int col = ((ColumnButton)theClick.getSource()).getColumn();
				if(col == lastColumnClicked)
					{
					flipped = !flipped;
					}
				else
					flipped=false;
				lastColumnClicked=col;
				if(col == tableHeaders.length-1)
					col = -1;
				else if(col == tableHeaders.length-2)
					col = -2;
				froComp compy = new froComp(col,flipped);
				Arrays.sort(monFroList,compy);
				remakeTable(false);
                		}
                            }
            		});
          	
                }
            if(freshList==true)
                {
                monFroList = new Frotein[monFroListLength];
                int index=0;
                for(int i=0;i<FroList.length;i++)
                    {
                    if(FroList[i] != null)
                        {
                        if(FroList[i].getGstatistic()>=gCutoff && FroList[i].getTtest()<=tCutoff)
                            {
                            monFroList[index]=FroList[i];
                            index++;
                            addProteinRow(table,FroList[i]);
                            }
                        }
                    }
                }
            else
                {
                for(int i=0;i<monFroList.length;i++)
                    {
                    if(monFroList[i] != null)
                        {
                        addProteinRow(table,monFroList[i]);
                        }
                    }    
                }
            RootPanel.get("lane2").add(table);
            }
        }//end remakeTable

    //True Positives = Total Positives - False Positives.
    //note: this may not be the rarified statistical meaning of the phrase.
    public double getTPbyIndex(int GStatisticIndex,int PTestIndex) {
        double FP = getFP(GStatisticIndex, PTestIndex);
        double nullHypPassCount = nPositives[GStatisticIndex][PTestIndex];
        double TP = (nullHypPassCount -  FP);
        return Math.max(TP,0.0);
    }

    public double getFDRbyIndex(int GStatisticIndex,int PTestIndex) {
        double FP = getFP(GStatisticIndex, PTestIndex);
        double TP = getTPbyIndex(GStatisticIndex, PTestIndex);
        double FDR = ((FP + TP) != 0)? (FP / (FP + TP)) : 1.0;
        return (FDR);
    }

    //to a reverse lookup and get a graph X coord from a G-Test cutoff
    int GStatisticToX(double gt)
        {
        for(int i=0; i< Gcutoffs.length;i++)
            {
            if(Gcutoffs[i]==gt)
                {
                return i;
                }
            }
        return 0;
        }
    //to a reverse lookup and get a graph Y coord from a T-Test cutoff
    int TTestToY(double tt)
        {
        for(int i=0; i< Tcutoffs.length;i++)
            {
            if(Tcutoffs[i]==tt)
                {
                return i;
                }
            }
        return 0;
        }
    double getFP(int gt, int tt)
        {
        return nFalsePositives[gt][tt];
        }
    class PepcViewGChart extends GChart
        {
        static final double FDR_MAGIC = 9999.8877;  //A magic value
        static final int CHART_WIDTH = 350;
        static final int CHART_HEIGHT = 350;
        ClickListener listener;
        int lastClickedX = -1;
        int lastClickedY = -1;
        int XSize;  //size in chart units (i.e. number of squares), not pixels
        int YSize;
        double FDRcutoff=defaultFDRcutoff;
        PepcViewGChart(int maxx, int maxy) {
        setChartSize(CHART_WIDTH, CHART_HEIGHT);
        XSize = Math.min(maxx,Gcutoffs.length);
        YSize = Math.min(maxy,Tcutoffs.length);

        this.addClickHandler(new ClickHandler() {
            //
            // For a click we need to A) Show the appropriate protein list and B) redraw the chart with the clicked
            // square marked.
            public void onClick(ClickEvent theClick)
                {
                if(getTouchedCurve() != null)
                    {
                    if (FroList == null)
                        {
                        setLabel("Please wait...");
                        doSecondParsing(jsonTree);
                        }
                    //  extract the Ttest and Gstatistic cutoffs
                    //
                    Curve.Point point=getTouchedCurve().getPoint();
                    int x = (int)point.getX();
                    int y = (int)point.getY();
                    String title = describeChartCell(x,y);
                    double gt = Gcutoffs[x];
                    double tt = Tcutoffs[y];

                    //Curves are stored in a 1-d array. One curve for each point
                    getCurve(x*YSize+y).getSymbol().setBorderColor("0");    //black
                    if(lastClickedX != -1 && lastClickedY != -1)
                        {
                        String color =getCurve(lastClickedX*YSize+lastClickedY).getSymbol().getBackgroundColor();
                        getCurve(lastClickedX*YSize+lastClickedY).getSymbol().setBorderColor(color);
                        }
                    lastClickedX=x;
                    lastClickedY=y;
                    monFroListLength=(int)nPositives[x][y];
                    update();
                    setLabel(title);
                    setCutoffs(gt,tt);
                    remakeTable(true);
                    }
                }
            });
        setFDRcutoff(defaultFDRcutoff);
        } //end constructor

    //Squares failing the False Discovery Rate cutoff will not be displayed
    public void setFDRcutoff(double fdrc)
        {
        FDRcutoff = fdrc;
        double rmax=FDR_MAGIC;
        double rmin=FDR_MAGIC;
        for(int i=0;i<XSize;i++)
            for(int ii=0;ii<YSize;ii++)
                {
                if(getFDRbyIndex(i,ii) <= getFDRcutoff())
                    {
                    double r =  getTPbyIndex(i,ii);
                    if( r > 0)
                        {
                        if(rmax == FDR_MAGIC)
                            rmax =r;
                        else rmax = Math.max(rmax,r);
                        if(rmin == FDR_MAGIC)
                            rmin = r;
                        else rmin = Math.min(rmin,r);
                        }
                    }
                }
        if(rmax > rmin && rmax >=0 && rmin >=0)
            {
            //setLabel("fdr cutoff: "+getFDRcutoff()+" min: "+rmin+" max: "+rmax);
            setBounds(rmin,rmax);
            }
        }
    private double getFDRcutoff()
        {
        return FDRcutoff;
        }
        //If this square doesn't pass the FDR test, we want to show it as white,
        //even if its value lies within the display range
    double getValidTPdisplayValue(int gt, int tt)
        {
        if(getFDRbyIndex(gt,tt) <= FDRcutoff)
            {
                return getTPbyIndex(gt,tt);
            }
        else
        {
            return FDR_MAGIC;
        }
    }
    String describeChartCell(int x, int y)
    {
        String result = "Found "+nPositives[x][y]+ " proteins with "+ trimDouble(Double.toString(nFalsePositives[x][y]),3)+" likely false ("+trimDouble(Double.toString((getFDRbyIndex(x,y)*100)),3)+"% FDR), GStatistic >="+ Gcutoffs[x]+ " PValue <="+Tcutoffs[y];
        return result;
    }
    void redrawChart()
        {
        int tileWidth = CHART_WIDTH / XSize + 1;    //try and get the right size for the squares
        int tileHeight = CHART_HEIGHT / YSize + 1;

        clearCurves();
        for (int i = 0; i < XSize; i++)
            for(int ii= 0; ii < YSize; ii++)
            {
                addCurve();
                //each curve can only have one symbol (therefore one color)
                //so we need a new curve for each point
                getCurve().getSymbol().setSymbolType(SymbolType.BOX_NORTHEAST);
                getCurve().getSymbol().setBackgroundColor(getColorHexString(getValidTPdisplayValue(i,ii)));
                getCurve().getSymbol().setBorderColor(getColorHexString(getValidTPdisplayValue(i,ii)));
                getCurve().getSymbol().setWidth(tileWidth);
                getCurve().getSymbol().setHeight(tileHeight);
                getCurve().getSymbol().setBorderWidth(2);
                getCurve().addPoint(i,ii);
                Curve.Point point=getCurve().getPoint();
                int x = (int)point.getX();
                int y = (int)point.getY();
                // set the text seen when mouse pauses over this part of chart
                getCurve().getSymbol().setHovertextTemplate(GChart.formatAsHovertext(describeChartCell(x,y)));
                getCurve().getSymbol().setHoverAnnotationSymbolType(SymbolType.ANCHOR_MOUSE);
            }
        getXAxis().setAxisLabel("G-statistic");
        getXAxis().setTickLength(32);
        getYAxis().setAxisLabel("T-Test p-value");
        getYAxis().setTickLength(32);
        getYAxis().setAxisVisible(false);
        getXAxis().setAxisVisible(false);
        getXAxis().clearTicks();
        getYAxis().clearTicks();
        //
        // we need custom ticks
        for(int i=0;i<XSize;i++)
            {
            getXAxis().addTick(1+i,Double.toString(Gcutoffs[i]));
            }
        for(int i=0;i<YSize;i++)
            {
            getYAxis().addTick(1+i,"\n"+Double.toString(Tcutoffs[i]));
            }
        update();
        }
    //for color gradiation
    private void setBounds(double l, double u)
        {
        upperBound=u;
        lowerBound=l;
        }

    String getTwoCharHex(int val) {
        val += 1024; // force a leading 0 in case of val <= 0xF
        String result = Integer.toHexString(val);
        return result.substring(result.length()-2);
    }
    //
    // get a pretty color based on a square's TP value, keeping in mind the range determined
    // by the FDR cutoff. Anything that doesn't pass will arrive as FDR_MAGIC and get set to white
    String getColorHexString(double value)
        {
        if(value == FDR_MAGIC)
            {
            return "FFFFFF";    //white
            }
        double v = value;
        double U = upperBound;
        double L = lowerBound;
        double range = U-L;
        v = Math.max(v, L);
        v = Math.min(v, U);
        double offset = v-L;
        double gray = (offset / range);
        // this craziness is the gnuplot default gray->rgb conversion
        int r = (int)(255.0 * Math.sqrt(gray));
        int g = (int)(255.0 * (gray*gray*gray));
        int b = (int)(255.0 * Math.sin(2*gray*Math.PI));
        return getTwoCharHex(r) +getTwoCharHex(g) + getTwoCharHex(((b<0)?0:b));
        }
        }   //end PepcViewGChart

void setLabel(String str)
    {
    ((Label)RootPanel.get("text").getWidget(0)).setText(str);
    }
protected class QueryParameters
{
    private Map map = new HashMap();

    public QueryParameters()
    {
        String search = getQueryString();
        if ((search != null) && (search.length() > 0))
        {
            String[] nameValues = search.substring(1).split("&");
            for (int i = 0; i < nameValues.length; i++)
            {
                String[] pair = nameValues[i].split("=");
                map.put(pair[0], pair[1]);
            }
        }
    }

    public String getValue(String key)
    {
        return (String) map.get(key);
    }

    private native String getQueryString()
    /*-{
          return $wnd.location.search;
    }-*/;

}// end class QueryParameters

native JavaScriptObject doCSV() /*-{	
    monwindow = $wnd.open("", '_blank');
    monwindow.document.open("text/csv","replace");
    monwindow.document.writeln(@com.insilicos.client.PepcView::table2CSV()())
    monwindow.document.close()
  }-*/; 

static String table2CSV()
	{
        FlexTable table = (FlexTable) RootPanel.get("lane2").getWidget(0);
        String ret="";
        for(int i=0;i<table.getRowCount();i++)
             {
             int ccount=table.getCellCount(i);
             for(int p=0;p<ccount;p++)
                  {
                  ret+=table.getText(i,p);
                  if(p != ccount-1)
                       ret+=",";
                  }
             ret+="\n";
             }
        return ret;        
	}
}// end class PepcView


