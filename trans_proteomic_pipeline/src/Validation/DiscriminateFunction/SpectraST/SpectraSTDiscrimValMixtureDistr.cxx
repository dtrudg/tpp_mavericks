#include "SpectraSTDiscrimValMixtureDistr.h"

/*

Program       : SpectraSTDiscrimValMixtureDistr for PeptideProphet                                                       
Author        : Henry Lam <hlam@systemsbiology.org>                                                       
Date          : 04.10.06 

Copyright (C) 2006 Henry Lam

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Henry Lam
Insitute for Systems Biology
1441 North 34th St. 
Seattle, WA  98103  USA
hlam@systemsbiology.org

*/
SpectraSTDiscrimValMixtureDistr::SpectraSTDiscrimValMixtureDistr(int charge, const char* name, const char* tag, Boolean maldi, Boolean qtof, Boolean nonparam) {  
  initializeDistr(charge, name, tag);
  all_negs_ = False;
  maldi_ = maldi;
  maxdiff_ = 0.00001;
  gamma_ = False; //True;
  qtof_ = qtof;
  nonparam_ = nonparam;

  doublevals_ = new Array<double>;
  if (nonparam_) {
    negdistr_ = new NonParametricDistribution(0.02, True);
    posdistr_ = new NonParametricDistribution(0.01, False);
  }
  else {
    posdistr_ = new GaussianDistribution(maxdiff_);
    negdistr_ = new GammaDistribution(maxdiff_);
  }
  //    double posprior[] = {0.63, 0.075}; // {mean, stdev} for the Gaussian
  //  	double posprior[] = {0.65, 0.005}; // {mean, stdev} for the Gaussian
   	double posprior[] = {0.65, 0.005}; // {mean, stdev} for the Gaussian
	
       double negprior[] = {0.2, 0.005, 0.0}; // {alpha, beta, gamma} for the Gamma
  //  double negprior[] = {0.25, 0.07, 0.0}; // {alpha, beta, gamma} for the Gamma
//    double negprior[] = {0.2, 0.01, 0.0}; // {alpha, beta, gamma} for the Gamma
	

  minval_ = 0.0;
  negmean_ = 0.27;

  MIN_NUM_PSEUDOS_ = 50;
  ZERO_SET_ = 100; // ?
  NUM_DEVS_ = 6;
  USE_TR_NEG_DISTR_ = False;
  posinit_ = NULL;
  neginit_ = NULL;

  posinit_ = copy(posprior, 2);
  neginit_ = copy(negprior, 3);

  reset();
  
  //  ((GaussianDistribution*)(posdistr_))->ignoreStdev();
  if (!nonparam_) ((GammaDistribution*)(negdistr_))->setDistrMinval(0.0);
  
}


Boolean SpectraSTDiscrimValMixtureDistr::initializeNegDistribution(NTTMixtureDistr* nttdistr) {
 
 	return (false);
 
}

Boolean SpectraSTDiscrimValMixtureDistr::update(Array<double>* probs) {
  Boolean rtn = DiscrimValMixtureDistr::update(probs);
  negmean_ = ((ContinuousDistribution*)negdistr_)->getMean()+1.75*((ContinuousDistribution*)negdistr_)->getStdev();
  return rtn;
}

Boolean SpectraSTDiscrimValMixtureDistr::noDistr() {

  double num_stdevs[] = {0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8}; // for each charge
  double min_pos_means[] = {0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25};

  if(qtof_ || (charge_ == 0 && maldi_)) {
    return False;
  } else {
    double posMean = ((ContinuousDistribution*)(posdistr_))->getMean();
    double posStDev = ((ContinuousDistribution*)(posdistr_))->getStdev();
    double negMean = ((ContinuousDistribution*)(negdistr_))->getMean();
    double negStDev = ((ContinuousDistribution*)(negdistr_))->getStdev();  
    double negZero = 0;
    if (!nonparam_) ((GammaDistribution*)(negdistr_))->getZero();

    if (posMean < min_pos_means[charge_]) {
      //cout << "WARNING: No +" << charge_ + 1 << " distribution because PosMean (" << posMean << ") is smaller than " << min_pos_means[charge_] << endl;
    }
    if (posMean + posStDev < negMean + negZero + negStDev * num_stdevs[charge_]) {
      //cout << "WARNING: No +" << charge_ + 1 << " distribution because PosMean (" << posMean << ") + PosStDev (" << posStDev << ") is smaller than ";
      //cout << " NegMean (" << negMean << ") + NegZero (" << negZero << ") + NegStDev (" << negStDev << ") * " << num_stdevs[charge_] << endl;
    }


    return ((posMean < min_pos_means[charge_]) ||
            (posMean + posStDev < negMean + negZero + negStDev * num_stdevs[charge_]));
  }
}


void SpectraSTDiscrimValMixtureDistr::setDiscrimFunction(const char* mass_spec_type) {
  //cout << "setting new probid discrim function...for input file: " << mass_spec_type << endl;
  discrim_func_ = new SpectraSTDiscrimFunction(charge_);
}

