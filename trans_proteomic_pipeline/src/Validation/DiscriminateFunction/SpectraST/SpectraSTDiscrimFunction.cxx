#include "SpectraSTDiscrimFunction.h"

/*

Program       : SpectraSTDiscriminantFunction for discr_calc of PeptideProphet                                                       
Author        : Henry Lam <hlam@systemsbiology.org>                                                       
Date          : 04.10.06 


Copyright (C) 2006 Henry Lam

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Henry Lam
Insitute for Systems Biology
1441 North 34th St. 
Seattle, WA  98103  USA
hlam@systemsbiology.org

*/

SpectraSTDiscrimFunction::SpectraSTDiscrimFunction(int charge) : DiscriminantFunction(charge) { 
 
  dot_wt_ = 0.6;
  normdeltadot_wt_ = 0.4;


}




Boolean SpectraSTDiscrimFunction::isComputable(SearchResult* result) {
  return True;
}
  
double SpectraSTDiscrimFunction::getDiscriminantScore(SearchResult* result) {
  if(strcasecmp(result->getName(), "SpectraST") != 0) {
    cerr << "illegal type of SpectraST result: " << result->getName() << endl;
    exit(1);
  }
  SpectraSTResult* spectraSTResult = (SpectraSTResult*)(result);

  return (spectraSTResult->fval_);
  /*

  double score = 0.0;
  if (spectraSTResult->dot_ < 0.01) {
    score = 0.15; // put into neg without messing up with the distribution shape
  } else {
    //    score = dot_wt_ * spectraSTResult->dot_ + normdeltadot_wt_ * spectraSTResult->deltadot_ / spectraSTResult->dot_;
    score = 0.5 * spectraSTResult->dot_ + 0.5 * spectraSTResult->separation_;
  }  
  if ((spectraSTResult->dotbias_ < 0.1 || spectraSTResult->dotbias_ > 0.35) && score > 0.4) {
  	score -= 0.12;
	if (spectraSTResult->dotbias_ > 0.4) {
	  score -= 0.06;
	}
	if (spectraSTResult->dotbias_ > 0.45) {
	  score -= 0.06;
	}
  }	

  return (score);
  */
}
