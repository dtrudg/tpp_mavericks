
#include "PeptideProphetParser.h"
#include "Parsers/Parser/Parser.h"
#include "Parsers/Parser/TagListComparator.h" // for REGRESSION_TEST_CMDLINE_ARG defn

int main(int argc, char** argv) {
  hooks_tpp handler(argc,argv); // set up install paths etc

  // regression test stuff - bpratt Insilicos LLC, Nov 2005
  char *testArg=NULL;
  for (int argNum=1;argNum<argc;argNum++) {
    if (!strncmp(argv[argNum],REGRESSION_TEST_CMDLINE_ARG,strlen(REGRESSION_TEST_CMDLINE_ARG))) {
        // learn or run a test
        testArg = argv[argNum];
        // collapse down over this
        while (argNum < argc-1) {
           argv[argNum] = argv[argNum+1];
           argNum++;
        }
        argc--;
     }
  }


  // options are all args further down...
  int k,opt_len = 2;
  for(k = 2; k < argc; k++) {
    opt_len += strlen(argv[k]) + 1;
    //if(k > 2)
    // opt_len++;
  }
  char* options = new char[opt_len];
  options[0] = ' ';
  options[1] = 0;
  for(k = 2; k < argc; k++) {
    strcat(options, argv[k]);
    strcat(options, " ");
  }
    
  PeptideProphetParser *p;
  if(argc > 1)
    p = new PeptideProphetParser(argv[1], options, testArg);
  else {
	  std::string fname("new");
	  fname += get_pepxml_dot_ext();
	  p = new PeptideProphetParser(fname.c_str(), "", testArg);
  }
  bool success = p && p->success();
  delete p;
  delete[] options;
  return success?0:1;
}
