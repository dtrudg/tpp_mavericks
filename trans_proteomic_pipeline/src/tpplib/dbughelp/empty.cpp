/*
This is just a dummy project to create an empty library named "dbughelp".

In my own development environment I overwrite the result of this build with the 
HeapAgent debug heap manager library.  You can do what you like.

- Brian Pratt
Insilicos LLC
*/

void empty_fake_debughelp_func() {
	return;
}