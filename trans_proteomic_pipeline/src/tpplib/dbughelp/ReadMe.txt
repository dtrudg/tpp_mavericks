This is just a dummy VC8 project to create an empty library named "dbughelp".

In my own development environment I overwrite the result of this build with the 
HeapAgent debug heap manager library.  You can do what you like, or nothing at all.

- Brian Pratt
Insilicos LLC