
License note:

While the rest of the SPC's TPP project is licensed under the LGPL license, the MHDACWrapper is licenced under the non-reciprocal Apache license.

The MHDACWrapper is an apache-licensed wrapper class, in order to insulate LGPL code from directly calling into propretrary Agilent code.  The author's intention is to remove any possiblity of implication of reciprocal open-source code obligations from Agilent.  More specifically, we want to use the MHDAC system without any implied requirement that the MHDAC be open-source or redistributable in any way.  This arraignment was jointly determined and agreed upon between the author of this code and Agilent.
