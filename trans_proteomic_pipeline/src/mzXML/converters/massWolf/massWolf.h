/***************************************************************************
                             massWolf

This program converts MassLynx native data into mzXML format. The program 
requires the DAC library from Waters to run.

                             -------------------
    begin				: Sat Mar 27
    copyright			: (C) 2004 by Pedrioli Patrick, ISB, Proteomics
    email				: ppatrick@student.ethz.ch
	additional work	by	: Brian Pratt, Insilicos (brian.pratt@insilicos,com)
	                      Ted Holzman, FHCRC (tholzman@fhcrc.org)
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#pragma once

#include "resource.h"
