#!/usr/bin/perl -w

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library or "Lesser" General Public      *
 *   License (LGPL) as published by the Free Software Foundation;          *
 *   either version 2 of the License, or (at your option) any later        *
 *   version.                                                              *
 *                                                                         *
 ***************************************************************************/

/****
 
       Date:  03/06/2009
 
    Purpose:  convert the raw file from local dir.
 
  Copyright:  Zhi Sun, Copyright (c) Institute for Systems Biology, 2009.  All rights reserved.
  
****/ 

# a simple client using IO:Socket
# add suport to convert .yep and .baf file.
# add converter mzML_Exporter (only works on epimethius)

use strict;
use Getopt::Long;
use IO::Socket;
use FindBin qw($Bin);


$Getopt::Long::autoabbrev = 0;
$Getopt::Long::ignorecase = 0;
my (%options, @file, $par, @alloption);
GetOptions(\%options,'server=s', 'default', 'help', 'showoptions', 'convertWith=s',
           'centroid|c','mzXML', 'mzML', 'compress|z', 'verbose|v','nolockspray',
           'MSe', 'I=s', 'T=s', "D=s", 'G', 'coordinate', 's=s', 'AnalystQS', 
           'Analyst','only-compatible', 'showwifftree', 'GC', 'PI=s','PP=s',
           'P1I=s', 'P1P=s', 'c1', 'deisotope|d', 'GPM=f', 'GMA=i', 'GMI=i',
           'FPC=i', 'mode=i','log=s', 'raw=i', 'TD=f', 'TZ=f', 'relTol=f', 
           'absTol=f','maxcharge=i', 'requirePepProfile','profile',
           'ProteinPilotPeakList');
           
printusage() if $Getopt::Long::error;
printusage() if ($options{'help'}); 
printShowOptions() if $options{'showoptions'};


if(! @ARGV)
{
  print "please input a raw file\n"; 
  printusage();
}
foreach (@ARGV)
{
  push (@file, $_);
}

#parse config file 
#the default converter is the first convert in the list for each file type

my %ConverterOptions=();
my %defaultConverter=();
my $configfile = "$Bin/convert.config";
my @config=();
open (CONFIG, "<$configfile") or die "cannot open $configfile\n";
@config=<CONFIG>;
shift @config;

my ($strconfig, @converters, $nextFileType, $i);
$strconfig = join ("\n", @config);
$strconfig =~ s/^\s+//;
$strconfig =~ s/\s+/ /g;
@config = ();
@config = split ("SERVERS:", $strconfig);
@converters=split(":", $config[0]);

$nextFileType = $converters[0];
$nextFileType =~ s/\s+//g;

for ($i=1; $i<scalar @converters; $i++)
{
  my ($filetype, @tmp2, $counter);
  $counter=1;
  $filetype = lc($nextFileType);
  $nextFileType = '';
  $converters[$i]=~ /.*\]\s*(.*)$/;
  $nextFileType = $1;
  if($nextFileType)
  {
    $converters[$i] =~ s/$nextFileType//;
    $nextFileType =~ s/\s+//g;
  }
  @tmp2 = split(/\]/, $converters[$i]);
  pop @tmp2;
  foreach my $ln (@tmp2)
  {
    my ($converter, $options) = split(/\[/, $ln);
    $converter =~ s/^\s+//;
    $converter =~ s/\s+$//;
    $ConverterOptions{$filetype}{$converter} = $options;
    #print "$filetype\t$converter\t$ConverterOptions{$filetype}{$converter}\n";
    if($counter ==1)
    {
      $defaultConverter{$filetype}=$converter;
    }
    $counter++;
  }
}

#parse converter for each machine
my %ConverterOnServer=();
my @servers =split(/\[/, $config[1]);
shift @servers;
for ($i=0; $i<scalar @servers; $i++)
{
  $servers[$i] =~ s/\s+vender software\:.*converters\:\s+//;
  $servers[$i] =~ s/(\=.*?\s+)/ /g;
  my ($server, $converters) = split(/\]/, $servers[$i]);
  my @converters = split(/\s+/, $converters);
  $server =~ s/\+//g;
  foreach (@converters)
  {
    s/\s+//g;
    $ConverterOnServer{$server}{$_} = 1;
    #print "$server\t$_\n";
  }

}
my @commonOption=qw(default help showoptions server convertWith);

$par='';
$par = GetParameters(\%options);

my $host = $options{'server'};
my $port = 7890;
my $log = "convert.log";
open(LOG, ">$log") or die "cannot open $log file\n";

my $convert=0;

for (my $i=0; $i< scalar @file; $i++)
{
    my $sock = new IO::Socket::INET( 
        PeerAddr => $host, 
        PeerPort => $port, 
        Proto => 'tcp',
        Timeout => 5);
    $sock or die "cannot connect to server\n";

    print scalar <$sock>;

    my ($text, $size, $buf, $recv_size, $file_name, $file_type, $converter);
    chomp $file[$i];
    print "$file[$i]\n";

    $file[$i] =~ /(.*)\.(.*)/;
    $file_name = $1;
    $file_type = lc($2);
    $file_name =~ s/\..*//;
    if( -d $file[$i])
    {
      $file_type .='dir';
    }
    if(! $options{'convertWith'})
    {
      $converter=$defaultConverter{$file_type};
    }
    else
    {
      $converter=$options{'convertWith'};
      if(not defined $ConverterOnServer{$host}{$converter})
      {
        print "$host don't have converter $converter\n";
        exit;
      }
    }

    if (not defined $ConverterOnServer{$host}{$converter})
    {
      print "$host don't have converter for $file_type file\n";
      print "please check $configfile file for all converter in $host\n";
      exit;
    }

    print "use converter: $converter\n";
    print $sock "$converter\n";
    print scalar <$sock>;


    if( -f $file[$i])
    { 
      checkoptions($file_type,$converter,\@commonOption,\%ConverterOptions,\%options);
      print $sock "$file[$i]\n";
      print scalar <$sock>;
      my @supplementfile = `ls $file[$i]*`;
      if(@supplementfile >1)
      {
        `zip $file_name.zip $file[$i] $file[$i].*`;
      }
      else
      {
        `zip $file_name.zip $file[$i]`;
      }
    }
    elsif( -d $file[$i])
    {
      checkoptions($file_type,$converter, \@commonOption, \%ConverterOptions,\%options);
      print $sock "$file[$i]\/\n";
      print scalar <$sock>;
      `zip -r $file_name.zip $file[$i]`;
    }
 
    if($options{'default'})
    {
      if('baf yep' =~/$file_type/)
      {
        $par .='-a';
      }
      else
      {
        $par='';
        print "parameter not provided\n";
      }
      print $sock "$par\n";
    }
    else
    {
      $par =~ s/\s+$//;
      print $sock "$par\n";
    }
    print scalar <$sock>;
    
    open(FILE,"$file_name.zip") || die "cannot open $file_name.zip\n";
    binmode(FILE);
    $buf='';

    while (read(FILE, $buf, 204800))
    {
       print $sock "$buf";
       $buf ='';
    }
    print $sock "client finishs sending file\n";

    $text = `ls -l $file_name.zip`;
    $size = (split(/\s+/, $text))[4];
    print "compressed file size: $size\n";
    $recv_size = scalar <$sock>;
    chomp $recv_size;
    print "server received: $recv_size\n";
    
    if($recv_size == $size)
    {
	print "waiting for the server to convert and send file\n";
      open(F,">$file_name.conv.zip");
      binmode(F); 
      $buf ='';
      LOOP:while (defined ($buf = <$sock>)) 
      { 
        if ($buf !~ /server finishs sending converted file/)
        {
          print F $buf;
        }
        else
        {
          $buf =~ s/server finishs sending converted file\n//;
          print F $buf;
          last LOOP;
        }
      }
      print "server finishs sending converted file\n";
      print "server start sending log file\n";
      print LOG "***************************************\n";
      print LOG "$file[$i]\n";
      while (defined ($buf = <$sock>)) 
      {
         print LOG $buf;
      }
      print LOG "FINISH CONVERTING\n";

      close F; 
   
      `unzip -o $file_name.conv.zip`;
      `rm *.zip`;
     print "$converter\t$file_name\n";
      if($converter eq "mzML_Exporter" &&  -e "$file_name.mzML")
      {
        print "********INNNNN*\n";
        `mv $file_name.mzML temp.mzML`;
        open (IN, "<temp.mzML") or die "cannot open temp.mzML\n";
        open (OUT, ">$file_name.mzML") or die "cannot open $file_name.mzML for write\n"; 
        while (<IN>) 
        {
          if (/index="(\d+)"/) 
          {
            my $index=$1;
            /nativeID="([\d\.]+)"/;
            my $native = $1;
            #print "$index, $native from line $_\n";
            my $newnative = $index+1;
            $_ =~ s/nativeID="[\d\.]+"/nativeID="$newnative"/;
            #print "$index, $native from line $_\n";
            print OUT $_;
          }
          else 
          {
            print OUT $_;
          }
        }
        close OUT;
        close IN; 
        `rm temp.mzML`;
      }
      
      print "finish converting file\n\n";      
        
    } 
    else
    {
      print "file transfer error\nresending file\n";
      $i--;
    }
  close FILE;
  close $sock;
}
close LOG;
###########################################################
exit;
###########################################################
sub printShowOptions
{
  print <<"  END";
*******************************************************
ReAdW 4.0.2(build Jul  1 2008 14:23:37)

.RAW FILE CONVERTER

 Options
  --mzXML:         mzXML mode (default)
  --mzML:          mzML mode (EXPERIMENTAL)
      one of --mzXML or --mzML must be selected

  --centroid, -c: Centroid all scans (MS1 and MS2)
      meaningful only if data was acquired in profile mode;
      default: off
  --compress, -z: Use zlib for compressing peaks
      default: off
  --verbose, -v:   verbose

*******************************************************
massWolf 4.0.2(build Jul  1 2008 14:37:30)

raw DIR CONVERTER

 Options
  --mzXML:         mzXML mode (default)
  --mzML:          mzML mode (EXPERIMENTAL)
      one of --mzXML or --mzML must be selected
  --centroid, -c: EXPERIMENTAL
     Centroid all scans (MS1 and MS2);
     meaningful only if data was acquired in profile mode;
     default: off
  --compress, -z: Use zlib for compressing peaks
      default: off
  --verbose, -v:   verbose
  --nolockspray:   Normally massWolf will annotate the lockspray function;
      If the last function is MS and not MSMS, then it is assumed to be lockspray;
      specify '--nolockspray' to turn this off if your last function is MS and not actually lockspray.
  --MSe:           Use if your run is MS^E data (that is, if your run is composed of three functions: Low, High, Lockspray (or two functions-- Low then High only, if --nolockspray set.))

*******************************************************
mzWiff.exe version 4.0.2(build Jul  1 2008 14:34:11)

WIFF FILE CONVERTER
  
  Information options=
    -I"<str>"        where str specifies the ionisation used
    -T"<str>"        where str specifies the mass spectrometry type
    -D"<str>"        where str specifies detector used
    -G               use information recorded in wiff file; default off

  General options=
    --compress, -z   use zlib to compress peaks
    --verbose, -v    verbose mode; default: quiet mode
    --mzXML          mzXML output format
    --coordinate     report native scan refernce
    --mzML           mzML output format
     -s<num>-<num>   report only these range of sample ids;
                     -s1   : only sample#1
                     -s3-  : only sample#3 onward
                     -s-6  : only sample#1 to sample#6
                     -s2-4 : only sample#2 to sample#4
    --AnalystQS      assume AnalystQS library
    --Analyst        assume Analyst library
    --only-compatible
                     convert only if library is compatible
    --showwifftree   print wiff tree structure on console

  Processing Operation options=
    -GC (disabled)             determine precursor charge; default off
    -PI<num1>-<num2> where num1 and num2 are float specifying min and max peak
                     intensity for MS/MS to be considered as signal; default 0
                     (i.e. no thresholding applied)
                     -PI0.3     : only peak with intensity >=0.3
                     -PI0.3-    : same as -PI0.3
                     -PI-6.4    : only peak with intensity <=6.4
                     -PI0.3-6.4 : only peak satisfying 0.3<=intensity<=6.4
    -PP<num1>-<num2> where num1 and num2 are float specifying min and max % of
                     max peak intensity for MS/MS to be considered as signal; 
                     default 0 (0-100); instrument and data dependent
                     -PP0.1      : only peak with intensity >=0.1% of maxInt
                     -PP0.1-     : same as -PP0.1
                     -PP-97.6    : only peak with intensity <=97.6% of maxInt
                     -PP0.1-97.6 : only peak satisfying 
                                   0.1<=intensity/maxInt*100<=97.6
    -P1I<num1>-<num2> same as -PI<num1>-<num2> above but applies to MS data
    -P1P<num1>-<num2> same as -PP<num1>-<num2> above but applies to MS data
    -c1              centroid MS data; default off
    --centroid, -c   centroid MS/MS data; default off
    --deisotope, -d  deisotope MS/MS data; default off

  MS/MS Averaging options=
    -GPM<num>      where num is a float specifying the precursor mass tolerance
                   to be considered for grouping; unit da; default: 0, i.e.
                   no MS2 averaging; suggested: 1
    -GMA<num>      where num is a int specifying the max cycles span allowed
                   within a group; default 10
    -GMI<num>      where num is a int specifying the min cycles per group;
                   within a group; default 1

  MS/MS Filtering options=
    -FPC<num>      where num is a int specifying the min peak count to include
                   a spectra in output; default 10

*******************************************************
CompassXport command line utility version 1.3.6
------------------- command line usage -------------------------

.YEP .BAF FILE CONVERTER

Optional Parameters:

1) -mode 1/0: Default: 0
              mzXML (0) or mzData (1).
2) -log level:Set log level
              Default: all Set log level:
              level can be: (non, error, all)
3) -raw X: Switch to control raw data export:
           X can be 1 (export raw data) or 0 (export line spectra)
           Default: 0

Input file name can be:
- analysis.baf acquisition data files
- analysis.yep acquisition data files
- AutoXecute run file for LCMaldi (not support in remotecomverter)
- fid acquisition data files  (not support in remotecomverter)

****************************************************************

mzML Exporter (only works on epimethius)

The mzML_Exporter command-line tool converts a .wiff file into mzML

 -profile
 -centroid
 -ProteinPilotPeakList

****************************************************************

trapper version 4.1.0-pr (build Jul 17 2008 12:21:45)

.d dir CONVERTER 

requires MHDAC API version 1.2.2

  --mzXML:         mzXML mode
                    (--mzML will be supported in an upcoming version;
                     one of --mzXML or --mzML must be selected)

  --centroid, -c:  Centroid scans (meaningful only
                    if data was acquired in profile mode;
                     default: not active)
  --verbose, -v:   verbose
  --deisotope
  --relTol <n.n>
  --absTol <n.n>
  --maxCharge <n>
  --requirePepProfile

Author: Joshua Tasman (ISB/SPC)

  END

  exit;
}

sub printusage
{
  print <<"  END";

  Usage: remoteconvert.pl OPTIONS inputfile(s)orfolder(s)
    e.g.: remoteconvert.pl --default 1min.RAW 
          remoteconvert.pl -server ep --default 1min.RAW
          remoteconvert.pl --c --mzXML --z 1min.RAW
          remoteconvert.pl --mzXML --z --I \\\"IT\\\" fraction.wiff
          remoteconvert.pl --default --convertWith mzML_Exporter *.wiff
  Options:
  --showoptions Show the supported options for the supported remote converters
  --help Show this usage statement
  --default
     mzWiff: -c -c1 --mzXML
     ReAdW: -c --mzXML
     massWolf: --mzXML
     trapper: --mzXML -c (beta version)
     mzML_Exproter: -ProteinPilotPeakList
     compassXport: -a
  --convertWith choose the converter you want to use. Currently, there are two
        converters for wiff file, mzWiff and mzML_Exporter. For all other file 
        type, we only have one converter for each.
  NOTE: if an option needs an input value, there should be a space between 
        the option and the input value.
  --server: eb for "ebox", ep for "epimethius". The default value is "eb"

  END
  exit;
}
#***********************************************************
# check if the input options are acceptable to the converter
#************************************************************
sub checkoptions
{
   my $file_type=shift;
   my $converter=shift;
   my $coption=shift;
   my $eoption=shift;
   my $options = shift;

   if(! $eoption->{$file_type}{$converter})
   {
     print "converter $converter not supported\n";
     exit;
   } 
   my @array1;
   @array1 = split(/\s+/, $eoption->{$file_type}{$converter});
   my %count = ();
   push(@array1, @$coption);
   foreach my $element (@array1)
   { 
     $count{$element}++;
    # print "$element\t"; 
   }
   #print "\n";
    my @inputOptions = keys %$options;
    my @NAO;
    foreach (@inputOptions)
    {
      if(not defined $count{$_})
      {
        push(@NAO, $_);
      }
    }

    if(scalar @NAO >=1)
    {
       print join(" ", @NAO), " are not acceptable options for the converter\n";
       print "please check --showoptions for detail\n";
       exit;
    }
}

#***********************************************************
# generate parameter from the input options
#************************************************************
sub GetParameters
{
  my $options = shift;
  my $par;
  my @opt;

  if(! $options->{'server'} ||$options->{'server'} eq 'eb')
  {
    $options->{'server'}='ebox';
  }
  elsif( $options->{'server'} eq 'ep'){$options->{'server'}='epimethius';}
  else { print "server  $options->{'server'} is not supported\n"; exit;}


  if($options->{'centroid'}){$par .='-centroid ';}
  if($options->{'mzXML'}){$par .='--mzXML ';}
  if($options->{'mzML'}){$par .='--mzML ';}
  if($options->{'compress'}){$par .='-z ';}
  if($options->{'verbose'}){$par .='-v ';}
  if($options->{'nolockspray'}){$par .='--nolockspray ';}
  if($options->{'MSe'}){$par .='--MSe ';}
  if($options->{'coordinate'}){$par .='--coordinate ';}
  if($options->{'AnalystQS'}){$par .='--AnalystQS ';}
  if($options->{'Analyst'}){$par .='--Analyst ';}
  if($options->{'only-compatible'}){$par .='--only-compatible ';}
  if($options->{'showwifftree'}){$par .='--showwifftree ';}
  if($options->{'deisotope'}){$par .='-d ';}
  if($options->{'requirePepProfile'}){$par .='--requirePepProfile ';}
  
  if($options->{'c1'}){$par .='-c1 ';}
  #if($options->{'GC'}){$par .='-GC ';}
  if($options->{'G'}){$par .='-G ';}
  if($options->{'profile'}){$par .='-profile ';}
  if($options->{'ProteinPilotPeakList'}){$par .='-ProteinPilotPeakList ';}

  @opt = qw (I T D);
  foreach (@opt)
  {
    if($options->{$_})
    { 
      #if($options->{$_}=~ /^\".*\"$/ )
      if($options->{$_}!~/--/ && lc($options->{$_})!~/\.wiff/&& lc($options->{$_})!~/\.raw/)
      {
        $par .= "-$_\"$options->{$_}\" ";
      }
      else
      {
        print "\noption $_ needs an input string put between double quote\n\n";
        print "$options->{$_}\n";
        printusage();
      }
    }
  }
  @opt = qw (GPM GMA GMI FPC TD TZ absTol relTol maxCharge);
  foreach (@opt)
  {
    if($options->{$_})
    {
      if($options->{$_}=~ /^\d+/ || $options->{$_}=~ /^\.\d+/)
      {
        if($_ eq 'TD' || $_ eq 'TZ' || $_ eq 'relTol' || $_ eq 'absTol'
           || $_ eq 'maxCharge')
        {
          $par .= '--'.$_." ".$options->{$_}.' ';
        }
        else
        {
          $par .= '-'.$_.$options->{$_}.' ';
        }
      }
      else
      {
        print "\nplease input a number for option $_\n\n";
        printusage();
      }
    }
  }

  @opt =();
  @opt = qw (s PI PP P1I P1P);
  foreach (@opt)
  {
    if($options->{$_})
    { 
      my $str = $options->{$_};
      if($str =~ /--/)
      {
        print "\ninvalid range for option $_\n";
        printusage();
      }
      if($str=~/\d+(.\d+)*/ || $str=~/\d+(.\d+)*\-/ ||
         $str=~/\-\d+(.\d+)*/ || $str=~/\d+(.\d+)?\-\d+(.\d+)*/)
      {  
        $par .= '-'.$_.$options->{$_}.' ';
      }
      else
      {
        print "\ninvalid range for option $_\n\n";
        printusage();
      }
    }
  }
  @opt = qw (mode raw);
  foreach (@opt)
  {
    if(defined $options->{$_})
    {
      if($options->{$_} ==1 || $options->{$_}==0 )
      {  
        $par .= '-'.$_.' '.$options->{$_}.' ';   
      }
      else
      {
        print "\nexpecting 0 or 1 for option $_\n";
        printusage();
      }
    }
  }
  if($options->{'log'})
  { 
    if($options->{'log'} ne 'non'||$options->{'log'} ne 'error'
        || $options->{'log'} ne 'all')
    {
      print "\nexpecting (non/error/all) for option log\n";
      printusage();
    }
    else
    {
      $par .= '-'.$_.' '.$options->{$_}.' ';
    }
  }
  return $par;
}
