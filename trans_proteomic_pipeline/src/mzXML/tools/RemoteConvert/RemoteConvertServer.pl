#!/usr/bin/perl

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library or "Lesser" General Public      *
 *   License (LGPL) as published by the Free Software Foundation;          *
 *   either version 2 of the License, or (at your option) any later        *
 *   version.                                                              *
 *                                                                         *
 ***************************************************************************/

/****
 
       Date:  03/06/2009
 
    Purpose:  convert the raw file from local dir.
 
  Copyright:  Zhi Sun, Copyright (c) Institute for Systems Biology, 2009.  All rights reserved.
  
****/ 


use IO::Handle;
use IO::Socket;
use Sys::Hostname;
use strict;
use POSIX qw(:sys_wait_h);

require Exporter;
use Carp;
use Errno qw(ENOENT EPERM);

our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( all => [ qw( fork_now
				  weight
				  waitpids
				  run_back
				  run_back_now
				  system_back
				  system_back_now
				  all_exit_ok
				  running_now ) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );
our @EXPORT = qw();

# are we running on Windows?
use constant _win => $^O=~/win32/i;

# parameters
my $queue_size=2; # max number of councurrent processes running.
my $debug=0; # shows debug info.
my $trace=0; # shows function calls.
my $delay=0.2; # delay between fork calls.
my $ignore_children=0; # similar to $SIG{CHILD}='IGNORE';

my $weight=1;
my $allow_excess=1;

my $last=0; # last time fork was called.

# module status
my $queue_now=0;
my %process;
my @captured;

*CORE::GLOBAL::wait = \&new_wait;
*CORE::GLOBAL::waitpid = \&new_waitpid;
*CORE::GLOBAL::exit = \&new_exit;
*CORE::GLOBAL::fork = \&new_fork;


size(10); 
my $MAXPROCESS=3;

debug(0); 
my $host = hostname;
local $| = 1;
my $sock = new IO::Socket::INET(
  LocalHost => $host,
  LocalPort => 7890,
  Proto => 'tcp',
  Type => SOCK_STREAM,
  Listen => 5,
  Reuse => 1);

$sock or die "no socket :$!";
print "$sock\n";
STDOUT->autoflush(1);

my %defaultParameter=();
$defaultParameter{'ReAdW'}= '-c --mzXML';
$defaultParameter{'massWolf'} = '--mzXML';
$defaultParameter{'mzWiff'} = '-c -c1 --mzXML';;
$defaultParameter{'trapper'} = '--mzXML -c';
$defaultParameter{'mzML_Exporter'} = '-ProteinPilotPeakList';
$defaultParameter{'CompassXport'} = '-a';



my($new_sock, $c_addr, $buf, $kid, $counter, $runningProcess, $connected);
my $notignored_child=0;
$runningProcess=0;

while (($new_sock, $c_addr) = $sock->accept()) {
  my ($client_port, $c_ip) =sockaddr_in($c_addr);
  my $client_ipnum = inet_ntoa($c_ip);
  my $client_host =gethostbyaddr($c_ip, AF_INET);

  print "running process $runningProcess\n";
  print "ignored ", $runningProcess- $notignored_child, "\n";
  if($notignored_child == $MAXPROCESS)
  {
    my $msg ="server is busy, waiting for other processes to finish\n";
    print $new_sock $msg;
    next if $kid = _fork();
  }
  elsif($notignored_child == 0)
  {
    print $new_sock "server is free\n";
    `del tmp*`;
    next if $kid = fork_now();
  }
  else
  {
    print $new_sock "server is free\n";
    next if $kid = fork_now();
  } 
    
  # execute a fork, if this is
  # the parent, its work is done,
  # go straight to next
  #next if $kid = fork_now();
  die "fork: $!" unless defined $kid;
  print "running process now ", $runningProcess+1,"\n";
  $notignored_child++;

  # child now...
  # close the server - not needed
  $counter = $$;
  print "new child process $$\n";
  #close $sock; 
  print "got a connection from: $client_host"," [$client_ipnum] \n"; 
 
  my ($count, $text, $recv_size, $time);
  my ($file_name, $cmd, $file_type, $msg, $converter);
  my $parameter;

  close STDOUT;
  open (OUTPUT, ">tmp$counter.log") or die $!;
  STDOUT->fdopen( \*OUTPUT, 'w' ) or die $!;
  open(STDERR,">&STDOUT");

  $time = localtime();
  print "$time\n";
   
  $converter = scalar <$new_sock>;
  chomp $converter;
  print $new_sock "server got converter type\n"; 
  
  $file_name = scalar <$new_sock>;
  chomp $file_name;
  $file_name =~ /(.*)\.(.*)/;
  $file_name = $1;
  $file_type = $2;

  $connected =0;
  checkstatus($new_sock, $counter, $file_name, $file_type);
  print $new_sock "server got $file_name.$file_type\n";
  
  $parameter = scalar <$new_sock>;
  chomp $parameter;
  $connected =0;
  if(!$parameter)
  {
    checkstatus($new_sock, $counter, $file_name, $file_type);
    print $new_sock "will use default parameter\n";
    $parameter = $defaultParameter{$converter};
  }
  else
  {
    checkstatus($new_sock, $counter, $file_name, $file_type);
    print $new_sock "parameter:$parameter\n";

  }

  open(FILE,">tmp$counter.zip");
  binmode(FILE); 
  $count=0;
  checkstatus($new_sock, $counter, $file_name, $file_type);

  LOOP:while (defined ($buf = <$new_sock>)) 
  { 
    if ($buf !~ /client finishs sending file/)
    {
       print FILE $buf;
    }
    else
    {
     $buf =~ s/client finishs sending file\n//;
     print FILE $buf;
     last LOOP;
    }
  }
  close FILE;

  checkstatus($new_sock, $counter, $file_name, $file_type);

  #$text = `ls -l tmp$counter.zip`;
  #$recv_size = (split(/\s+/, $text))[4];
  #print "received file size: $recv_size\n\n";


  $text = `dir tmp$counter.zip`;
  my @tmp = split(/\n/, $text);
  $text = $tmp[6];
  $text =~ /File.*\s+(.*)\s+bytes/;
  
  $recv_size = $1;
  $recv_size =~ s/\,//g;

  print "received file size: $recv_size\n\n";
  checkstatus($new_sock, $counter, $file_name, $file_type);
  print $new_sock "$recv_size\n";
  print "UNZIPPING...\n";

  if($file_type =~/\/$/)
  {
    $msg=`7-zip\\7z -y x tmp$counter.zip`;
  }
  else
  {
    $msg=`7-zip\\7z -y e tmp$counter.zip`;
  }
  print "$msg\n";
 
  checkstatus($new_sock, $counter, $file_name, $file_type); 

  print "CONVERTING...\n";
  if($converter eq 'mzML_Exporter')
  {
    $cmd = "mzML_Exporter\\$converter WIFF $file_name.$file_type $parameter";
    $msg=`mzML_Exporter\\mzML_Exporter WIFF $file_name.$file_type $parameter`;
  }
  else
  {
    my $ftype = $file_type;
    $ftype =~ s/\/$//;
    if($parameter =~/centroid/)
    {
      $parameter =~ s/\-centroid/\-\-centroid/;
    }
    $cmd = "$converter $parameter $file_name.$ftype";
    $msg=`$converter $parameter $file_name.$ftype`;
  }

  print "\ncommand: $cmd\n";
  print "$msg\n";

  checkstatus($new_sock, $counter, $file_name, $file_type);
  print "ZIPPING...\n";
  $msg=`7-zip\\7z -y a $file_name.zip $file_name.mz* $file_name-*.mz* `;
  print "$msg\n";
  
  print "SENDING CONVERTED FILE\n";
  open(F,"$file_name.zip");
  binmode(F); 
  $buf ='';
  while (read(F, $buf, 204800))
  {
    print $new_sock $buf;
    $buf ='';
  }

  checkstatus($new_sock, $counter, $file_name, $file_type);
  print $new_sock "server finishs sending converted file\n";
  print "SENDING LOG FILE\n";

  close OUTPUT;

  open(LOG, "<tmp$counter.log") or die "cannot open tmp$counter.log file\n";
  while (read(LOG, $buf, 1024))
  {
    print $new_sock $buf;
    $buf ='';
  }

  
  close LOG;
  close STDOUT;
  close FILE;
  close F;
  cleanup($counter, $file_name,$file_type);
  close $new_sock;
  
  exit;
}continue{
    # parent closes the client since
    # it is not needed
    close $new_sock;  
    $runningProcess=running_now();
}

#******************************************************************
# below subs is copy from PROC::Queue package
# http://search.cpan.org/~salva/Proc-Queue-1.23/lib/Proc/Queue.pm
#******************************************************************

sub cleanup
{
  my $counter=shift;
  my $file_name=shift;
  my $file_type=shift;

  #STDERR->autoflush;  
  print "type $file_type\n";
  if($file_name ne '' && $file_type ne '')
  {
    if($file_type =~/\/$/)
    {
       $file_type =~ s/\/$//;
       `rmdir $file_name.$file_type /s/q`;
       `del $file_name*`;
    }   
    else { `del $file_name*`;} 
  }
  `del tmp$counter*`;
  _waitpid ($counter, WNOHANG);
}
sub checkstatus
{ 
  my $socket = shift;
  my $counter= shift;
  my $file_name=shift;
  my $file_type=shift;

  # check if socket is closed
  if (!defined($socket)) {
     cleanup($counter, $file_name, $file_type);
     $notignored_child--;
   }
}

sub import {
  my ($pkg,@opts)=@_;
  my $i;
  for ($i=0; $i<@opts; $i++) {
    my $o=$opts[$i];
    if( $o eq 'size'
        or $o eq 'debug'
        or $o eq 'trace'
	or $o eq 'delay'
	or $o eq 'weight'
        or $o eq 'ignore_childs'
	or $o eq 'ignore_children'
        or $o eq 'allow_excess') {
      $#opts>$i or croak "option '$o' needs a value";
      my $value=$opts[$i+1];
      { no strict qw( subs refs );
	&$o($value) }
      splice @opts,$i--,2;
    }
  }
  carp "Exporting '".join("', '",@opts)."' symbols from Proc::Queue" if $debug;
  @_=($pkg,@opts);
  goto &Exporter::import;
}

sub delay {
  return $delay unless @_;
  my $old_delay=$delay;
  $delay=$_[0];
  carp "Proc queue delay set to ${delay}s, it was $old_delay" if $debug;
  $old_delay;
}

sub size {
  return $queue_size unless @_;
  my $size=shift;
  my $old_size=$queue_size;
  croak "invalid value for Proc::Queue size ($size), min value is 1"
    unless $size >= 1;
  $queue_size=$size;
  carp "Proc queue size set to $size, it was $old_size" if $debug;
  $old_size;
}

sub weight {
  return $weight unless @_;
  my $old_weight=$weight;
  croak "invalid value for Proc::Queue weight ($_[0]), min value is 1"
    unless int($_[0])>=1;
  $weight=int($_[0]);
  carp "Proc weight set to $weight, it was $old_weight" if $debug;
  $old_weight;
}

sub debug {
  return $debug unless @_;
  my $old_debug=$debug;
  if ($_[0]) {
    $debug=1;
    carp "debug mode ON";
  }
  else {
    $debug=0;
    carp "debug mode OFF" if $old_debug;
  }
  return $old_debug;
}

sub trace {
  return $trace unless @_;
  my $old_trace=$trace;
  if ($_[0]) {
    $trace=1;
    carp "trace mode ON" if $debug;
  }
  else {
    $trace=0;
    carp "trace mode OFF" if $debug;
  }
  return $old_trace;
}

sub  allow_excess {
  return $allow_excess unless @_;
  my $old_allow_excess=$allow_excess;
  if ($_[0]) {
    $allow_excess=1;
    carp "allow_excess mode ON" if $debug;
  }
  else {
    $allow_excess=0;
    carp "allow_excess mode OFF" if $debug;
  }
  $old_allow_excess;
}

sub ignore_children {
  return $ignore_children unless @_;
  my $old_ignore_children=$ignore_children;
  if ($_[0]) {
    $ignore_children=1;
    carp "ignore_children mode ON" if $debug;
  }
  else {
    $ignore_children=0;
    carp "ignore_children mode OFF" if $debug;
  }
  return $old_ignore_children;
}

*ignore_childs = \&ignore_children;

# sub to store internally captured processes
sub _push_captured {
  push @captured, shift,$? unless $ignore_children;
  croak "captured stack is corrupted" if (@captured & 1)
}

# do the real wait and housekeeping
sub _wait () {
  carp "Proc::Queue::_wait private function called" if $debug && $trace;
  carp "Waiting for child processes to exit" if $debug;
  my $w=CORE::wait;
  if ($w != -1) {
    if(exists $process{$w}) {
      $queue_now -= delete($process{$w});
      carp "Process $w has exited, $queue_now processes running now" if $debug;
    }
    else {
      carp "Unknow process $w has exited, ignoring it" if $debug;
    }
  }
  else {
    carp "No child processes left, continuing" if $debug;
  }
  return $w;
}

sub new_wait () {
  carp "Proc::Queue::wait called" if $trace;
  if(@captured) {
    my $w=shift @captured;
    $?=shift @captured;
    carp "Wait returning old child $w captured in wait" if $debug;
    return $w;
  }
  return _wait;
}

sub _waitpid ($$) {
  my ($pid,$flags)=@_;
  carp "Proc::Queue::_waitpid($pid,$flags) private function called" if $debug && $trace;
  carp "Waiting for child process $pid to exit" if $debug;
  my $w=CORE::waitpid($pid,$flags);
  if ($w != -1) {
    if(exists $process{$w}) {
      $queue_now -= delete($process{$w});
      carp "Process $w has exited, $queue_now processes running now" if $debug;
    }
    else {
      carp "Unknow process $w has exited, ignoring it" if $debug;
    }
  }
  else {
    carp "No child processes left, continuing" if $debug;
  }
  return $w;
}

sub _clean() {
  my $pid;
  while (1) {
    $pid=_waitpid(-1,WNOHANG);
    return unless ((_win && $pid < -1) || $pid >0);
    _push_captured $pid
  }
}

sub new_waitpid ($$) {
  my ($pid,$flags)=@_;
  carp "Proc::Queue::waitpid called" if $trace;
  foreach my $i (0..$#captured) {
    next if $i&1;
    my $r;
    if ($pid==$captured[$i] or $pid==-1) {
      croak "corrupted captured stack" unless ($#captured & 1);
      ($r,$?)=splice @captured,$i,2;
      return $r;
    }
  }
  return _waitpid($pid,$flags);
}

sub new_exit (;$ ) {
  my $e=@_?shift:0;
  carp "Proc::Queue::exit(".(defined($e)?$e:'undef').") called" if $trace;
  carp "Process $$ exiting with value ".(defined($e)?$e:'undef') if $debug;
  return CORE::exit($e);
}

# use Time::Hires::time if available;
BEGIN { eval "use Time::HiRes 'time'" }

sub _fork () {
  carp "Proc::Queue::_fork called" if $trace && $debug;
  if ($delay>0) {
    my $wait=$last+$delay - time;
    if ($wait>0) {
      carp "Delaying $wait seconds before forking" if $debug;
      select (undef,undef,undef,$wait);
    }
    $last=time;
  }
  my $f=CORE::fork;
  if (defined($f)) {
    if($f == 0) {
      carp "Process $$ now running" if $debug;
      # reset queue internal vars in child proccess;
      $queue_size=1;
      $queue_now=0;
      %process=();
      @captured=();
    }
    else {
      $process{$f}=$weight;
      $queue_now+=$weight;
      carp "Child forked (pid=$f), $queue_now processes running now" if $debug;
    }
  }
  else {
    carp "Fork failed: $!" if $debug;
  }
  return $f;
}

sub new_fork () {
  carp "Proc::Queue::fork called" if $trace;
  while($queue_now and
        $queue_now + ($allow_excess ? 1 : $weight) > $queue_size) {
    carp "Waiting for some process to finish" if $debug;
    my $nw;
    if (($nw=_wait) != -1) {
      _push_captured $nw;
    }
    else {
      carp "Proc queue seems to be corrupted, $queue_now children lost";
      last;
    }
  }
  return _fork;
}

sub fork_now () {
  carp "Proc::Queue::fork_now called" if $trace;
  return _fork;
}

sub waitpids {
  carp "Proc::Queue::waitpids(".join(", ",@_).")" if $trace;
  my @result;
  foreach my $pid (@_) {
    if (defined($pid)) {
      carp "Waiting for child $pid to exit" if $debug;
      my $r=new_waitpid($pid,0);
      if ((_win && $r < -1) || $r > 0) {
	carp "Child $r return $?" if $debug;
	push @result,$r,$?;
      }
      else {
	carp "No such child returned while waiting for $pid" if $debug;
	push @result,$r,undef;
      }
    }
    else {
      carp "Undef arg found";
      push @result,undef,undef
    };
  }
  return @result;
}


sub _run_back {
  my ($now,$code)=@_;
  carp "Proc::Queue::_run_back($now,$code) called" if $trace and $debug;
  my $f=$now ? fork_now : new_fork;
  if(defined($f) and $f==0) {
    carp "Running code $code in forked child $$" if $debug;
    $?=0;
    eval {
      &$code();
    };
    if ($@) {
      carp "Uncaught exception $@" if $debug;
      new_exit(255)
    }
    else {
      carp "Code $code in child $$ returns '$?'" if $? && $debug;
    }
    new_exit($?)
  }
  return $f;
}

sub run_back (&) {
  my $code=shift;
  carp "Proc::Queue::run_back($code) called" if $trace;
  return _run_back(0,$code)
}

sub run_back_now (&) {
  my $code=shift;
  carp "Proc::Queue::run_back_now($code) called" if $trace;
  return _run_back(1,$code)
}

sub _system_back {
  my $now=shift;
  carp "Proc::Queue::_system_back($now, ".join(", ",@_).") called" if $trace and $debug;
  if (0 and @_ > 1) { # TODO, search command on the PATH
      unless (-e $_[0]) {
          carp "command '$_[0]' not found" if $debug;
          $! = ENOENT;
          return undef;
      }
      unless (-f _ and -x _) {
          carp "permission to execute command '$_[0]' denied" if $debug;
          $! = EPERM;
          return undef;
      }
  }
  my $f=$now ? fork_now : new_fork;
  if(defined($f) and $f==0) {
    carp "Running exec(".join(", ",@_).") in forked child $$" if $debug;
    { exec(@_) }
    carp "exec(".join(", ",@_).") failed" if $debug;
    require POSIX;
    POSIX::_exit(255);
  }
  return $f;
}

sub system_back {
  carp "Proc::Queue::system_back(".join(", ",@_).") called" if $trace;
  return _system_back(0,@_);
}

sub system_back_now {
  carp "Proc::Queue::system_back_now(".join(", ",@_).") called" if $trace;
  return _system_back(1,@_);
}

sub all_exit_ok {
  carp "Proc::Queue::all_exit_ok(".join(", ",@_).")" if $trace;
  my @result=waitpids(@_);
  my $i;
  for($i=0; $i<@result; $i++) {
    next unless $i&1;
    if (!defined($result[$i]) or $result[$i]) {
      carp "Child ".$_[$i>>1]." fail with code $result[$i], waitpid return $result[$i-1]" if $debug;
      return 0;
    }
  }
  carp "All children run ok" if $debug;
  return 1;
}

# this function is mostly for testing pourposes:
sub running_now () {
  _clean;
  return $queue_now;
}



