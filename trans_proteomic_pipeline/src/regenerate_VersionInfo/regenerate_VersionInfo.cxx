/*

Program       : regenerate_VersionInfo
Author        : Brian Pratt, Insilicos
Date          : 9-18-05

Routine to create a timestamped TPP build info string - text is
taken from src/common/TPPVersion.h


Copyright (C) 2005,2007 Insilicos LLC, Institute for Systems Biology

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "common/TPPVersion.h"
#include <time.h>
#include "common/sysdepend.h"
#include "common/constants.h"
#include <string.h>

#if !defined(_MSC_VER) && !defined(__MINGW32__)
#include <errno.h>
#endif


static char *findRightmostPathSeperator(char *path) {
   char *seek = path+strlen(path);
   while (seek>=path) {
      if (('\\'==*seek)||('/'==*seek)) {
         return seek;
      }
   }
   return NULL;
}

int noreplace = 0; // if nonzero, don't replace existing

FILE *checked_fopen(const char *file, const char *mode) {
   if (noreplace) {
       FILE *test = fopen(file,"r");
       if (test) {
           fclose(test);
           return NULL;  // exists, don't overwrite
       }
   }
   FILE *result = fopen(file,mode);
   if (!result) {
      fprintf(stderr,"can't open %s in mode %s\n",file,mode);
      exit(1);
   }
   return result;
}


int main(int argc, char *argv[]) {
  time_t now = time(NULL);
  struct tm* tmstruct = localtime(&now);
  char fullVersionString[1024];
  char versionString[1024];
  char versionStringNoSpaces[1024];
  char buildString[1024];  
  char buildnumString[1024];
  bool wrote = false;

  FILE *versionInfo;

  if (argc>1){
      if (!strcmp(argv[1],"-noreplace")) { // don't overwrite existing
          noreplace = 1;
          argc--;
      }
  }
  if (argc>1){
	  if (chdir(argv[noreplace+1])) {
		printf("regenerate_VersionInfo failed to chdir(%s) - %s",argv[1],strerror(errno));
		exit(1);
	  }
  }

  // format the version string
  snprintf(versionString,sizeof(versionString),"v%d.%d %s rev %d",
	   TPP_MAJOR_RELEASE_NUMBER, TPP_MINOR_RELEASE_NUMBER, TPP_RELEASE_NAME, TPP_REV_RELEASE_NUMBER);
  strcpy(versionStringNoSpaces,versionString);
  for (char *c=versionStringNoSpaces;*c;c++) {
	  if (!isalnum(*c)) {
		  *c = '_';
	  }
  }

  snprintf(buildnumString,sizeof(buildnumString),"%04d%02d%02d%02d%02d",
     tmstruct->tm_year+1900, tmstruct->tm_mon+1, tmstruct->tm_mday,
     tmstruct->tm_hour, tmstruct->tm_min);

  snprintf(buildString,sizeof(buildString),"Build %s (%s)",buildnumString, TPP_ARCH); 

  snprintf(fullVersionString,sizeof(fullVersionString),"%s %s, %s",
     TPP_PRODUCT_NAME, versionString, buildString);

  // for C++
  versionInfo = checked_fopen("TPPVersionInfo.cxx","w"); // replace
  if (versionInfo) {
  fprintf(versionInfo,"// THIS IS A GENERATED FILE, DO NOT EDIT OR PLACE UNDER REVISION CONTROL\n");
  fprintf(versionInfo,"#include \"common/TPPVersion.h\"\n"); // ensure consistency with declaration
  fprintf(versionInfo,"const char *szTPPVersionInfo=\"%s\";\n", fullVersionString);
  fprintf(versionInfo,"const time_t globTPPBuildTime = %ld;\n",now);
  fclose(versionInfo);
  wrote = true;
  }

  // for Perl
  versionInfo = checked_fopen("TPPVersionInfo.pl","w"); // replace
  if (versionInfo) {
  fprintf(versionInfo,"# THIS IS A GENERATED FILE, DO NOT EDIT OR PLACE UNDER REVISION CONTROL\n");
  fprintf(versionInfo,"sub getTPPVersionInfo {\n  my $versionInfo = \"%s\";\n  return $versionInfo;\n}\n", fullVersionString);
  fprintf(versionInfo,"sub get_tpp_hostname {\n my $hostname = `tpp_hostname`;\n  if (\"\" eq $hostname) {\n    $hostname=\"localhost\";\n  }\n  return $hostname;\n}\n");
  fprintf(versionInfo,"1; # required so that file can be correctly included in another script\n   #- gives a 'true' response when loaded \n");
  fclose(versionInfo);
  wrote = true;
  }

#ifdef WINDOWS_NATIVE
  // for NSIS installer, if you're using one
  versionInfo = checked_fopen("TPPVersionInfo.nsh","w"); // replace
  if (versionInfo) {
  fprintf(versionInfo,"; THIS IS A GENERATED FILE, DO NOT EDIT OR PLACE UNDER REVISION CONTROL\n");
  fprintf(versionInfo,"!define PRODUCT_FULLNAME \"%s\"\n",TPP_PRODUCT_FULLNAME);
  fprintf(versionInfo,"!define PRODUCT_NAME \"%s\"\n",TPP_PRODUCT_NAME);
  fprintf(versionInfo,"!define PRODUCT_VERSION \"%s\"\n",versionString);
  fprintf(versionInfo,"!define PRODUCT_VERSION_NOSPACES \"%s\"\n",versionStringNoSpaces);
  fprintf(versionInfo,"!define PRODUCT_MAJOR_VERSION_NUMBER \"%d\"\n",TPP_MAJOR_RELEASE_NUMBER);
  fprintf(versionInfo,"!define PRODUCT_MINOR_VERSION_NUMBER \"%d\"\n",TPP_MINOR_RELEASE_NUMBER);
  fprintf(versionInfo,"!define PRODUCT_REV_VERSION_NUMBER \"%d\"\n",TPP_REV_RELEASE_NUMBER);
  fprintf(versionInfo,"!define PRODUCT_BUILD_NUMBER \"%s\"\n",buildString);
  fclose(versionInfo);
  wrote = true;
  }
#endif

#if !defined(WINDOWS_CYGWIN) && !defined(WINDOWS_NATIVE)
  // for RPM installer, if you're using one
  char rpmString[1024];
  versionInfo = checked_fopen("rpm.spec","w"); // replace
  if (versionInfo) {
  fprintf(versionInfo,"# THIS IS A GENERATED FILE, DO NOT EDIT OR PLACE UNDER REVISION CONTROL\n");
  fprintf(versionInfo,"Summary: %s\n",TPP_PRODUCT_FULLNAME);
  fprintf(versionInfo,"Name: %s\n",TPP_PRODUCT_NAME);
  fprintf(versionInfo,"Version: %d.%d.%d\n", TPP_MAJOR_RELEASE_NUMBER,TPP_MINOR_RELEASE_NUMBER,TPP_REV_RELEASE_NUMBER);
  fprintf(versionInfo,"Release: %s\n",buildnumString);
  fprintf(versionInfo,"Source0: %%{name}-%%{version}.tar.gz\n");
  fprintf(versionInfo,"License: Commercial and LGPL (tpplib)\n");
  fprintf(versionInfo,"Group: Applications/Proteomics\n");
  fprintf(versionInfo,"BuildRoot: %%{_builddir}/%%{name}-root\n");
  fprintf(versionInfo,"%%description\n");
  fprintf(versionInfo,"%s\n", TPP_BRIEF_DESCRIPTION);
  fprintf(versionInfo,"%%prep\n");
  fprintf(versionInfo,"%%setup -q\n");
  fprintf(versionInfo,"%%build\n");
  fprintf(versionInfo,"%%install\n");
  fprintf(versionInfo,"[ ${RPM_BUILD_ROOT} != \"/\" ] &&rm -rf $RPM_BUILD_ROOT\n");
  fprintf(versionInfo,"make DESTDIR=$RPM_BUILD_ROOT BASEDIR=%s  install\n",DEFAULT_TPP_INSTALL_ROOT);
  fprintf(versionInfo,"%%clean\n");
  fprintf(versionInfo,"[ ${RPM_BUILD_ROOT} != \"/\" ] && rm -rf ${RPM_BUILD_ROOT}\n");
  fprintf(versionInfo,"%%post\n/sbin/ldconfig ${RPM_BUILD_ROOT}/%s/bin\n",DEFAULT_TPP_INSTALL_ROOT);
  fprintf(versionInfo,"%%postun\n/sbin/ldconfig ${RPM_BUILD_ROOT}/%s/bin\n",DEFAULT_TPP_INSTALL_ROOT);
  fprintf(versionInfo,"%%files\n");
  fprintf(versionInfo,"%%defattr(-,root,root)\n");
  fprintf(versionInfo,"%s\n",DEFAULT_TPP_INSTALL_ROOT);
  fprintf(versionInfo,"%%config(noreplace) %s/cgi-bin/residue_modifications.cfg\n",DEFAULT_TPP_INSTALL_ROOT);
  fprintf(versionInfo,"%%doc releasenotes.txt license.txt IPP_FAQ.html\n");
  fprintf(versionInfo,"\n");
  fclose(versionInfo);
  wrote = true;
  }

  // create a script helpful in RPM generation, if you're doing that
  versionInfo = checked_fopen("rpm_tgz.sh","w"); // replace
  if (versionInfo) {
  fprintf(versionInfo,"# THIS IS A GENERATED FILE, DO NOT EDIT OR PLACE UNDER REVISION CONTROL\n");
  sprintf(rpmString,"%s-%d.%d.%d",TPP_PRODUCT_NAME, TPP_MAJOR_RELEASE_NUMBER,TPP_MINOR_RELEASE_NUMBER,TPP_REV_RELEASE_NUMBER);
  fprintf(versionInfo,"cd ..; mkdir %s; cp -r Release/* %s; tar -czf %s.tar.gz %s/*; rm -rf %s;",rpmString,rpmString,rpmString,rpmString,rpmString);  
  fprintf(versionInfo,"sudo cp %s.tar.gz /usr/src/redhat/SOURCES;",rpmString);
  fprintf(versionInfo,"sudo rpmbuild -bb src_insilicos/IPP.spec;");
  fprintf(versionInfo,"sudo rm /usr/src/redhat/SOURCES/%s.tar.gz \n",rpmString);

  fclose(versionInfo);
  wrote = true;
  }
#endif
  if (wrote) {
  char cwd[1024];
  char *res = getcwd(cwd,sizeof(cwd));
  printf("regenerate_VersionInfo setting new version info string in %s\\TPPVersionInfo.cxx: %s\n",res?res:"???",versionString);
  }

  return 0;
}
