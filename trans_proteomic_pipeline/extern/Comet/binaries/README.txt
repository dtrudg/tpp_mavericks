2013/06/05

Comet version "2013.01 rev. 0"

This is a major release of Comet.  A new parameters file is 
required for this version of Comet.  From this release format, 
Comet it will check to make sure the new version string at the 
head of each comet.params file is compatible with the binary.  
Other features of this version include sparse matrix support 
(uses lot less memory which is relevant for high-res ms/ms using 
small fragment_bin_tol values), supporting searching multiple 
input files, internally segmenting searches in batches of 
spectra also for memory considerations, and lots of bug fixes.  
See the release notes at http://comet-ms.sourceforge.net/ for 
more details on this release.


Comet is an open source MS/MS database search engine released 
under the Apache 2.0 license.

Current supported input formats are mzXML, mzML, ms2, and cms2.
Current supported output formats are pepXML, SQT, and .out files.

To run a search on an input file requires the Comet binary, a 
search parameters file (comet.params), an input file, and a 
protein sequence database in FASTA format.  The syntax of a search:

   comet.exe input.mzXML
   comet.exe input.ms2

Search parameters, such as which sequence database to query,
modifications to consider, mass tolerances, output format, etc. 
are defined in the comet.params file.

One can generate a parameters file with the command

   comet.exe -p

Rename the created file "comet.params.new" to "comet.params".

Windows and linux command line binaries are included with this 
release.
