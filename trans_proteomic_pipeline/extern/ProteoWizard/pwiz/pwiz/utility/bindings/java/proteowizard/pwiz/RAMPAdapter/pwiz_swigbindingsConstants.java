/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.39
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package proteowizard.pwiz.RAMPAdapter;

public interface pwiz_swigbindingsConstants {
  public final static int INSTRUMENT_LENGTH = pwiz_swigbindingsJNI.INSTRUMENT_LENGTH_get();
  public final static int SCANTYPE_LENGTH = pwiz_swigbindingsJNI.SCANTYPE_LENGTH_get();
  public final static int CHARGEARRAY_LENGTH = pwiz_swigbindingsJNI.CHARGEARRAY_LENGTH_get();
  public final static int MASK_SCANS_TYPE = pwiz_swigbindingsJNI.MASK_SCANS_TYPE_get();
  public final static int BIT_ORIGIN_SCANS = pwiz_swigbindingsJNI.BIT_ORIGIN_SCANS_get();
  public final static int BIT_AVERAGE_SCANS = pwiz_swigbindingsJNI.BIT_AVERAGE_SCANS_get();
  public final static int OPTION_AVERAGE_SCANS = pwiz_swigbindingsJNI.OPTION_AVERAGE_SCANS_get();
  public final static int OPTION_ORIGIN_SCANS = pwiz_swigbindingsJNI.OPTION_ORIGIN_SCANS_get();
  public final static int OPTION_ALL_SCANS = pwiz_swigbindingsJNI.OPTION_ALL_SCANS_get();
  public final static int DEFAULT_OPTION = pwiz_swigbindingsJNI.DEFAULT_OPTION_get();

}
