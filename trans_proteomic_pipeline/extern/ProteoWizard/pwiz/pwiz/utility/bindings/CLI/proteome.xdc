<?xml version="1.0"?>
<doc>
<members>
<member name="T:pwiz.proteome.Chemistry.MassAbundance" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="51">
struct for holding isotope information
</member>
<member name="D:pwiz.proteome.Chemistry.MassDistribution" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="66">
struct for holding isotope distribution
</member>
<member name="T:pwiz.proteome.Chemistry.Element.Type" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="78">
enumeration of the elements
</member>
<member name="M:pwiz.proteome.Chemistry.Element.Info.record(pwiz.proteome.Chemistry.Element.Type)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="114">
retrieve the record for an element
</member>
<member name="T:pwiz.proteome.Chemistry.Formula" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="129">
class to represent a chemical formula
</member>
<member name="M:pwiz.proteome.Chemistry.Formula.#ctor(std.basic_string&lt;System.SByte!System.Runtime.CompilerServices.IsSignUnspecifiedByte,std.char_traits{System.SByte!System.Runtime.CompilerServices.IsSignUnspecifiedByte},std.allocator&lt;System.SByte!System.Runtime.CompilerServices.IsSignUnspecifiedByte&gt;&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="134">
formula string given by symbol/count pairs, e.g. water: "H2 O1" (whitespace optional)
</member>
<member name="M:pwiz.proteome.Chemistry.Formula.op_Subscript(pwiz.proteome.Chemistry.Element.Type)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="145">
access to the Element's count in the formula
</member>
<member name="M:pwiz.proteome.Chemistry.Formula.op_Equality(pwiz.proteome.Chemistry.Formula!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="158">
formulas are equal iff their elemental compositions are equal
</member>
<member name="M:pwiz.proteome.Chemistry.op_LeftShift(std.basic_ostream&lt;System.SByte!System.Runtime.CompilerServices.IsSignUnspecifiedByte,std.char_traits{System.SByte!System.Runtime.CompilerServices.IsSignUnspecifiedByte}&gt;*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,pwiz.proteome.Chemistry.Formula!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\chemistry.hpp" line="174">
output a Formula
</member>
<member name="T:pwiz.proteome.ModificationParsing" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="37">
settings to enable parsing of inline modifications in peptide sequences
</member>
<member name="T:pwiz.proteome.ModificationDelimiter" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="46">
the delimiter expected to signify an inline modification
</member>
<member name="T:pwiz.proteome.Peptide" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="58">
represents a peptide or polypeptide (a sequence of amino acids)
</member>
<member name="M:pwiz.proteome.Peptide.sequence" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="71">
returns the sequence of amino acids making up the peptide
</member>
<member name="M:pwiz.proteome.Peptide.formula(System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="74">
if modified = false: returns the composition formula of sequence()+water
if modified = true: returns the composition formula of sequence()+modifications()+water
throws an exception if modified = true and any modification has only mass information
</member>
<member name="M:pwiz.proteome.Peptide.monoisotopicMass(System.Int32,System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="79">
if charge = 0: returns neutral mass
if charge &gt; 0: returns charged m/z
if modified = false: returns the monoisotopic mass of sequence()+water
if modified = true: returns the monoisotopic mass of sequence()+modifications()+water
</member>
<member name="M:pwiz.proteome.Peptide.molecularWeight(System.Int32,System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="85">
if charge = 0: returns neutral mass
if charge &gt; 0: returns charged m/z
if modified = false: returns the molecular weight of sequence()+water
if modified = true: returns the molecular weight of sequence()+modifications()+water
</member>
<member name="M:pwiz.proteome.Peptide.modifications" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="91">
the map of sequence offsets (0-based) to modifications;
modifications can be added or removed from the peptide with this map
</member>
<member name="M:pwiz.proteome.Peptide.modifications" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="95">
the map of sequence offsets (0-based) to modifications
</member>
<member name="M:pwiz.proteome.Peptide.fragmentation(System.Boolean,System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="98">
returns a fragmentation model for the peptide;
fragment masses can calculated as mono/avg and as modified/unmodified
</member>
<member name="M:pwiz.proteome.Peptide.op_Equality(pwiz.proteome.Peptide!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="102">
returns true iff peptide sequences and masses are equal
</member>
<member name="D:pwiz.proteome.Protein" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="113">
represents a protein (a long peptide)
</member>
<member name="T:pwiz.proteome.Fragmentation" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\peptide.hpp" line="117">
provides fragment ion masses for a peptide
</member>
<!-- Discarding badly formed XML document comment for member 'M:pwiz.proteome.Fragmentation.a(System.UInt32,System.UInt32)'. -->
<!-- Discarding badly formed XML document comment for member 'M:pwiz.proteome.Fragmentation.b(System.UInt32,System.UInt32)'. -->
<!-- Discarding badly formed XML document comment for member 'M:pwiz.proteome.Fragmentation.c(System.UInt32,System.UInt32)'. -->
<!-- Discarding badly formed XML document comment for member 'M:pwiz.proteome.Fragmentation.x(System.UInt32,System.UInt32)'. -->
<!-- Discarding badly formed XML document comment for member 'M:pwiz.proteome.Fragmentation.y(System.UInt32,System.UInt32)'. -->
<!-- Discarding badly formed XML document comment for member 'M:pwiz.proteome.Fragmentation.z(System.UInt32,System.UInt32)'. -->
<!-- Discarding badly formed XML document comment for member 'M:pwiz.proteome.Fragmentation.zRadical(System.UInt32,System.UInt32)'. -->
<member name="T:pwiz.proteome.Modification" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="39">
represents a post-translational modification (PTM)
modification formula or masses must be provided at instantiation
</member>
<member name="M:pwiz.proteome.Modification.#ctor" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="45">
constructs a zero-mass modification (provided for MSVC compatibility)
</member>
<member name="M:pwiz.proteome.Modification.hasFormula" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="55">
returns true iff the mod was constructed with formula
</member>
<member name="M:pwiz.proteome.Modification.formula" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="58">
returns the difference formula;
throws runtime_error if hasFormula() = false
</member>
<member name="M:pwiz.proteome.Modification.op_Equality(pwiz.proteome.Modification!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="65">
returns true iff delta masses are equal
</member>
<member name="T:pwiz.proteome.ModificationList" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="74">
represents a list of modifications on a single amino acid
</member>
<member name="M:pwiz.proteome.ModificationList.monoisotopicDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="84">
returns the sum of the monoisotopic delta masses of all modifications in the list
</member>
<member name="M:pwiz.proteome.ModificationList.averageDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="87">
returns the sum of the average delta masses of all modifications in the list
</member>
<member name="T:pwiz.proteome.ModificationMap" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="92">
maps peptide/protein sequence indexes (0-based) to a modification list
* ModificationMap::NTerminus() returns the index for specifying N terminal mods
* ModificationMap::CTerminus() returns the index for specifying C terminal mods
</member>
<member name="M:pwiz.proteome.ModificationMap.monoisotopicDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="116">
returns the sum of the monoisotopic delta masses of all modifications in the map
</member>
<member name="M:pwiz.proteome.ModificationMap.averageDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="119">
returns the sum of the average delta masses of all modifications in the map
</member>
<member name="M:pwiz.proteome.ModificationMap.begin" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="122">
Returns an iterator pointing to the first element stored in the map. First is defined by the map's comparison operator, Compare.
</member>
<member name="M:pwiz.proteome.ModificationMap.end" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="125">
Returns an iterator pointing to the last element stored in the map; in other words, to the off-the-end value.
</member>
<member name="M:pwiz.proteome.ModificationMap.rbegin" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="128">
Returns a reverse_iterator pointing to the first element stored in the map. First is defined by the map's comparison operator, Compare.
</member>
<member name="M:pwiz.proteome.ModificationMap.rend" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="131">
Returns a reverse_iterator pointing to the last element stored in the map; in other words, to the off-the-end value).
</member>
<member name="M:pwiz.proteome.ModificationMap.op_Subscript(System.Int32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="134">
If an element with the key x exists in the map, then a reference to its associated value is returned. Otherwise the pair x,T() is inserted into the map and a reference to the default object T() is returned.
</member>
<member name="M:pwiz.proteome.ModificationMap.equal_range(System.Int32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="137">
Returns the pair (lower_bound(x), upper_bound(x)).
</member>
<member name="M:pwiz.proteome.ModificationMap.find(System.Int32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="140">
Searches the map for a pair with the key value x and returns an iterator to that pair if it is found. If such a pair is not found the value end() is returned.
</member>
<member name="M:pwiz.proteome.ModificationMap.lower_bound(System.Int32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="143">
Returns a reference to the first entry with a key greater than or equal to x.
</member>
<member name="M:pwiz.proteome.ModificationMap.upper_bound(System.Int32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="146">
Returns an iterator for the first entry with a key greater than x.
</member>
<member name="M:pwiz.proteome.ModificationMap.clear" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="149">
Erases all elements from the self.
</member>
<member name="M:pwiz.proteome.ModificationMap.erase(std._Tree&lt;std._Tmap_traits&lt;System.Int32,pwiz.proteome.ModificationList,std.less&lt;System.Int32&gt;,std.allocator&lt;std.pair&lt;System.Int32!System.Runtime.CompilerServices.IsConst,pwiz.proteome.ModificationList&gt;&gt;,false&gt;&gt;.iterator)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="152">
Deletes the map element pointed to by the iterator position.
</member>
<member name="M:pwiz.proteome.ModificationMap.erase(std._Tree&lt;std._Tmap_traits&lt;System.Int32,pwiz.proteome.ModificationList,std.less&lt;System.Int32&gt;,std.allocator&lt;std.pair&lt;System.Int32!System.Runtime.CompilerServices.IsConst,pwiz.proteome.ModificationList&gt;&gt;,false&gt;&gt;.iterator,std._Tree&lt;std._Tmap_traits&lt;System.Int32,pwiz.proteome.ModificationList,std.less&lt;System.Int32&gt;,std.allocator&lt;std.pair&lt;System.Int32!System.Runtime.CompilerServices.IsConst,pwiz.proteome.ModificationList&gt;&gt;,false&gt;&gt;.iterator)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="155">
If the iterators start and finish point to the same map and last is reachable from first, all elements in the range [start, finish) are deleted from the map.
</member>
<member name="M:pwiz.proteome.ModificationMap.erase(System.Int32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="158">
Deletes the element with the key value x from the map, if one exists. Returns 1 if x existed in the map, 0 otherwise.
</member>
<member name="M:pwiz.proteome.ModificationMap.insert(std.pair&lt;System.Int32!System.Runtime.CompilerServices.IsConst,pwiz.proteome.ModificationList&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="161">
If a value_type with the same key as x is not present in the map, then x is inserted into the map. Otherwise, the pair is not inserted.
</member>
<member name="M:pwiz.proteome.ModificationMap.insert(std._Tree&lt;std._Tmap_traits&lt;System.Int32,pwiz.proteome.ModificationList,std.less&lt;System.Int32&gt;,std.allocator&lt;std.pair&lt;System.Int32!System.Runtime.CompilerServices.IsConst,pwiz.proteome.ModificationList&gt;&gt;,false&gt;&gt;.iterator,std.pair&lt;System.Int32!System.Runtime.CompilerServices.IsConst,pwiz.proteome.ModificationList&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\proteome\modification.hpp" line="164">
If a value_type with the same key as x is not present in the map, then x is inserted into the map. Otherwise, the pair is not inserted. A position may be supplied as a hint regarding where to do the insertion. If the insertion is done right after position, then it takes amortized constant time. Otherwise it takes O(log N) time.
</member>
<member name="P:pwiz.CLI.proteome.Chemistry.Proton" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="44">
<summary>
the mass of a proton in unified atomic mass units
</summary>
</member>
<member name="P:pwiz.CLI.proteome.Chemistry.Neutron" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="49">
<summary>
the mass of a neutron in unified atomic mass units
</summary>
</member>
<member name="P:pwiz.CLI.proteome.Chemistry.Electron" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="54">
<summary>
the mass of an electron in unified atomic mass units
</summary>
</member>
<member name="T:pwiz.CLI.proteome.ModificationParsing" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="63">
<summary>
settings to enable parsing of inline modifications in peptide sequences
</summary>
</member>
<member name="T:pwiz.CLI.proteome.ModificationDelimiter" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="74">
<summary>
the delimiter expected to signify an inline modification
</summary>
</member>
<member name="T:pwiz.CLI.proteome.Peptide" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="84">
<summary>
represents a peptide (sequence of amino acids)
</summary>
</member>
<member name="P:pwiz.CLI.proteome.Peptide.sequence" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="97">
<summary>
returns the sequence of amino acids using standard single character symbols
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.formula" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="102">
<summary>
returns the unmodified chemical composition of the peptide (sequence()+water)
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.formula(System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="107">
<summary>
returns the (possibly modified) chemical composition of the peptide
<para>- if modified = false: returns the composition formula of sequence()+water</para>
<para>- if modified = true: returns the composition formula of sequence()+modifications()+water</para>
<para>- note: throws an exception if modified = true and any modification has only mass information</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.monoisotopicMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="115">
<summary>
returns the monoisotopic mass of the modified peptide at neutral charge (sequence()+modifications()+water)
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.monoisotopicMass(System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="120">
<summary>
returns the monoisotopic mass of the (possibly modified) peptide at neutral charge
<para>- if modified = false: returns the monoisotopic mass of sequence()+water</para>
<para>- if modified = true: returns the monoisotopic mass of sequence()+modifications()+water</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.monoisotopicMass(System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="127">
<summary>
returns the monoisotopic mass of the modified peptide at charge &lt;charge&gt;
<para>- if charge = 0: returns the monoisotopic mass of the modified peptide at neutral charge (sequence()+modifications()+water)</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.monoisotopicMass(System.Boolean,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="133">
<summary>
<para>if charge = 0: returns neutral mass</para>
<para>if charge &gt; 0: returns charged m/z</para>
<para>if modified = false: returns the monoisotopic mass of sequence()+water</para>
<para>if modified = true: returns the monoisotopic mass of sequence()+modifications()+water</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.molecularWeight" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="141">
<summary>
returns the molecular weight of the modified peptide at neutral charge (sequence()+modifications()+water)
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.molecularWeight(System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="146">
<summary>
returns the molecular weight of the (possibly modified) peptide at neutral charge
<para>- if modified = false: returns the molecular weight of sequence()+water</para>
<para>- if modified = true: returns the molecular weight of sequence()+modifications()+water</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.molecularWeight(System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="153">
<summary>
returns the molecular weight of the modified peptide at charge &lt;charge&gt;
<para>- if charge = 0: returns the molecular weight of the modified peptide at neutral charge (sequence()+modifications()+water)</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.molecularWeight(System.Boolean,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="159">
<summary>
<para>- if charge = 0: returns neutral mass</para>
<para>- if charge &gt; 0: returns charged m/z</para>
<para>- if modified = false: returns the molecular weight of sequence()+water</para>
<para>- if modified = true: returns the molecular weight of sequence()+modifications()+water</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.modifications" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="167">
<summary>
the map of sequence offsets (0-based) to modifications;
<para>- modifications can be added or removed from the peptide with this map</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Peptide.fragmentation(System.Boolean,System.Boolean)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="173">
<summary>
returns a fragmentation model for the peptide;
<para>- fragment masses can calculated as mono/avg and as modified/unmodified</para>
</summary>
</member>
<member name="T:pwiz.CLI.proteome.Fragmentation" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="181">
<summary>
provides fragment ion masses for a peptide
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Fragmentation.a(System.Int32,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="193">
<summary>
returns the a ion of length &lt;length&gt;
<para>- if &lt;charge&gt; = 0: returns neutral mass</para>
<para>- if &lt;charge&gt; &gt; 0: returns charged m/z</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Fragmentation.b(System.Int32,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="200">
<summary>
returns the b ion of length &lt;length&gt;
<para>- if &lt;charge&gt; = 0: returns neutral mass</para>
<para>- if &lt;charge&gt; &gt; 0: returns charged m/z</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Fragmentation.c(System.Int32,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="207">
<summary>
returns the c ion of length &lt;length&gt;
<para>- if &lt;charge&gt; = 0: returns neutral mass</para>
<para>- if &lt;charge&gt; &gt; 0: returns charged m/z</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Fragmentation.x(System.Int32,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="214">
<summary>
returns the x ion of length &lt;length&gt;
<para>- if &lt;charge&gt; = 0: returns neutral mass</para>
<para>- if &lt;charge&gt; &gt; 0: returns charged m/z</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Fragmentation.y(System.Int32,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="221">
<summary>
returns the y ion of length &lt;length&gt;
<para>- if &lt;charge&gt; = 0: returns neutral mass</para>
<para>- if &lt;charge&gt; &gt; 0: returns charged m/z</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Fragmentation.z(System.Int32,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="228">
<summary>
returns the z ion of length &lt;length&gt;
<para>- if &lt;charge&gt; = 0: returns neutral mass</para>
<para>- if &lt;charge&gt; &gt; 0: returns charged m/z</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Fragmentation.zRadical(System.Int32,System.Int32)" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="235">
<summary>
returns the z radical ion of length &lt;length&gt;
<para>- if &lt;charge&gt; = 0: returns neutral mass</para>
<para>- if &lt;charge&gt; &gt; 0: returns charged m/z</para>
</summary>
</member>
<member name="T:pwiz.CLI.proteome.Modification" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="244">
<summary>
represents a post-translational modification (PTM)
<para>- note: modification formula or masses must be provided at instantiation</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Modification.hasFormula" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="257">
<summary>
returns true iff the mod was constructed with formula
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Modification.formula" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="262">
<summary>
returns the difference formula;
<para>- note: throws runtime_error if hasFormula() = false</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Modification.monoisotopicDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="268">
<summary>
returns the monoisotopic delta mass of the modification
</summary>
</member>
<member name="M:pwiz.CLI.proteome.Modification.averageDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="273">
<summary>
returns the average delta mass of the modification
</summary>
</member>
<member name="T:pwiz.CLI.proteome.ModificationList" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="282">
represents a list of modifications on a single amino acid
</member>
<member name="M:pwiz.CLI.proteome.ModificationList.monoisotopicDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="291">
<summary>
returns the sum of the monoisotopic delta masses of all modifications in the list
</summary>
</member>
<member name="M:pwiz.CLI.proteome.ModificationList.averageDeltaMass" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="296">
<summary>
returns the sum of the average delta masses of all modifications in the list
</summary>
</member>
<member name="T:pwiz.CLI.proteome.ModificationMap" decl="false" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="306">
<summary>
maps peptide/protein sequence indexes (0-based) to a modification list
<para>- ModificationMap.NTerminus() returns the index for specifying N terminal mods</para>
<para>- ModificationMap.CTerminus() returns the index for specifying C terminal mods</para>
</summary>
</member>
<member name="M:pwiz.CLI.proteome.ModificationMap.NTerminus" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="318">
<summary>
returns the index for specifying N terminal mods
</summary>
</member>
<member name="M:pwiz.CLI.proteome.ModificationMap.CTerminus" decl="true" source="c:\proteowizard\pwiz\pwiz\utility\bindings\cli\proteome.hpp" line="323">
<summary>
returns the index for specifying C terminal mods
</summary>
</member>
</members>
</doc>
