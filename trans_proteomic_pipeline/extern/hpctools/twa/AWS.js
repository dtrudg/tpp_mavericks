/**
* Program: TPP Web Launcher for Amazon
* Author:  Joe Slagel
*
* Copyright (C) 2010-2013 by Joseph Slagel
* 
* This library is free software; you can redistribute it and/or             
* modify it under the terms of the GNU Lesser General Public                
* License as published by the Free Software Foundation; either              
* version 2.1 of the License, or (at your option) any later version.        
*                                                                           
* This library is distributed in the hope that it will be useful,           
* but WITHOUT ANY WARRANTY; without even the implied warranty of            
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         
* General Public License for more details.                                  
*                                                                           
* You should have received a copy of the GNU Lesser General Public          
* License along with this library; if not, write to the Free Software       
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
* 
* Institute for Systems Biology
* 401 Terry Avenue North
* Seattle, WA  98109  USA
* jslagel@systemsbiology.org
*
* $Id: $
*/

/**
 * @constructor
 * 
 * Crude class for interacting with Amazon Web Services.
 */
function AWS() {

   // AWS keys
   this.accessKey = null;
   this.secretKey = null;
   
   // EC2 parameters
   this.instanceType = 'm1.large'; 	// EC2 instance type
   this.zone         = null;		// EC2 availability zone
   this.keyName      = null;		// EC2 Key to use
   this.shutdown     = 1;               // Autoshutdown in (n) hours
   
   // S3 parameters
//   this.bucket        = null;		// S3 bucket to use for storage
   this.s3Url         = null;		// S3 URL to sync instance with
   this.s3SyncOnStart = true;		// Sync with S3 on startup
   this.s3SyncOnStop  = true;		// Sync with S3 on shutdown
   
   // EBS parameters
   this.ebsAttach     = false;		// Attach EBS block
   this.ebsSize       = 100;		// Initial size of EBS block (GiB)
   
   // Instance info
   this.imageId       = null;
   this.instanceId    = null;
   this.instanceState = null;
   this.instanceUrl   = null;
   this.instanceDNS   = null;
   this.uuid          = '';			// Unique session identifier

// Uncomment the following line in order to generate a uuid for each browser
// session so users can share AWS accounts and get separate instances.
// this.uuid = getUUID();
    
   // AWS API
   this.apiVersion = '2011-02-28';
   this.amiOwner   = '178066177892';			// Owner of TPP AMI's
   this.region     = 'us-west-2';               // Default EC2 region
   this.server     = 'http://regis-dev/twa';	// Proxy server for requests
   this.endpoint   = 'ec2.' + this.region + '.amazonaws.com';
   this.endpoint   = this.server + '/aws/' + this.endpoint;
   this.securityGroup = 'TPP-TWA';              // EC2 security group to use
   
   this.amiList     = [];                       // List of TPP TWA AMIs
   this.keyPairList = [];                       // List of EC2 key pairs
}

/**
 * Initialize using the credentials
 */
AWS.prototype.signin = function( callback ) {
    console.debug( 'AWS signing in' );
    
    this.signinCallback = callback;
    
    // Start a chain of asynchronous requests to sign us in
    this._checkCredentials();
}
 
/**
 * Check the credentials
 */
AWS.prototype._checkCredentials = function() {
    console.debug( 'AWS checking credentials' ); 
    
    if ( !this.accessKey && !this.secretKey )
       throw 'Missing needed credentials for signing in';
       
    var params = {};
    var url    = this.generateSignedURL( 'DescribeRegions', params );
    this.getUrl( url, bind ( this, function( xmlhttp, status, msg ) { 
       console.debug( 'AWS check credentials status = ' + status );
       if ( status != 200 )
          return this.signinCallback( 'Problem with your AWS credentials', msg );
       this._initSecurityGroup();
    } ) );
}

/**
 * Initialize our security group 
 */
AWS.prototype._initSecurityGroup = function() {
    console.debug( 'AWS checking for security group' ); 
    
    var params = { 'Filter.1.Name' : 'group-name',
                   'Filter.1.Value': this.securityGroup,
                 };
    var url    = this.generateSignedURL( 'DescribeSecurityGroups', params );
    this.getUrl( url, bind ( this, function( xmlhttp, status, msg ) { 
       console.debug('AWS init security group status = ' + status );
       if ( status != 200 )
          return this.signinCallback( 'Error creating TPP-TWA security group', msg );
          
       if ( !xmlhttp.getElementsByTagName('groupName')[0] ) 
          {
          this._createSecurityGroup();
          return;
          }
          
       console.debug( 'AWS TPP-TWA security group already exists' );
       this._getAMIs();
    } ) );
}

/**
 * Create the TPP-TWA security group
 */
AWS.prototype._createSecurityGroup = function() {
    console.debug( 'AWS creating TPP-TWA security group' ); 

    var params = { GroupName        : this.securityGroup,
                   GroupDescription : 'Security group used by the TPP web launcher for Amazon.'
                 };
    var url = this.generateSignedURL( 'CreateSecurityGroup', params );
    this.getUrl( url, bind( this, function( xmlhttp, status, msg ) { 
       console.debug( 'TWA create security group status = ' + status ); 
       if ( status != 200 )
          return this.signinCallback( 'Error creating TPP-TWA security group', msg );
       this._enableIngress();
    } ) );
}

/**
 * Enable ingress on the TPP-TWA security group
 */
AWS.prototype._enableIngress = function() {
    console.debug( 'enabling ingress on TPP-TWA security group' ); 

    var params = { 'GroupName'                  : 'TPP-TWA',
                   'IpPermissions.1.IpProtocol' : 'tcp',
                   'IpPermissions.1.FromPort'   : '80',
                   'IpPermissions.1.ToPort'     : '80',
                   'IpPermissions.1.IpRanges.1.CidrIp' : '0.0.0.0/0',
                   'IpPermissions.2.IpProtocol' : 'tcp',
                   'IpPermissions.2.FromPort'   : '22',
                   'IpPermissions.2.ToPort'     : '22',
                   'IpPermissions.2.IpRanges.1.CidrIp' : '0.0.0.0/0'
                 };
    var url = this.generateSignedURL( 'AuthorizeSecurityGroupIngress', params );
    this.getUrl( url, bind( this, function( xmlhttp, status, msg ) { 
       console.debug( 'TPP-TWA security group engress status = ' + status ); 
       if ( status != 200 )
          return this.signinCallback( 'Error enabling TPP-TWA security group', msg );
       this._getAMIs();
    } ) );
}

/**
 *  Lookup list of TWA AMIs 
 */
AWS.prototype._getAMIs = function() {

    console.debug( 'AWS getting list of TPP-TWA ami images' );
        
    var params = { 'Owner.1'          : this.amiOwner,
                   'Filter.1.Name'    : 'state',
                   'Filter.1.Value.1' : 'available'
                 };
    var url = this.generateSignedURL( 'DescribeImages', params );
    this.getUrl( url, bind( this, this._getAMIsResponse ) );
}

AWS.prototype._getAMIsResponse = function( xmlhttp, status, msg ) {
    console.debug( 'TPP-TWA get ami status = ' + status ); 
    if ( status != 200 )
        return this.signinCallback( 'Error getting TWA Amazon Machine Images', msg );
          
    // Iterate through AMIs
    this.amiList = [];
    var items = xmlhttp.getElementsByTagName('item'); 
    for ( var i = 0; i < items.length; i++ ) {
        if ( !items[i].getElementsByTagName('imageId')[0] )
            continue; // images tagged have additional <item> elements, ignore

        id   = items[i].getElementsByTagName('imageId')[0].childNodes[0].nodeValue;
        desc = items[i].getElementsByTagName('description')[0].childNodes[0].nodeValue;
        console.debug( 'TPP-TWA found ami id: ' + id + ' desc: ' + desc );

        if ( desc.indexOf('TPP v4.6') !== -1 ) // FIXME
           this.amiList.push( { id: id, desc: desc } );
        if ( desc.indexOf('TPP v0.0') !== -1 ) // FIXME
           this.amiList.push( { id: id, desc: desc } );
    }
      
    this._getKeyPairs();
}

/**
 *  Lookup list of EC2 Keys
 */
AWS.prototype._getKeyPairs = function() {

    console.debug( 'AWS getting list of key pairs' );
        
    var params = {};
    var url = this.generateSignedURL( 'DescribeKeyPairs', params );
    this.getUrl( url, bind( this, this._getKeyPairsResponse ) );
}

AWS.prototype._getKeyPairsResponse = function( xmlhttp, status, msg ) {
    console.debug( 'TPP-TWA get key pairs status = ' + status ); 
    if ( status != 200 )
        return this.signinCallback( 'Error getting EC2 key pairs', msg );
          
    // Iterate through key pairs
    this.keyPairList = [];
    var items = xmlhttp.getElementsByTagName('item'); 
    for ( var i = 0; i < items.length; i++ ) {
        name  = items[i].getElementsByTagName('keyName')[0].childNodes[0].nodeValue;
        console.debug( 'TPP-TWA found keypair ' + name );
        this.keyPairList[i] = { name: name };
    }
    this._getInstanceId();
}

/**
 * Get any TWA instance
 */
AWS.prototype._getInstanceId = function() {
    console.debug( 'AWS checking for a live instance' );
	
    this.instanceId = null;

    var tag = 'TPP-TWA';
    if ( this.uuid )
        tag += '-' + this.uuid;

    var params = { 
       'Filter.1.Name'    : 'tag:' + tag,
       'Filter.1.Value.1' : aws.accessKey,
       'Filter.2.Name'    : 'instance-state-code',
       'Filter.2.Value.1' : 16,					// active
       'Filter.2.Value.2' : 0,					// pending
       'Filter.2.Value.3' : 32 					// shutting down
       };
    var url  = this.generateSignedURL( 'DescribeInstances', params );
//    this.getUrl( url, bind( this, this._stateResponse )  );
    this.getUrl( url, bind( this, function ( resp, status, msg ) {
       if ( status != 200 )
          return this.signinCallback( 'Error checking for EC2 instance', msg );
       this.instanceId = elemText( resp, "instanceId" );
       this.signinCallback();
    } ) );
       /**
    this.getUrl( url, bind( this, function ( resp, status, msg ) {
       try { this.instanceId = resp.getElementsByTagName("instanceId")[0].textContent; } catch (e) {}
       console.debug( "instanceId: " + this.instanceId );
       if ( status == 200 && this.instanceId )
    	  this.getState();
       } ) );
       */
}

/**
 * @event stateUpdated
 * Fires when the state of the instance is updated
 */
AWS.prototype.stateUpdated = function() { console.debug( 'state updated ' + this ) }


/**
 * Start a new instance
 */
AWS.prototype.startInstance = function( imageId )
{
    if ( this.instanceId && this.instanceState != 16  )
        throw "Attempt to start a 2nd instance";

    console.debug( 'startInstance()' );
    
    this.instanceId    = null;
    this.instanceState = null;
    this.instanceUrl   = null;
    this.instanceDNS   = null;
    
    // Build bash script to run on bootup of instance
    var bootstrap = "#!/bin/sh\n";
    bootstrap += "chmod a+rwxt /tmp; cd /tmp\n";

    // ...missed this in image
//    bootstrap += "perl -pi -e 's#WEBSERVER_ROOT /mnt/tppdata#WEBSERVER_ROOT /mnt#' /etc/apache2/sites-available/default\n";
//    bootstrap += "perl -pi -e 's#Alias /tpp #Alias /tppdata #' /etc/apache2/sites-available/default\n";
//    bootstrap += "sed -i '/Alias \\/tppdata/i Alias /ISB /opt/tpp' /etc/apache2/sites-available/default\n";
//    bootstrap += "/etc/init.d/apache2 restart\n";
    bootstrap += "sed -i \"/dummy/i 'www_root' => '/mnt/tppdata',\" /opt/tpp/cgi-bin/tpp_gui_config.pl\n";
   
    //    bootstrap += "mkdir -p /usr/local/tpp/ISB/data\n";
    //    bootstrap += "chmod a+rwx /usr/local/tpp/ISB/data\n";
    // ...fix apache conf
    //    bootstrap += "echo SetEnv WEBSERVER_ROOT /usr/local/tpp >> /etc/httpd/conf/httpd.conf\n";
    //    bootstrap += 'perl -pi -e "s/var\\/www\\/ISB/usr\\/local\\/tpp\\/ISB/" /etc/httpd/conf/httpd.conf' + "\n";
	
    // ...fix perl 
    // bootstrap += "mkdir -p /hpc/bin\n";
    // bootstrap += "ln -s /usr/bin/perl /hpc/bin/perl\n";	
    
    // ...fix tandem 
    // bootstrap += "ln -s /opt/tpp/bin/tandem.exe /opt/tpp/bin/tandem\n";
    
    // ...add some data
    bootstrap += "cd /mnt/tppdata/local\n";
    bootstrap += "wget -q http://s3.amazonaws.com/spctools/demo_template.tgz\n";
    bootstrap += "tar xvzf demo_template.tgz\n";
    bootstrap += "rm demo_template.tgz\n";
    bootstrap += "mv demo_template demo\n";
    bootstrap += "chown -R www-data:www-data demo\n";

    // bootstrap += "wget http://tools.proteomecenter.org/twa/demo.tgz\n";
    // bootstrap += "tar xvzf demo.tgz\n";
    // bootstrap += "rm -f demo.tgz\n";
    // bootstrap += "chmod a+rw *\n";
	
    // bootstrap += "mkdir -p /opt/tpp/tmp\n";
    // bootstrap += "chmod a+rwxt /opt/tpp/tmp\n";
	
    // ...setup shutdown period
    if ( this.shutdown )
       bootstrap += 'start deadman TIMEOUT=+' + ((this.shutdown * 60) - 1) + "\n";

    // ...write s3cfg file for s3cmd upstart scripts
    if ( this.s3Url )
       {
       bootstrap += "\n";
       bootstrap += "echo '# TWA autogenerated'                  > /opt/tpp/users/.s3cfg\n"; 
       bootstrap += "echo '[default]'                           >> /opt/tpp/users/.s3cfg\n"; 
       bootstrap += "echo 'access_key = " + aws.accessKey + "'  >> /opt/tpp/users/.s3cfg\n"; 
       bootstrap += "echo 'bucket_location = us-west-2'         >> /opt/tpp/users/.s3cfg\n"; 
       bootstrap += "echo 'secret_key = " + aws.secretKey + "'  >> /opt/tpp/users/.s3cfg\n"; 
       bootstrap += "echo 'use_https = True'                    >> /opt/tpp/users/.s3cfg\n";
       bootstrap += "echo 'tpp_s3url = s3://" + aws.s3Url + "'  >> /opt/tpp/users/.s3cfg\n";
       bootstrap += "chown www-data:www-data /opt/tpp/users/.s3cfg\n";
       
       // ...now generate amztpp/petunia awssecret file 
       //bootstrap += "echo '" + aws.accessKey + "' >  /opt/tpp/users/guest/.awssecret";
       //bootstrap += "echo '" + aws.secretKey + "' >> /opt/tpp/users/guest/.awssecret";
       //bootstrap += "echo '" + aws.region    + "' >> /opt/tpp/users/guest/.awssecret";
       //bootstrap += "echo '" + aws.bucket    + "' >> /opt/tpp/users/guest/.awssecret";
       //bootstrap += "chmod 600 /var/www/.awssecret\n";
       //bootstrap += "chown www-data:www-data /opt/tpp/users/.awssecret\n";
       
       // .. add our special parameter
//       for ( var i = 0; i < this.s3Urls.length; i++ ) 
//    	  {
//          bootstrap += "echo '" + this.s3Urls[i] + "' >> /var/www/.awssecret";
//    	  }
       

//       bootstrap += 'sudo -u www-data ';
//       bootstrap += 's3fs ' + this.s3Bucket + ' /mnt/tppdata/s3 ';
//       bootstrap += '-o use_cache=/var/cache/s3fs';
       }
    
    // ...enable S3 sync on start
    if ( this.s3SyncOnStart && this.s3Url )
        {
// FIX: get the bootstrap scripts
bootstrap += "cd /etc/init\n";
bootstrap += "s3cmd --force -c /opt/tpp/users/.s3cfg get s3://spctools/tpp-s3-get.conf\n";

        bootstrap += "rm -f /etc/init/tpp-s3-get.override\n";
// FIX: upstart doesn't run?
//bootstrap += "start tpp-s3-get\n";      
        }
    
    // ...enable S3 sync on shutdown
    if ( this.s3SyncOnStop && this.s3Url )
       {
//  Code for using upstart (doesn't work)
//bootstrap += "cd /etc/init\n";
//bootstrap += "s3cmd --force -c /opt/tpp/users/.s3cfg get s3://spctools/tpp-s3-put.conf\n";
//     bootstrap += "rm -f /etc/init/tpp-s3-put.override\n";
        
// FIX: get the bootstrap scripts
bootstrap += "cd /etc/init.d\n";
bootstrap += "s3cmd --force -c /opt/tpp/users/.s3cfg get s3://spctools/tpp-s3-put\n";
bootstrap += "chmod a+x tpp-s3-put\n";

       bootstrap += "update-rc.d tpp-s3-put stop 01 0 1 .\n";
       }
    
    // Generate request
    if ( imageId )
       this.imageId = imageId;
    bootstrap = rstr2b64( bootstrap );	
    var params = { ImageId  : this.imageId,
                   MinCount : 1,
                   MaxCount : 1,
            	   'SecurityGroup.1' : this.securityGroup,
                   InstanceType      : this.instanceType,
                   UserData          : bootstrap
    };
    if ( this.keyName )
       params.KeyName = this.keyName;
    
    var url  = this.generateSignedURL( 'RunInstances', params );
    this.getUrl( url, bind( this, function( resp, status, msg ) {
	   console.debug( "_startResponse()" );
	   this.start = true;
           this._stateResponse( resp, status, msg );
	   this.start = false;
    } ) );
} 

/**
 * Create the TPP-TWA tag for identifying instances
 */
AWS.prototype._createTag = function() {
    var tag = 'TPP-TWA';
    if ( this.uuid )
        tag += '-' + this.uuid
    var params = { 'ResourceId.1' : aws.instanceId,
                   'Tag.1.Key'    : tag,
                   'Tag.1.Value'  : aws.accessKey
                 };
    
    var url = this.generateSignedURL( 'CreateTags', params );
    this.getUrl( url, function(status, headers, result) { console.debug( "tagged instance: " + status );} );
}

/**
 * Asynchronous request to update the state of the instance
 */
AWS.prototype.getState = function() {
    console.debug( "getState() id: " + this.instanceId + ' state: ' + this.instanceState );
    if ( !this.instanceId )
       {
       this.instanceState = -1;
       this.error =  "Attempt to get state of non-instance";
       return;
       }

    var params = { InstanceId : this.instanceId };
    var url = this.generateSignedURL( 'DescribeInstances', params );
    this.getUrl( url, bind( this, this._stateResponse )  );
}

AWS.prototype._stateResponse = function( resp, status, msg ) {

    console.debug( '_stateResponse() status = ' + status );
    
    if ( status != 200 )
        return this.stateUpdated( resp, status, msg );
    
    this.instanceId    = null;
    this.instanceState = null;
    this.instanceUrl   = null;
    this.instanceDNS   = null;

    this.instanceId = elemText( resp, "instanceId" );
    console.debug( "_stateResponse() instanceId: " + this.instanceId );
    if ( this.start && !this.instanceId )
    	{
        console.error( "_stateResponse() no instance Id received from AWS" );
        this.instanceState = null;
        msg = "No instance Id for started instance.  The instance may/may not have started.";
        msg += "Please use the EC2 console to check for any running instances.";
        return this.stateUpdated( resp, status, msg );
    	}
    
    this.instanceState = elemText( resp, "code" );
    console.debug( "_stateResponse() instanceState: " + this.instanceState );
    if ( this.start && this.instanceState == null )
    	{
        console.error( "no instance State received from AWS" );
        msg = "No instance state for started instance.  The instance may/may not have started.";
        msg += "Please use the EC2 console to check for any running instances.";
        return this.stateUpdated( resp, status, msg );
    	} 
    
    var dns = elemText( resp, "dnsName" );
    if ( dns && this.instanceState == 16 ) {
       this.instanceDNS = dns;
       this.instanceUrl = "http://" + dns + '/tpp/cgi-bin/tpp_gui.pl';
    }
    console.debug( "_stateResponse() url: " + this.instanceUrl );
    
    if ( this.start )
       this._createTag();
    
    return this.stateUpdated( resp, status, msg );
}

/**
 * Stop the instance
 */
AWS.prototype.stopInstance = function() {
    if ( !this.instanceId ) {
       console.error( "No instance detected to stop" );
       this.instanceState = null;
       return this.stateUpdated( null, -1, "No instance to stop" );
    }
    
    var params = { InstanceId : this.instanceId };
    var url = this.generateSignedURL( 'TerminateInstances', params );
    this.getUrl( url, bind( this, this._stateResponse )  );
}


//
// Check to see if Petunia web interface is available
//
AWS.prototype.checkPetunia = function( retries, callback ) {
    if ( !aws.instanceDNS || !aws.instanceId ) {
       console.error( "checkPetunia() no instance found" );
       callback( null, "no EC2 instance found" );
    }

    // make a proxy request to the Apache server on the EC2 instance
    var u = this.server + '/tpp/' + this.instanceDNS;
    console.debug( 'checkPetunia() retries: ' + retries );
    this.getUrl( u, bind ( this, function( xmlhttp, status, msg ) { 
       if ( status != 200 && retries ) {
          setTimeout( function() {  aws.checkPetunia(--retries, callback ); }, 1750 );
       } else {
          callback( status, msg );
       }
    } ) );
}

/**                                                                           */
/********************   Utility Functions   ***********************************/
/**                                                                           */

// only used for sharing accounts -- should be disabled by default
function getUUID()
{
   var uuid = Ext.util.Cookies.get( 'TPP-TWA-uuid' );
   if ( uuid )
      {
      console.debug( 'uuid found in cookie: ' + uuid );
      return uuid;
      }

   uuid = Math.uuid(15);
   console.debug( 'uuid created: ' + uuid );
   Ext.util.Cookies.set( 'TPP-TWA-uuid', uuid, 
      new Date(new Date().getTime() + (1000*60*60*24*7) ) );
   return uuid;
}

// Fix standard Date function
Date.prototype.toISODate =
        new Function("with (this)\n    return " +
           "getFullYear()+'-'+addZero(getMonth()+1)+'-'" +
           "+addZero(getDate())+'T'+addZero(getHours())+':'" +
           "+addZero(getMinutes())+':'+addZero(getSeconds())+'.000Z'");

function generateV1Signature(url, key) {
        var stringToSign = getStringToSign(url);
        var signed =   b64_hmac_sha1(key, stringToSign);
        return signed;
}

function addZero(n) {
    return ( n < 0 || n > 9 ? "" : "0" ) + n;
}

function getNowTimeStamp() {
    var time = new Date();
    var gmtTime = new Date(time.getTime() + (time.getTimezoneOffset() * 60000));
    return gmtTime.toISODate() ;
}

function ignoreCaseSort(a, b) {
    var ret = 0;
    a = a.toLowerCase();
    b = b.toLowerCase();
    if(a > b) ret = 1;
    if(a < b) ret = -1;
    return ret;
}

function getStringToSign(url) {

    var stringToSign = "";
    var query = url.split("?")[1];

    var params = query.split("&");
    params.sort(ignoreCaseSort);
    for (var i = 0; i < params.length; i++) {
        var param = params[i].split("=");
        var name =   param[0];
        var value =  param[1];
        if (name == 'Signature' || undefined  == value) continue;
            stringToSign += name;
            stringToSign += decodeURIComponent(value);
         }

    return stringToSign;
}

/**
 * Sign AWS url request
 */
AWS.prototype.generateSignedURL = function( actionName, params )  {
	
   var url = this.endpoint + "?SignatureVersion=1"
           + "&Action=" + actionName 
           + "&Version=" + encodeURIComponent( this.apiVersion );
           
   console.log( "generateSignedURL() url: " + url );
   
   // Add params
   for ( prop in params )
      {
      if ( !params.hasOwnProperty( prop ) ) continue;
      
      console.log( " param: " + prop + " value: " + params[prop] );
      url += "&" + prop + "=" + encodeURIComponent( params[prop] ) 
      }
    
   // Add access components
   var timestamp = getNowTimeStamp();
   url += "&Timestamp=" + encodeURIComponent( timestamp );
   url += "&AWSAccessKeyId=" + encodeURIComponent( this.accessKey );
            
   // Sign url
console.debug( '   url: ' + url );
   var signature = generateV1Signature( url, this.secretKey );
   url += "&Signature=" + encodeURIComponent( signature ); 
   
//   console.log( "final: " + url );
   return url;
}

//
// Adapted from http://jibbering.com/2002/4/httprequest.html
//
function getXmlHttp() {
	
  var xmlhttp;
  
  try {
     xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
     try {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
     } catch (E) {
        xmlhttp = false;
     }
  }
  
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  
  return xmlhttp;
}

AWS.prototype.getUrl = function ( url, callback )  {
   console.debug( "getUrl() url  = " + url );
   console.debug( "getUrl() this = ", this );
   
   var xmlhttp = getXmlHttp();
   xmlhttp.open( "GET", url, true );
   xmlhttp.onreadystatechange = function()  {
      console.debug( "getUrl() readyState: " + xmlhttp.readyState );
      if ( xmlhttp.readyState != 4 ) 
         return;
      
      console.debug( "getUrl() status = " + xmlhttp.status );
      var result;
      var regerr = /<Errors>/m;
      if ( xmlhttp.status == 200 && xmlhttp.responseText == 'ERROR: invalid url' ) {
         console.debug( 'getUrl() cross-site proxy failed' ); 
         callback( xmlhttp.responseText, -100 );
         return;
     // TODO: Fix proxy code/xml issue for now brute force regex
//     else if ( xmlhttp.status == 200 && xmlhttp.responseText.match(regerr) )
      } else if ( xmlhttp.responseText.match(regerr) ) {
         console.debug( 'getUrl() AWS error response received ' );
         var xmlDoc;
         if (window.DOMParser) {
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(xmlhttp.responseText,"text/xml");
            try { error = xmlDoc.getElementsByTagName("Message")[0].textContent } catch (e) {}
         } else  { // Internet Explorer
            console.debug( 'getUrl() other error' );
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlhttp.responseText);
            try { error = xmlDoc.getElementsByTagName("Message")[0].text } catch (e) {}
        } 
        console.debug( "getUrl() error = " + error );
        if ( !error )
           error = xmlhttp.responseText;
        callback( xmlhttp, xmlhttp.status, error );
        return;
     } else if ( xmlhttp.status != 200 ) {
        error = "Network Error: (" + xmlhttp.status + ") " + xmlhttp.statusText;
        console.debug( "getUrl() error = " + error );
        callback( xmlhttp, xmlhttp.status, error );
        return;
     }
     
      // FIX: if you start a instance, and terminate it outside of twa then
      //      start another one then you get a exception 'attempt to start 2nd'
     try { result = xmlhttp.responseXML.getElementsByTagName("Message")[0].textContent } catch (e) {}
     if ( result ) {
        callback( result, -200 );
        return;
     }
     
     callback( xmlhttp.responseXML, xmlhttp.status );
   }
   xmlhttp.send( null );
}

// Bind scope of "this" so its accessible in a callback
function bind(scope, fn) { return function () { fn.apply(scope, arguments); }; }

// Replace above with this?  Allows for a cleaner bind syntax
//
//Function.prototype.bind = function(scope) {
//  var _function = this;
  
//  return function() {
//    return _function.apply(scope, arguments);
//  }
//}

// Cross browser XML DOM lookup
function elemText( xml, tag ) {
   var txt = null;
   try { 
      var e = xml.getElementsByTagName(tag)[0]
      if ( !e )
         return null;
      if ( 'textContent' in e )
         txt = e.textContent; 
      else
         txt = e.text; 
   } catch (e) {};
   return txt;
}

/*!
Math.uuid.js (v1.4)
http://www.broofa.com
mailto:robert@broofa.com

Copyright (c) 2010 Robert Kieffer
Dual licensed under the MIT and GPL licenses.
*/

/*
 * Generate a random uuid.
 *
 * USAGE: Math.uuid(length, radix)
 *   length - the desired number of characters
 *   radix  - the number of allowable values for each character.
 *
 * EXAMPLES:
 *   // No arguments  - returns RFC4122, version 4 ID
 *   >>> Math.uuid()
 *   "92329D39-6F5C-4520-ABFC-AAB64544E172"
 *
 *   // One argument - returns ID of the specified length
 *   >>> Math.uuid(15)     // 15 character ID (default base=62)
 *   "VcydxgltxrVZSTV"
 *
 *   // Two arguments - returns ID of the specified length, and radix. (Radix must be <= 62)
 *   >>> Math.uuid(8, 2)  // 8 character ID (base=2)
 *   "01001010"
 *   >>> Math.uuid(8, 10) // 8 character ID (base=10)
 *   "47473046"
 *   >>> Math.uuid(8, 16) // 8 character ID (base=16)
 *   "098F4D35"
 */
(function() {
  // Private array of chars to use
  var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');

  Math.uuid = function (len, radix) {
    var chars = CHARS, uuid = [], i;
    radix = radix || chars.length;

    if (len) {
      // Compact form
      for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
    } else {
      // rfc4122, version 4 form
      var r;

      // rfc4122 requires these characters
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
      uuid[14] = '4';

      // Fill in random data.  At i==19 set the high bits of clock sequence as
      // per rfc4122, sec. 4.1.5
      for (i = 0; i < 36; i++) {
        if (!uuid[i]) {
          r = 0 | Math.random()*16;
          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
        }
      }
    }

    return uuid.join('');
  };

  // A more performant, but slightly bulkier, RFC4122v4 solution.  We boost performance
  // by minimizing calls to random()
  Math.uuidFast = function() {
    var chars = CHARS, uuid = new Array(36), rnd=0, r;
    for (var i = 0; i < 36; i++) {
      if (i==8 || i==13 ||  i==18 || i==23) {
        uuid[i] = '-';
      } else if (i==14) {
        uuid[i] = '4';
      } else {
        if (rnd <= 0x02) rnd = 0x2000000 + (Math.random()*0x1000000)|0;
        r = rnd & 0xf;
        rnd = rnd >> 4;
        uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
      }
    }
    return uuid.join('');
  };

  // A more compact, but less performant, RFC4122v4 solution:
  Math.uuidCompact = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  };
})();

/**
 * Disable browser's same origin security policy to allow access to Amazon's Web
 * Services.
 *
 * NOTE: This doesn't work as you need to do this within the scope of the same
 *       origin "violation".
 */
AWS.prototype.enableAccess = function() {
   try 
      {
      netscape.security.PrivilegeManager.enablePrivilege( "UniversalBrowserRead" );
      console.log( "User allowed cross site privileges" );
      }
   catch( err )
      {
      console.warn( "User choose not to allow cross site privileges" );
      console.warn( "getUrl: script does not have cross site privileges" );
      this.instanceState = -1;
      this.error = err.toString();
      }
}
