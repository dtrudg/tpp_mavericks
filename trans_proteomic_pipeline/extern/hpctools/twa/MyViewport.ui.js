/**
* Program: TPP Web Launcher for Amazon
* Author:  Joe Slagel
*
* Copyright (C) 2010-2013 by Joseph Slagel
* 
* This library is free software; you can redistribute it and/or             
* modify it under the terms of the GNU Lesser General Public                
* License as published by the Free Software Foundation; either              
* version 2.1 of the License, or (at your option) any later version.        
*                                                                           
* This library is distributed in the hope that it will be useful,           
* but WITHOUT ANY WARRANTY; without even the implied warranty of            
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         
* General Public License for more details.                                  
*                                                                           
* You should have received a copy of the GNU Lesser General Public          
* License along with this library; if not, write to the Free Software       
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
* 
* Institute for Systems Biology
* 401 Terry Avenue North
* Seattle, WA  98109  USA
* jslagel@systemsbiology.org
*
* $Id: $
*/

var version = 'v1.1';

// Note: Amazon SDK doesn't provide an API for returning the possible instance
// types to use.  Just use: 
//    http://aws.amazon.com/amazon-linux-ami/instance-type-matrix/
var instanceTypes = [ 'm1.small',  'm1.medium',  'm1.large',    'm1.xlarge', 
                      'm2.xlarge', 'm2.2xlarge', 'm2.4xlarge', 
                      'c1.medium', 'c1.xlarge',  'hi1.4xlarge', 'hs1.8large' ];

//
// FIX: nice to load zones dynamically
//
var zones = [ 'No Preference', 
              'us-west-2a',
              'us-west-2b',
              'us-west-2c' ];

Ext.define( 'ami', { extend: 'Ext.data.Model', 
	                 fields: [ { name: 'id',   type: 'string'}, 
	                           { name: 'desc', type: 'string' } ], 
                     proxy: { type: 'localstorage', id: 'id' }
                   } );
                   
Ext.define( 'keypair', { extend: 'Ext.data.Model', 
	                 fields: [ { name: 'id', type: 'string'} 
	                         ], 
                     proxy: { type: 'localstorage', id: 'id' }
                   } );


MyViewportUi = Ext.extend(Ext.Viewport, {
   initComponent: function() {
      aws.stateUpdated = MyViewportUi.stateUpdated;
      
      var key  = { akey: '', skey: '' };
      var ckey = Ext.util.Cookies.get( 'TPP-TWA-session' );
      if ( ckey )
         key = Ext.JSON.decode( ckey );

      this.items = [
         {
            xtype: 'toolbar',
            items : [
                     {
                        xtype: 'label',
                        html: '<a href="" noborder><img src="images/sm_tpp_logo.png" height=32></a>'
                     },
                     { xtype: 'tbspacer' },
                     { xtype: 'label', text : 'TPP Web Launcher for Amazon ', style: 'font-size: larger'},
                     { xtype: 'tbspacer', width: 5 },
                     { xtype: 'label', text : version, style: 'font-size: smaller' },
                     { xtype: 'tbspacer', width: 50 },
                     { xtype: 'label', text : 'Key ID:' },
                     { xtype: 'tbspacer' },
                     {
                         id: 'accessKeyId',
                         xtype: 'textfield',
                         allowBlank: false,
                         value: key.akey,
                     },
                     '   ',
                     { xtype: 'label', text : 'Secret:' },
                     { xtype: 'tbspacer' },
                     {
                        id: 'secretKeyId',
                        xtype: 'textfield',
                        allowBlank: false,
                        inputType: 'password',
                        value: key.skey,
                     },
                     {  xtype: 'button', 
                        icon: 'images/reload.png',
                    	handler :  function () {
                    	   checkCredentialsChange( true );
                        }
                     },
                     { xtype: 'tbspacer' },
                     optionsMenu(),
                     toolsMenu(),
                     '->',
                     { xtype: 'tbseparator' },
                     {
                        id: 'controlButtonId',
                        xtype: 'button',
                        text: 'Start Instance',
                        icon: 'images/start.png',
                        handler: startButtonClicked,
                        disabled: true
                     },
                     { xtype: 'tbspacer' }
            ]
         }
      ];
      MyViewportUi.superclass.initComponent.call(this);
   }
});

function optionsMenu() {
	
   // Add a vtype to check s3 url
   Ext.apply( Ext.form.field.VTypes, {
      s3url : function( val, field ) {
         var bpat = new RegExp( '^[a-z0-9][a-z0-9.-]{2,}$' );
    	 if ( !bpat.test( val.split('/',1)[0] ) )
            return false;
    	 
    	 return Ext.form.VTypes.url( 'http://mytest.com/' + val );
      },
      s3urlText : 'The S3 bucket name can only only contain numbers, lowercase ' +
    	          'characters, periods(.), and dashes(-).  It must also start '  +
    	          'with a letter or character and be more than 3 characters.'    +
    	          'Additional folder paths using the (/) delimiter may also be ' +
    	          'included.'
   });
   
   var ami = {
		      xtype: 'combo',
		      id: 'amiComboId',
		      fieldLabel: 'AMI',
		      labelAlign: 'right',
		      allowBlank: false,
		      forceSelection: true,
		      disableKeyFilter: true,
		      editable: false,
		      autoSelect: true,
		      queryMode: 'local',
		      displayField: 'desc',
		      valueField: 'id',
		      store:  new Ext.data.Store( { model: 'ami' } ),
		      listeners : { 'select': function(r) { aws.imageId = r.getValue() } }
		   };
   
   var instanceType = {
      xtype: 'combo',
      fieldLabel: 'Instance Type',
      labelAlign: 'right',
      store: instanceTypes,
      value: 'm1.large',
      allowBlank: false,
      autoScroll: true,
      editable: false,
	  size : 30,		// not working right now, unless width set on form
      triggerAction: 'all',
      listeners : { 'blur': function(r) { aws.instanceType = r.getValue() } }
   };

   // FIX: dynamically load zones
   var zone = {
      xtype: 'combo',
      fieldLabel: 'Availability Zone',
      store: [ 'No Preference', 
               'us-west-2a',
               'us-west-2b',
               'us-west-2c' ],
      value: 'No Preference',
      allowBlank: false,
      autoScroll: true,
      editable: false,
      listeners : { 'blur': function(r) { aws.zone = r.getValue() } }
   };
   
   var keyPair = {
      xtype: 'combo',
      id: 'keyPairComboId',
      fieldLabel: 'Key Pair',
      labelAlign: 'right',
      allowBlank: true,
      disableKeyFilter: true,
      store:  new Ext.data.Store( { model: 'keypair' } ),
      queryMode: 'local',
      displayField: 'id',
      valueField: 'id',
      autoScroll: true,
      editable: true,
      listeners : { 'blur': function(r) { aws.keyName = r.getValue() } }
   };   
   
   var timeout = {
		      xtype: 'numberfield',
		      fieldLabel: 'Auto shutdown (hrs)',
		      labelAlign: 'right',
		      allowDecimals: false,
		      allowNegative: false,
		      allowBlank: false,
		      maxLengthText: 3,
		      value: aws.shutdown,
		      valid : function(r) { aws.shutdown = r.getValue() },
		      minValue: 1,
		      maxValue: 72
		   };
   
   var ec2OptionsGroup = {
      xtype: 'fieldset',
	  id: 'ec2OptionsGroupId',
      title: 'EC2 Options',
      disabled: true,
      layout: 'anchor',
      defaults : {
    	 anchor: '100%',
    	 labelAlign: 'right',
    	 hideEmptyLabel: true
      }, items: [ instanceType, ami, zone, keyPair, timeout ]
   };
   
   var s3OptionsGroup = {
      xtype: 'fieldset',
	  id: 's3OptionsGroupId',
      title: 'S3 Options',
      defaultType: 'checkbox',
      disabled: true,
      defaults : {
    	 hideEmptyLabel: true
      },
      items: [{
    	 xtype: 'textfield',
    	 fieldLabel: 'S3 Bucket/Folder path',
    	 labelAlign: 'top',
         allowBlank: true,
         editable: true,
    	 name: 's3Url',
	     id: 's3UrlId',
	     size: 30,		// not working right now, unless width set on form
	     width: 275,
	     vtype: 's3url',
         value: aws.s3Url,
         listeners: { 
        	 'change': function(f,v) { 
                 if ( f.isValid() )
                     aws.s3Url = v; 
                 else
                     aws.s3Url = null; 
                 } 
             } 
      },{
         boxLabel: 'Sync with S3 contents on instance startup',
         checked: aws.s3SyncOnStart,
         listeners: { 'change' : function(f,v) { aws.s3SyncOnStart = v; } }
      },{
         boxLabel: 'Sync with S3 contents on instance shutdown',
         checked: aws.s3SyncOnStop,
         listeners: { 'change' : function(f,v) { aws.s3SyncOnStop = v; } }
      }]
   };
   
   var ebsOptionsGroup = {
      xtype: 'fieldset',
      id: 'ebsOptionsGroupId',
      title: 'EBS Options',
      disabled: true,
      defaults : {
    	  hideEmptyLabel: true
      },
      items: [{
    	 xtype: 'checkbox',
         boxLabel: ' Create and/or attach a EBS block storage volume to the ' +
                   '<br>instance at startup.<br>',
         name: 'ebsIsAttach',
         inputValue: 'false',
         checked: false,
         listeners: { 'change' : function(f,v) { aws.ebsIsAttach = v; } }
      },{
    	 xtype: 'numberfield',
    	 fieldLabel: 'Initial Size (GiB)',
		 labelAlign: 'right',
         value:    100,
    	 minValue: 1,
    	 maxValue: 1000,
    	 tooltip: 'Enter initial size of partition (1GiB to 1000GiB)'
      }]
   };
   
   return {
      xtype: 'button',
      text:  'Options',
      menu: {
         xtype: 'menu',
         items: [
            {
               xtype: 'form',
               titleCollapse: true,
               bodyPadding: 5,
               items: [ ec2OptionsGroup, s3OptionsGroup, ebsOptionsGroup ]
            }
         ]
      }
   };
}

function toolsMenu() {
   return {
      xtype: 'button',
      text: 'AWS Shortcuts',
      style: 'left-margin: 20px',
      menu: {
         xtype: 'menu',
         items: [
            {
               xtype: 'menuitem',
               text: 'Amazon Sign In/Register',
               icon: 'images/aws_logo.png',
               handler: function() { open( 'https://portal.aws.amazon.com/gp/aws/developer/registration/index.html', '_twa_aws' ) }
            },
            {
               xtype: 'menuitem',
               text: 'Amazon Security Credentials',
               icon: 'images/aws_logo.png',
               handler: function() { open( 'https://portal.aws.amazon.com/gp/aws/securityCredentials', '_twa_aws' ) }
            },
            {
               xtype: 'menuitem',
               text: 'EC2 Management Console',
               icon: 'images/aws_logo.png',
               handler: function() { open( 'https://console.aws.amazon.com/ec2/home', '_twa_aws' ) }
            },
            {
               xtype: 'menuitem',
               text: 'S3 Management Console',
               icon: 'images/aws_logo.png',
               handler: function() { open( 'https://console.aws.amazon.com/s3/home', '_twa_aws' ) }
            }
        ]
     }
  };
}

/*
 * Enable/disable AWS UI elements
 */
function awsState( enable, button ) {
   Ext.ComponentMgr.get('ec2OptionsGroupId').setDisabled( !enable );
   Ext.ComponentMgr.get('s3OptionsGroupId').setDisabled( !enable );
   Ext.ComponentMgr.get('ebsOptionsGroupId').setDisabled( !enable );
   Ext.ComponentMgr.get('controlButtonId').setDisabled( !enable );
   
   var b = Ext.ComponentMgr.get('controlButtonId');
   b.setDisabled( !enable ); 
   if ( b.getText() == 'Start Instance' && button == 'Stop Instance') { 
      b.setText( 'Stop Instance' );
	  b.setIcon( 'images/stop.png' );
	  b.handler = stopButtonClicked;
   } else if ( b.getText() == 'Stop Instance' && button == 'Start Instance') { 
      b.setText( 'Start Instance' );
	  b.setIcon( 'images/start.png' );
	  b.handler = startButtonClicked;
   }
}

/*
 * Check user provided credentials by querying Amazon through the API. Called 
 * when the access key or secret key loses focus.
 */
function checkCredentialsChange( force ) {
	
   force = typeof(force) != 'undefined' ? force : false;
   
   console.debug( "checkCredentialsChange()" );
   
   var newAccess = Ext.ComponentMgr.get( 'accessKeyId' ).getValue();
   var newSecret = Ext.ComponentMgr.get( 'secretKeyId' ).getValue();
   
   // Assign new credentials
   aws.instanceId = null;
   aws.accessKey  = (newAccess === '') ? null : newAccess;
   aws.secretKey  = (newSecret === '') ? null : newSecret;
   
   // Store client side using a session cookie with no expires so it stays
   // only in browser memory.  Not the best approach but...
   var ckey = Ext.JSON.encode( { akey: aws.accessKey, skey: aws.secretKey } );
   var key  = Ext.util.Cookies.set( 'TPP-TWA-session', ckey );

   // Signin 
   awsState( false );
   if ( aws.accessKey && aws.secretKey )
      {
      Ext.MessageBox.show( {
          id: 'controlDialogId',
          title: 'Sign In',
          msg: 'Signing into AWS...',
          icon: 'twa-busy-icon',
          width: 300,
          closable: false
          } );
      aws.signin( signInResponse );
      }
   else
      {
      return Ext.MessageBox.show( { title: 'Problem with you AWS credentials',  
                                    msg: 'Missing access or secret key', 
                                    buttons: Ext.MessageBox.OK, 
                                    icon: Ext.MessageBox.ERROR } );
      }
}

/*
 * Callback when finished signing in
 */
function signInResponse( error, reason ) { 
   console.debug( 'signInResponse(): finishing up sign in' );
   
   if ( error )
      return Ext.MessageBox.show( { title: error,  msg: reason, 
                                    buttons: Ext.MessageBox.OK, 
                                    icon: Ext.MessageBox.ERROR } );

   // Fill in ami image ids
   var c = Ext.ComponentMgr.get('amiComboId');
   c.clearValue();
   c.store.removeAll();
   for ( var i = 0; i < aws.amiList.length; i++ ) {
      var id   = aws.amiList[i].id;
      var desc = aws.amiList[i].desc;
      c.store.add( { id: id, desc : '(' + id + ') ' + desc } );
   }
   c.store.sync();
   if ( aws.amiList.length )
      c.select( aws.amiList[0].id );
   aws.imageId = aws.amiList[0].id;
   
  // Fill in key names 
   var c = Ext.ComponentMgr.get('keyPairComboId');
   c.clearValue();
   c.store.removeAll();
   for ( var i = 0; i < aws.keyPairList.length; i++ ) {
      var name = aws.keyPairList[i].name;
      c.store.add( { id: name } );
   }
   c.store.sync();
   
   // Fill in S3 URLs
   var c = Ext.ComponentMgr.get('s3UrlId');
   if ( !c.getValue() )
      c.setValue( 'tpp-' + aws.accessKey.toLowerCase() + '/twa' );
   aws.s3Url = c.getValue();
   
   if ( aws.instanceId ) {
      aws.getState();
   } else {
      awsState( true, 'Start Instance' );
      loadWelcome();
   }
}

/*
 * Invoked when user selects the control button and its in the start state
*/
function startButtonClicked( button ) {
   if ( !aws.accessKey || !aws.secretKey ) {
      Ext.MessageBox.alert( 'Missing Key(s)', 'You must first provide both a AWS access and secret key' );
      return;
   }
   
   Ext.MessageBox.show( {
      id: 'controlDialogId',
      title: 'Start Instance',
      msg: 'Requesting start of instance...',
      icon: 'twa-busy-icon',
      width: 300,
      closable: false
      } );
   
     aws.startInstance();
}

/*
 * Invoked when user selects the control button and its in the stop state 
 */
function stopButtonClicked( button ) {
   if ( !aws.accessKey || !aws.secretKey ) {
      Ext.MessageBox.alert( 'Missing Key(s)', 'You must first provide both a AWS access and secret key' );
      return;
   }
   
   if ( !aws.s3SyncOnStop && !aws.ebsAttach ) {
	  Ext.MessageBox.confirm( 
	     'Confirm shutdown', 
		 'Are you sure you want to do that? You may have unsaved changes.',
		 function(b) { if ( b === 'yes' ) stopButtonAction(); } );
      return;
   } 
   
   var mb = Ext.MessageBox.show( {
      title: 'Stop Instance',
      msg: 'Requesting stop of instance...',
      icon: 'twa-busy-icon',
      width: 300,
      closable: false
      } );
      
   aws.stopInstance();
}

//
// Called when a state response is received
//
MyViewportUi.stateUpdated = function ( resp, status, msg ) {
   console.debug( "stateUpdated() state: ", aws.instanceState );
   
   if ( status != 200 ) {
      Ext.MessageBox.show( { title: 'Unexpected AWS Error', 
                             msg: msg, 
                             buttons: Ext.MessageBox.OK, 
                             icon: Ext.MessageBox.ERROR } );
      return false;
   }
  
   if ( aws.instanceState == null ) {							// No instance
      awsState( true, 'Start Instance' );
      Ext.MessageBox.hide();
   } else if ( aws.instanceState == 0 )  {						// Pending...
      var m = 'Instance is now launching. It may take a few minutes to start.';
      if ( Ext.MessageBox.isVisible() )
         Ext.MessageBox.updateText( m );
      setTimeout( function()  { aws.getState() }, 5000 );
   } else if ( aws.instanceState == 16 ) {						// Running...
      var m = 'Instance ' + aws.instanceId + ' is running. Connecting...';
      if ( Ext.MessageBox.isVisible() )
         Ext.MessageBox.updateText( m );
      awsState( true, 'Stop Instance' );
      setTimeout( function() {  loadPetunia(5); }, 1500 );
   } else if ( aws.instanceState == 32 ) {						// Shutdown...
      if ( Ext.MessageBox.isVisible() )
         Ext.MessageBox.updateText( 'Instance is shutting down...' );
      setTimeout( function()  { aws.getState() }, 5000 );
   } else if ( aws.instanceId && aws.instanceState == 48 ) {	// Terminated...
      aws.instanceId = null; 
      aws.instanceState = null; 
      if ( Ext.MessageBox.isVisible() )
         Ext.MessageBox.updateText( 'Instance was terminated.' );
      setTimeout( function() { loadWelcome(); awsState( true, 'Start Instance') }, 3500 );
   } else if ( aws.instanceId ) {                            	 // Gone?
      aws.instanceId = null; 
      aws.instanceState = null; 
      if ( Ext.MessageBox.isVisible() )
         Ext.MessageBox.updateText( 'Unknown state of instance' );
      setTimeout( function() { loadWelcome(); awsState( true, 'Start Instance') }, 3500 );
   }
};

function loadPetunia99( retries ) {
   var c = document.getElementById('content');
   
   console.debug( 'loadPetunia() retries:' + retries + " src: " + c.src );
console.debug( c );   

var doc;
try { doc = c.contentWindow.document; } catch (e) {};
console.debug( "doc" );
console.debug( doc );
if ( doc )
   console.debug( "title " + doc.title );

   if ( c.src != aws.instanceUrl ||
		(doc && doc.title.indexOf('Web Launcher') != -1 ) ) {
       if ( retries ) {
           c.onload = function() { Ext.MessageBox.hide(); clearTimeout(); };
           c.src    = aws.instanceUrl;
   	       setTimeout( function() {  loadPetunia( --retries ); }, 1500 );
           return;
       } else {
             Ext.MessageBox.alert( 'Error connecting to TPP', 'unknown' );
       }
   } else {
       clearTimeout();
       Ext.MessageBox.hide(); 
   }
}

function loadPetunia() {
   var c = document.getElementById('content');
   
   // Make proxy request to check for Apache server...
   aws.checkPetunia( 5, function( status, msg ) {
          // Try to load it in the iframe regardless of status.  BTW, we can't
	      // really tell if this succeeds or fails due to XHR security.
          c.onload = function() { Ext.MessageBox.hide(); clearTimeout(); };
          c.src = aws.instanceUrl;
          
          setTimeout( function() { 
             if ( status != 200 ) {
                var m = msg + "<br><br>The TPP Web Launcher was unable to "
                      + " connect to the newly started EC2 instance. Please try"
                      + " reloading the page or resigning back in.";
                Ext.MessageBox.alert( 'Error connecting to TPP',  m );
             } else {
        	    Ext.MessageBox.hide();
             }
          }, '3500' );
   });
}

//
// Load petunia in the iframe
//
function loadPetunia2() {
   var c = document.getElementById('content');
   c.onload  = function() { 
console.debug( 'loaded url ' + c.src);
	   Ext.MessageBox.hide(); clearTimeout(); };
   c.src = aws.instanceUrl;
   // in case load fails and message box is visible
   setTimeout( function()  { 
var c = document.getElementById('content');
console.debug( 'obj', c);
console.debug( 'url ' + c.src);

Ext.MessageBox.hide() }, 10000 ); 
}

// Load welcome in the iframe
function loadWelcome() {
   var c = document.getElementById('content');
   c.onload  = function() { Ext.MessageBox.hide(); clearTimeout(); };
   c.src = 'welcome.html';
   // in case load fails and message box is visible
   setTimeout( function()  { Ext.MessageBox.hide() }, 5000 ); 
}

function showControlDialog( title, msg ) {
   Ext.MessageBox.show( {
      id: 'controlDialogId',
      title: title,
      msg: msg,
      icon: 'twa-busy-icon',
      width: 300,
      closable: false
      } );
}
