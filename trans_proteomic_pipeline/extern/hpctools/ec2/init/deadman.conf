#
# deadman - a "deadman" switch for automatically shutting down a system
#
# This is a Ubuntu upstart script for automatically shutting down the system
# after a period of time.  Very useful to automatically shutdown Amazon Web
# Services EC2 instances. It takes one (optional) upstart environment variable
# named 'TIMEOUT' which controls the time period of the shutdown.  See the
# shutdown command for the format of timeout values.
#
# To "reset" the deadman switch run:
#   % restart deadman
#
# To "stop" the deadman switch run
#   % stop deadman              
#
# To "set" the deadman switch run as root:
#   % start deadman
#
# To use the switch on another job place "start deadman" in the pre-start 
# script, "stop deadman" in the post-stop script, and "start deadman" in the 
# post-stop script. That way when the daemon is started the switch is cleared 
# and when it stops the switch is set. 
#
# For more information on how to install and use this script please see Ubuntu
# upstart (http://upstart.ubuntu.com/).
#
# NOTE: This currently uses shutdown to terminate the node.  For EBS backed
#       instances you may need to actually terminate the node, not just shut
#       it down.
#
description     "Automatically shuts down the system after a period of time"
author          "Joe Slagel <jslagel@systemsbiology.org>"

# Start when system boots up to shut system down in TIME_DEFAULT minutes
start on runlevel [2345]
stop on runlevel [!2345]

# Use a default time of 55 minutes for EC2 instances as you are charged per hr
env TIMEOUT_DEFAULT="+55"

script
   exec shutdown -h ${TIMEOUT:-${TIMEOUT_DEFAULT}} "deadman switch started"
end script

pre-stop script
   shutdown -c "deadman switch canceled" || true
end script
