<?xml version="1.0" encoding="UTF-8"?>

<bioml>
 
<note> DEFAULT PARAMETERS. The value of "isb_default_input_kscore.xml" is recommended. Change to "isb_default_input_native.xml" for native X!Tandem scoring.</note> 
	<note type="input" label="list path, default parameters">/regis/sbeams/bin/tandem/default_input.xml</note>

<note> FILE LOCATIONS. Replace them with your input (.mzXML) file and output file -- these are REQUIRED. Optionally a log file and a sequence output file of all protein sequences identified in the first-pass can be specified. Use of FULL path (not relative) paths is recommended. </note>
	<note type="input" label="spectrum, path">full_mzXML_filepath</note>
	<note type="input" label="output, path">full_tandem_output_path</note>
	<note type="input" label="output, log path"></note>
	<note type="input" label="output, sequence path">full_tandem_output_sequence_path</note>

<note> TAXONOMY FILE. This is a file containing references to the sequence databases. Point it to your own taxonomy.xml if needed.</note>
	<note type="input" label="list path, taxonomy information">/regis/sbeams/bin/tandem/taxonomy.xml</note>

<note> PROTEIN SEQUENCE DATABASE. This refers to identifiers in the taxomony.xml, not the .fasta files themselves! Make sure the database you want is present as an entry in the taxonomy.xml referenced above. This is REQUIRED. </note>
	<note type="input" label="protein, taxon">MM_20MB_Human_Jul_2006_TargDecoy.fasta</note>
 
<note> PRECURSOR MASS TOLERANCES. In the example below, a -2.0 Da to 4.0 Da (monoisotopic mass) window is searched for peptide candidates. Since this is monoisotopic mass, so for non-accurate-mass instruments, for which the precursor is often taken nearer to the isotopically averaged mass, an asymmetric tolerance (-2.0 Da to 4.0 Da) is preferable. This somewhat imitates a (-3.0 Da to 3.0 Da) window for averaged mass (but not exactly)</note>
 	<note type="input" label="spectrum, parent monoisotopic mass error minus">50</note>
 	<note type="input" label="spectrum, parent monoisotopic mass error plus">50</note>
	<note type="input" label="spectrum, parent monoisotopic mass error units">ppm</note>
		<note>The value for this parameter may be 'Daltons' or 'ppm': all other values are ignored</note>
	<note type="input" label="spectrum, parent monoisotopic mass isotope error">yes</note>
		<note>This allows peptide candidates in windows around -1 Da and -2 Da from the acquired mass to be considered. Only applicable when the minus/plus window above is set to less than 0.5 Da. Good for accurate-mass instruments for which the reported precursor mass is not corrected to the monoisotopic mass. </note>


<note> MODIFICATIONS. In the example below, there is a static (carbamidomethyl) modification on C, and variable modifications on M (oxidation). Multiple modifications can be separated by commas, as in "80.0@S,80.0@T". Peptide terminal modifications can be specified with the symbol '[' for N-terminus and ']' for C-terminus, such as 42.0@[ .  </note>
	
 	<note type="input" label="residue, modification mass">58.0184949@C,0.99703489@G,0.99703489@A,0.99703489@S,0.99703489@P,0.99703489@T,0.99703489@L,0.99703489@I,0.99703489@D,0.99703489@E,0.99703489@M,0.99703489@F,0.99703489@Y,0.99703489@V,1.99406978@N,1.99406978@K,1.99406978@Q,1.99406978@W,2.99110467@H,3.98813956@R</note>
 	<note type="input" label="residue, potential modification mass">15.994915@M</note>
	<note type="input" label="residue, potential modification motif"></note>
		<note> You can specify a variable modification only when present in a motif. For instance, 0.998@N!{P}[ST] is a deamidation modification on N only if it is present in an N[any but P][S or T] motif (N-glycosite). </note>
	<note type="input" label="protein, N-terminal residue modification mass"></note>
 	<note type="input" label="protein, C-terminal residue modification mass"></note>
		<note> These are *static* modifications on the PROTEINS' N or C-termini. </note>

<note> SEMI-TRYPTICS AND MISSED CLEAVAGES. In the example below, semitryptic peptides are allowed, and up to 2 missed cleavages are allowed. </note>
 	<note type="input" label="protein, cleavage semi">yes</note>
	<note type="input" label="scoring, maximum missed cleavage sites">2</note>

<note> REFINEMENT. Do not use unless you know what you are doing. Set "refine" to "yes" and specify what you want to search in the refinement. For non-confusing results, repeat the same modifications you set above for the first-pass here.</note>
	<note type="input" label="refine">no</note>
	<note type="input" label="refine, maximum valid expectation value">0.1</note>
 	<note type="input" label="refine, modification mass">57.021464@C</note>
	<note type="input" label="refine, potential modification mass">15.994915@M</note>
	<note type="input" label="refine, potential modification motif"></note>
 	<note type="input" label="refine, cleavage semi">yes</note>
	<note type="input" label="refine, unanticipated cleavage">no</note>
	<note type="input" label="refine, potential N-terminus modifications"></note>
	<note type="input" label="refine, potential C-terminus modifications"></note>
	<note type="input" label="refine, point mutations">no</note>
	<note type="input" label="refine, use potential modifications for full refinement">no</note>
	
<note> SPECTRUM, THREADS. The value of this paramter is a non-zero, positive integer in the range of 1 to 16.</note>
	<note type="input" label="spectrum, threads">8</note>


 	


</bioml>
