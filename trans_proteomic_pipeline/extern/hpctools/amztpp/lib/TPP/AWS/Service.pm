#
# Program: TPP AWS Search Tool
# Author:  Joe Slagel
#
# Copyright (C) 2009-2012 by Joseph Slagel
# 
# This library is free software; you can redistribute it and/or             
# modify it under the terms of the GNU Lesser General Public                
# License as published by the Free Software Foundation; either              
# version 2.1 of the License, or (at your option) any later version.        
#                                                                           
# This library is distributed in the hope that it will be useful,           
# but WITHOUT ANY WARRANTY; without even the implied warranty of            
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         
# General Public License for more details.                                  
#                                                                           
# You should have received a copy of the GNU Lesser General Public          
# License along with this library; if not, write to the Free Software       
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
# 
# Institute for Systems Biology
# 1441 North 34th St.
# Seattle, WA  98103  USA
# jslagel@systemsbiology.org
#
# $Id: Service.pm 6181 2013-04-09 22:28:49Z slagelwa $
#
package TPP::AWS::Service;
use strict;
use warnings;

use TPP::AWS;


#-- @GLOBALS ----------------------------------------------------------------#

our $REVISION = (q$Revision: 6181 $ =~ /(\d+)/g)[0] || '???'; 


#-- @PUBLIC -----------------------------------------------------------------#

#
# Constructor.
#
sub new
   {
   my ( $class ) = @_;
   
   my $this = {
      VERSION      => $TPP::VERSION,    # Our version
      UID          => undef,            # Unique identfier for the service
      BUCKET       => undef,            # S3 bucket containing input/output
      ERROR        => undef,        	# Status of service      
      MSG_HANDLE   => undef,        	# Handle for last message
      
      WDIR => undef,			# Working directory to use
      
      CLIENT => undef,              	# Hostname of client
      CLIENT_UPCOUNT   => 0,        	# Num of files uploaded by client
      CLIENT_UPBYTES   => 0,        	# Num of bytes uploaded by client
      CLIENT_UPSECS    => 0,        	# Num of secs uploading
      CLIENT_DOWNCOUNT => 0,        	# Num of files downloaded by client
      CLIENT_DOWNBYTES => 0,        	# Num of bytes downloaded by client
      CLIENT_DOWNSECS  => 0,        	# Num of secs downloading
      CLIENT_SUBMIT    => undef,        # time() service submitted
      CLIENT_RECEIVE   => undef,    	# time() service received
      
      SERVER           => undef,    	# Hostname of server
      SERVER_UPCOUNT   => 0,        	# Num of files uploaded by server
      SERVER_UPBYTES   => 0,        	# Num of bytes uploaded by server
      SERVER_UPSECS    => 0,        	# Num of secs uploading
      SERVER_DOWNCOUNT => 0,        	# Num of files downloaded by server
      SERVER_DOWNBYTES => 0,        	# Num of bytes downloaded by server
      SERVER_DOWNSECS  => 0,        	# Num of secs downloading
      SERVER_RECEIVE   => undef,        # time() service received
      SERVER_SUBMIT    => undef,        # time() service submitted
      SERVER_WTIME     => undef,        # wall time of server job
      };
      
   bless( $this, $class );
   return $this;
   }

#
# Uploads any input files for the service.  
# Abstract method to be overridden by the subclass service.
#
sub uploadInput
   {
   die "abstract method called";
   }
    
#
# Run the service.
# Abstract method to be overridden by the subclass service.
#
sub run
   {
   die "abstract method called";
   }
   
#
# Download output files from the service.  
# Abstract method to be overridden by the subclass service.
#
sub downloadOutput
   {
   die "abstract method called";
   }
   
1;
