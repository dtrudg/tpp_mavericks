#
# Program: TPP AWS Search Tool
# Author:  Joe Slagel
#
# Copyright (C) 2009-2012 by Joseph Slagel
# 
# This library is free software; you can redistribute it and/or             
# modify it under the terms of the GNU Lesser General Public                
# License as published by the Free Software Foundation; either              
# version 2.1 of the License, or (at your option) any later version.        
#                                                                           
# This library is distributed in the hope that it will be useful,           
# but WITHOUT ANY WARRANTY; without even the implied warranty of            
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         
# General Public License for more details.                                  
#                                                                           
# You should have received a copy of the GNU Lesser General Public          
# License along with this library; if not, write to the Free Software       
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
# 
# Institute for Systems Biology
# 1441 North 34th St.
# Seattle, WA  98103  USA
# jslagel@systemsbiology.org
#
# $Id: SearchService.pm 6007 2013-01-16 00:35:16Z slagelwa $
#
package TPP::AWS::SearchService;
use strict;
use warnings;

use Cwd qw( abs_path cwd );
use File::Basename;
use File::Copy;
use File::pushd;
use File::Path qw( mkpath );
use File::Spec::Functions qw( rel2abs splitdir );
use File::Spec::Win32;
use File::Which qw(which);

use TPP::AWS::Logger qw( $log );

use base qw(TPP::AWS::Service);


#-- @GLOBALS ----------------------------------------------------------------#

our $REVISION = (q$Revision: 6007 $ =~ /(\d+)/g)[0] || '???'; 

our $num = 1;			# Unique identier for service


#-- @PUBLIC -----------------------------------------------------------------#

#
# Constructor.
#
sub new
   {
   my ( $class, $input, $params, $odir ) = @_;
   
   my $fullinput = rel2abs( $input );
   my ( $root, $dir, $suffix ) = fileparse( $fullinput, qr/\.[^.]*$/ );
   
   my $self = $class->SUPER::new( $class, @_ );
   $self->{NUM}  = $num++;
   $self->{NAME} = $fullinput;                  # Name of search (input file)
   $self->{ODIR} = $odir || $dir;               # Client output directory
   $self->{WDIR} = $self->{ODIR};
   
   if ( $^O =~ /MSWin/ )                        # Set working dir to unix path
      {
      $self->{WDIR} = '/' . join( '/', splitdir( $self->{ODIR} ) );
      }
   
   $self->{INPUT_FILES} = {                     # Local paths to the input files
      INPUT  => $fullinput,                     # S3 key to input file
      PARAMS => rel2abs( $params ),             # S3 key to parameters file
      DB     => undef,                          # S3 key to database file
      };
   $self->{INPUT_KEYS}   = {};                  # S3 keys for the input files
   $self->{OUTPUT_FILES} = [];                  # EC2 paths to output files
   $self->{OUTPUT_KEYS}  = [];                  # S3 keys for the output files
      
   $self->{RETRY} = 9;                          # Num of times to requeue search
   
   return $self;
   }

#
# Change to the working directory
#
sub chwdir 
   {
   $log->debug( "change to directory $_[0]->{WDIR}" );
   mkpath( $_[0]->{WDIR} );
   return pushd $_[0]->{WDIR}
      or $log->logdie( "Error: can't chdir to $_[0]->{WDIR}: $!\n" );
   }

#
# Change to the output directory
#
sub chodir 
   {
   $log->debug( "change to directory $_[0]->{ODIR}" );
   mkpath( $_[0]->{ODIR} );
   return pushd $_[0]->{ODIR}
      or $log->logdie( "Error: can't chdir to $_[0]->{ODIR}: $!\n" );
   }

#
# Mounts working directory.  Linux EC2 instances have a fixed 10GB root
# partition and one or more instance stores of various sizes depending on the
# instance type.  They all mount one store as /mnt on the sytem. This routine
# binds the filesystem to the root directory of the input/output paths so we
# use the instance store space as opposed to filling up the root partition.
#
sub mount
   {
   $log->debug( "Mounting store for mapping file paths" );
   my ( $self ) = @_;
   
   my %mnts;
   foreach my $k ( keys %{ $self->{INPUT_KEYS} } )
      {
      my ( $root ) = splitdir( $self->{INPUT_KEYS}->{$k} );
      next if ( $mnts{$root} || -d $root );
      $log->debug( "Binding /mnt/$root to /$root" );
      mkpath( "/mnt/$root" );
      mkpath( "/$root" ); 
      if ( system("mount --bind /mnt/$root /$root") )
         {
         $log->error( "failed to mount directory: $@" );	
         }
      else
         {
         $mnts{$root} = 1;
         }
      }
      
   return;
   }
   
#
# Upload the complete set of input files needed to perform the search
#
# @arg   Reference to the S3 manager to use to upload
#
sub uploadInput
   {
   my ( $self, $s3m ) = @_;
   
   $log->debug( "uploading search input files" );
   
   my $cnt   = $s3m->uploadCount();
   my $bytes = $s3m->uploadBytes();
   my $secs  = $s3m->uploadSecs();
   
   foreach ( sort keys %{ $self->{INPUT_FILES} } )
      {
      my $file = $self->{INPUT_FILES}->{$_};
      if ( ! -f $file )
         {
         $log->error( "missing input file $file" );
         next;
         }
      $self->{INPUT_KEYS}->{$_} = $s3m->put( $file );
      }
      
   $self->{CLIENT_UPCOUNT} = $s3m->uploadCount() - $cnt;
   $self->{CLIENT_UPBYTES} = $s3m->uploadBytes() - $bytes;
   $self->{CLIENT_UPSECS}  = $s3m->uploadSecs() - $secs;
   return( scalar( keys %{$self->{INPUT_KEYS}} ) );
   }
  
#
# Download input files
#
# @arg   reference to S3Manager object
#
sub downloadInput
   {
   my ( $self, $s3m ) = @_;
   
   $log->debug( "downloading search input files" );
   
   my $cnt   = $s3m->downloadCount();
   my $bytes = $s3m->downloadBytes();
   my $secs  = $s3m->downloadSecs();
   
   $self->{WORK_FILES} = {};
   foreach ( sort { $a cmp $b } keys %{ $self->{INPUT_KEYS} } )
      {
      my $s3key = $self->{INPUT_KEYS}->{$_};
      $self->{WORK_FILES}->{$_} = abs_path( $s3m->get( $s3key ) );
      }
      
   $self->{SERVER_DOWNCOUNT} += ($s3m->downloadCount() - $cnt);
   $self->{SERVER_DOWNBYTES} += ($s3m->downloadBytes() - $bytes);
   $self->{SERVER_DOWNSECS}  += ($s3m->downloadSecs() - $secs);
   
   return( $self->{WORK_FILES} );
   }

#
# Upload resulting output files from the search
#
# @arg   reference to S3Manager object
# @arg   list of paths to files to upload
#
sub uploadOutput
   {
   my ( $self, $s3m ) = @_;
   
   $log->debug( "uploading search output files" );
   
   my $cnt   = $s3m->uploadCount();
   my $bytes = $s3m->uploadBytes();
   my $secs  = $s3m->uploadSecs();
   
   foreach my $out ( @{$self->{OUTPUT_FILES}} )
      {
      if ( !-f $out )
         {
         $log->error( "missing output file $out" );
         next;	
         }
      push @{$self->{OUTPUT_KEYS}}, $s3m->put( $out );
      }
      
   $self->{SERVER_UPCOUNT} = $s3m->uploadCount() - $cnt;
   $self->{SERVER_UPBYTES} = $s3m->uploadBytes() - $bytes;
   $self->{SERVER_UPSECS}  = $s3m->uploadSecs() - $secs;
   }
 
#
# Download resulting output files from the search
#
sub downloadOutput
   {
   my ( $self, $s3m ) = @_;
   
   $log->debug( "downloading search output files" );
   
   my $cnt   = $s3m->downloadCount();
   my $bytes = $s3m->downloadBytes();
   my $secs  = $s3m->downloadSecs();
   
   $s3m->get($_) foreach ( @{$self->{OUTPUT_KEYS}} );
   
   $self->{CLIENT_DOWNCOUNT} += ($s3m->downloadCount() - $cnt);
   $self->{CLIENT_DOWNBYTES} += ($s3m->downloadBytes() - $bytes);
   $self->{CLIENT_DOWNSECS}  += ($s3m->downloadSecs() - $secs);
   }
 
#
# Run on the input file using the parameters and database
#
sub run
   {
   my ( $self, $s3m, $servicelog, $sqsm ) = @_;

   $log->debug( ref($self) . " run started" );
   
   my $cwd;                             # preserve's cwd when changing
   eval 
      {
      $cwd = $self->chwdir();
      $self->downloadInput( $s3m );
      $self->_search( $servicelog );
      };
      
   if ( my $e = $@ )                    # something go wrong?
      {
      if ( $e =~ /download failed.*max retries/ && $self->{RETRY} )
          {
          $log->warn( "download failed, will requeue service for another try" );
          $self->{RETRY}--;
          return 1;
          }
      elsif ( $e =~ /child died with signal 15/ )
          {
          $log->warn( "service received TERM signal, requeue for another try" );
          return 1;
          }
      else
          {
          $log->fatal( $self->{ERROR} = $e );
          }
      }
      
   eval { $self->uploadOutput( $s3m ) };
   if ( my $e = $@ )                    # anything go wrong?
      {
      $log->fatal( $e );
      $self->{ERROR} = $self->{ERROR} ? "$self->{ERROR}\n$e" : $e;
      }

   $log->debug( ref($self) . " run finished" );
   return 0;
   }


#-- @PRIVATE -----------------------------------------------------------------#
 
#
# FIXME: for the time being manually fix the paths in the pep.xml file on
# the server as updateAllPaths.pl is broke
#
sub _updatePepXML
   {
   my ( $self, $pepxml, @files ) = @_;
   
   $log->debug( "updating paths in pep.xml file" );
   
   # Client input file basename
   my $cliBase = $self->{INPUT_FILES}->{INPUT};
   my $srvBase = $self->{WORK_FILES}->{INPUT};
   $cliBase =~ s/\.gz$//;               # wack off extensions
   $cliBase =~ s/\.(.*)?$//;
   $srvBase =~ s/\.gz$//;               # wack off extensions
   $srvBase =~ s/\.(.*)?$//;

   # Client output pepxml file
   my $cliPepxml = $pepxml;
   if ( $self->{ODIR} !~ q#^/# )	# windows client?
      {
      $cliPepxml = File::Spec::Win32::catdir( splitdir( $cliPepxml ) );
      $cliPepxml =~ s#^\\([A-Z]):#$1#;	# add back volume
      }

   my $tmp = File::Temp->new();
   open( TMP, ">$tmp" )    or $log->logdie( "can't open file $tmp: $!" );
   open( PEP, "<$pepxml" ) or $log->logdie( "can't open file $pepxml: $!" );
   while ( my $line = <PEP> )
      {
      # Fix parm/db/other file paths...
      foreach ( @files )
         {
         my $srv = $self->{WORK_FILES}->{$_} or next; 
         my $cli = $self->{INPUT_FILES}->{$_} or next;
         $line =~ s/\Q$srv/$cli/g;
         }
      $line =~ s/\Q$pepxml/$cliPepxml/g;  # ...pepxml path
      $line =~ s/\Q$srvBase/$cliBase/g;   # ...base_name path
      print TMP $line;	
      }
   close( TMP );
   close( PEP );
   copy( $tmp, $pepxml ) or $log->logdie( "can't move $tmp to $pepxml: $!");
   }

#
# DEPRECATED
# Update (repair) mzXML/pep.xml paths found in the given file.
#
# @arg   input filename to update
# @arg   new location of database file (optional)
#
sub _updatePaths
   {
   my ( $self, $filename, $database ) = @_;

   # Check input/paths
   $database = abs_path( $database );
   $log->warn( "missing $filename " )     if ( !-f $filename );
   $log->warn( "missing $database file" ) if ( $database && !-f $database );
   if ( !-f $filename || ($database && !-f $database) )
      {
      $log->error( "unable to update paths in $filename" );
      return 0 
      }

   # Build command
   my $UPDATEPATHS = which('updateAllPaths.pl')
      or $log->logdie( "Error updateAllPaths.pl program not found in path" );
   my $db  = ( $database ? "--database \"$database\"" : '' );
   my $cmd = "$UPDATEPATHS $db --quiet $filename > /dev/null";
   
   # Believe it or not we need to run it twice due to a bug in updateAllPaths
   # skipping the update of the database if the paths are similar/same
   $self->_system( $cmd );
   $self->_system( $cmd );
   
   return 1;
   }

#
# Call system() but throw an exception if something goes wrong
#
sub _system
   {
   my $self = shift;
   $log->debug( "system( @_ )" );
   system( @_ );
   if ( $? == -1 ) 
      {
      $log->logdie( "failed to execute system: $!" );
      }
   elsif ( $? & 127 ) 
      {
      $log->logdie( sprintf("child died with signal %d", $? & 127) );
      }
   elsif ( $? >> 8 )
      {
      $log->logdie( sprintf("child exited with value %d", $? >> 8) );
      }
   }
   
1;
