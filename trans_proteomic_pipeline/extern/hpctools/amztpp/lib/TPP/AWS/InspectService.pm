#
# Program: TPP AWS Search Tool
# Author:  Joe Slagel
#
# Copyright (C) 2009-2012 by Joseph Slagel
# 
# This library is free software; you can redistribute it and/or             
# modify it under the terms of the GNU Lesser General Public                
# License as published by the Free Software Foundation; either              
# version 2.1 of the License, or (at your option) any later version.        
#                                                                           
# This library is distributed in the hope that it will be useful,           
# but WITHOUT ANY WARRANTY; without even the implied warranty of            
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         
# General Public License for more details.                                  
#                                                                           
# You should have received a copy of the GNU Lesser General Public          
# License along with this library; if not, write to the Free Software       
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
# 
# Institute for Systems Biology
# 1441 North 34th St.
# Seattle, WA  98103  USA
# jslagel@systemsbiology.org
#
# $Id: InspectService.pm 6181 2013-04-09 22:28:49Z slagelwa $
#
package TPP::AWS::InspectService;
use strict;
use warnings;

use File::Basename;
use File::Copy;
use File::pushd;
use File::Spec::Functions qw( rel2abs );
use File::Which qw(which);
use TPP::AWS::S3Manager;
use TPP::AWS::Logger qw( $log );

use base qw(TPP::AWS::SearchService);


#-- @GLOBALS ----------------------------------------------------------------#

our $REVISION = (q$Revision: 6181 $ =~ /(\d+)/g)[0] || '???'; 


#-- @PUBLIC -----------------------------------------------------------------#

#
# Constructor.
#
# @arg   Path to mzXML formatted spectrum file to search with Inspect
# @arg   Path to parameters file to use for search
#
sub new
   {
   my $class = shift;
   my ( $input, $params ) = @_;
   
   my $self = $class->SUPER::new( @_);
   
   # Get database file from parameters file
   $self->{INPUT_FILES}->{DB} = $self->_readParams( $self->{INPUT_FILES}->{PARAMS} );
   
   return $self;
   }
 

#-- @PRIVATE -----------------------------------------------------------------#

#
# Read the inspect parameters file for the database name
#
# @arg   parameter filename
#
sub _readParams
   {
   my ( $self, $params ) = @_;
   
   my $db = undef;
   open( PARAMS, $params ) or $log->logdie( "can't open file $params: $!" );
   while ( <PARAMS> )
      {
      chomp; s/#.*$//;                                  # remove comments
      next if ( /^$/ );                                 # skip empty lines
      $db = $2 if ( /\s*(SequenceFile|DB)\s*,(.*)$/i )  # found database?
      }
   close( PARAMS );
   $db || $log->logdie( "no database found in $params" );
   return rel2abs($db);
   }

#
# Copies the contents of a inspect parameter file, replacing specific values
# of parameters as directed.  Useful to create a input specific parameters
# file from a template as inspect includes the input file name as one of its
# parameters, necessitating a parameter file for each input.
#
#  @arg   source (template) parameter filename to copy from
#  @arg   destination parameter filename
#  @arg   name of inspect input filename to replace (optional)
#  @arg   name of database file to replace (optional)
#
sub _copyParams
   {
   my ( $self, $src, $dst, $input, $db ) = @_;

   $log->debug( "copying $src parameters to $dst" ); 
   
   open( IN,  "< $src" ) or $log->logdie( "can't open $src: $!\n" );
   open( OUT, "> $dst" ) or $log->logdie( "can't open $dst: $!\n" );

   while ( <IN> )
      {
      s/^(\s*spectra\s*),.*$/$1,$input/i        if ( $input );
      s/^(\s*SequenceFile\s*),.*$/$1,$db/i      if ( $db );
      print OUT $_;
      }

   close IN;
   close OUT;
   return $dst;
   }
 
#
# Runs inspect search on given input.
#
sub _search
   {
   my ( $self, $servicelog ) = @_;

   my $input  = $self->{WORK_FILES}->{INPUT};
   my $params = $self->{WORK_FILES}->{PARAMS};
   my $db     = $self->{WORK_FILES}->{DB};
   
   my ( $root, $dir, $suffix ) = fileparse( $input, qr/\.[^.]*$/ );
   
   my $output = $self->{OUTPUT_FILES};
   unlink "$root.inspect.params", "$root.inspect", "$root.pep.xml",
      "$root.inspect.log";
   if ( $servicelog )
      {
      unlink( $servicelog );
      symlink( rel2abs("$root.inspect.log"), $servicelog ) 
         or $log->warn( "unable to create symbolic link for service log");
      }
   
   # Copy parameters
   $self->_copyParams( $params, "$root.inspect.params", "$root.mgf", $db );
   push @$output, "$root.inspect.params";

   # Use location of inspect executable for directory containing resources
   my $inspect = which('inspect')
      or $log->logdie( "Error inspect program not found in path" );
   my $res = (fileparse( $inspect ))[1];
    
   # Convert input to mgf (inspect only reads mzML files)
   $log->debug( "converting input to mgf for inspect search" );
   unlink "$root.mgf";
   my $MZXML2SEARCH = which('MzXML2Search')
      or $log->logdie( "Error MzXML2Search program not found in path" );
   push @$output, "$root.inspect.log";
   my $cmd = "$MZXML2SEARCH -mgf $input > $root.inspect.log 2>&1";
   $self->_system( $cmd );
      
   move( "$dir$root.mgf", "$root.mgf" )
      or $log->logdie( "can't move mgf file: $!");
   push @$output, "$root.mgf";

   # Run search
   $log->debug( "invoking inspect search on $root$suffix" );
   $cmd = "$inspect -r $res -i $root.inspect.params -o $root.inspect -e $root.inspect.err"
           . " >> $root.inspect.log 2>&1";
   eval { $self->_system( $cmd ) };
   if ( my $e = $@ ) {
      system( "cat $root.inspect.err >> $root.inspect.log" );
      $log->logdie( $e );
   }
   push @$output, "$root.inspect";
   system( "cat $root.inspect.err >> $root.inspect.log" );

   # Convert output
   $log->debug( "converting inspect $root.inspect output" );
   $cmd = "python ${res}InspectToPepXML.py -i $root.inspect -o $root.pep.xml"
        . " -p $root.inspect.params "
        . " >> $root.inspect.log 2>&1";
   $self->_system( $cmd );
   if ( -f "$root.pep.xml" )
      {
      push @$output, "$root.pep.xml";
      $self->_updatePepXML( "$root.pep.xml", qw(INPUT PARAMS DB) );
      }
   else
      {
      $log->logdie( "invoking InspectToPepXML.py $?");
      }
   }

1;
