MaRiMba_Tutorial_README

Last updated: 19 July 2010, C. Holstein

---------------------------------------

As of TPP version 4.4, the MaRiMba tutorial files are no longer packaged with the TPP installer, for the sake of file size.
The MaRiMba tutorial files can now be freely downloaded from SourceForge at:

https://sourceforge.net/projects/sashimi/files/MaRiMba/

This directory contains a MaRiMba_Tutorial zip file that, when downloaded and unzipped, contains 8 files pertaining to the MaRiMba tutorial:

1. Getting_Started_with_MaRiMba.pdf
2. 18mix_SamplePeptideList.txt
3. 18mix_Ecoli_ProteinList.txt
4. 18mix_db_pure_20081209.fasta
5. 18mix_Mix7_Raw.splib
6. 18mix_Mix7_Raw.sptxt
7. 18mix_Mix7_Raw.spidx
8. 18mix_Mix7_Raw.pepidx

(You can also find the files under version control in the top-level "MaRiMba_Tutorial" directory corresponding to the TPP release, eg https://sashimi.svn.sourceforge.net/svnroot/sashimi/tags/release_4-4-0/MaRiMba_Tutorial)
