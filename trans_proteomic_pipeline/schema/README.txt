This directory contains the XML schemas for the various input/output
files used by TPP.  If you are going to update a schema its asked that
increment its version in the filename.  For example, pepXML_v116 went
to pepXML_v117.  Its also asked that before releasing the schema that
you validate it.  There are a number of ways to do this, including:

   * xmllint
   * http://www.validome.org/grammar/validate

To publish a schema please update the files at 
http://regis-web.systemsbiology.net/pepXML.
